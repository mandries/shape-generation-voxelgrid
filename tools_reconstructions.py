import os # to walk through directories, to rename files
import binvox_rw # to manipulate binvox files
# import pcl_tools

# Save the 3D voxelgrids to files
def save_reconstructed_samples(
        folder_path,
        samples,
        iteration_number):

    # Generate a folder to store the images
    directory = folder_path
    # directory = "reconstructions_" + str(int(time.time())) + "iter_{0}".format(iteration_number)
    # if not os.path.exists(directory):
    #     os.makedirs(directory)

    print('Saving reconstructed images for iteration {0}... '.format(iteration_number))
    for sample_index in range(0, samples.shape[0]):
        # 1. Write original model to file

        # Write as point cloud
        # filename = os.path.join(   directory,
        #                            "sample_{0}_orig.pcd".format(sample_index)
        #                            )
        # pointcloud = pcl_tools.voxelized_to_pointcloud(samples[sample_index])
        # pcl_tools.write_pointcloud_to_file(pointcloud, filename)

        # 1. Write reconstructed model to file
        # Threshold at 0.5 (to have binary states for voxels)
        voxelgrid = samples[sample_index] > 0.5
        # Write as voxelgrid (binvox format)
        data = voxelgrid
        dims = (64,64,64) # DEFAULT voxelgrid size here (not true size, which may vary)
        translate = (0,0,0)
        scale = 1
        axis_order = 'xyz'
        voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
        # Specify the file path
        filename = os.path.join(   directory,
                                   "sample_{0}.binvox".format(sample_index)
                                   )
        filewriter = open(filename, 'w')
        # Write the binvox model to file (so that it can be seen through viewvow)
        binvox_rw.write(voxel_model, filewriter)
    print('done')
    return

# This function is OK for 3d
def save_reconstructed_samples_pairs(
        folder_path,
        reconstructed_samples,
        original_samples,
        iteration_number):

    # Generate a folder to store the images
    directory = folder_path
    # directory = "reconstructions_" + str(int(time.time())) + "iter_{0}".format(iteration_number)
    # if not os.path.exists(directory):
    #     os.makedirs(directory)

    print('Saving reconstructed images for iteration {0}... '.format(iteration_number))
    for sample_index in range(0, reconstructed_samples.shape[0]):
        # 1. Write original model to file

        # Write as point cloud
        # filename = os.path.join(   directory,
        #                            "sample_{0}_orig.pcd".format(sample_index)
        #                            )
        # pointcloud = pcl_tools.voxelized_to_pointcloud(original_samples[sample_index])
        # pcl_tools.write_pointcloud_to_file(pointcloud, filename)

        # Write as voxelgrid (binvox format)
        data = original_samples[sample_index] #voxelgrid
        dims = (64,64,64) # DEFAULT voxelgrid size here (not true size, which may vary)
        translate = (0,0,0)
        scale = 1
        axis_order = 'xyz'
        voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
        # Specify the file path
        filename = os.path.join(   directory,
                                   "sample_{0}_orig.binvox".format(sample_index)
                                   )
        filewriter = open(filename, 'w')
        # Write the binvox model to file (so that it can be seen through viewvow)
        binvox_rw.write(voxel_model, filewriter)


        # 2. Write reconstructed model to file
        # Threshold at 0.5 (to have binary states for voxels)
        voxelgrid = reconstructed_samples[sample_index] > 0.5
        # Write as voxelgrid (binvox format)
        data = voxelgrid
        dims = (64,64,64) # DEFAULT voxelgrid size here (not true size, which may vary)
        translate = (0,0,0)
        scale = 1
        axis_order = 'xyz'
        voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
        # Specify the file path
        filename = os.path.join(
                        directory,
                        "sample_{0}_recon.binvox".format(sample_index))
        filewriter = open(filename, 'w')
        # Write the binvox model to file (so that it can be seen through viewvow)
        binvox_rw.write(voxel_model, filewriter)
    print('done')
    return

# Saves only the non-empty reconstructed samples
def save_reconstructed_samples_non_empty(
        folder_path,
        reconstructed_samples,
        original_samples,
        iteration_number):

    # Generate a folder to store the images
    directory = folder_path
    # directory = "reconstructions_" + str(int(time.time())) + "iter_{0}_ne".format(iteration_number)
    # if not os.path.exists(directory):
    #     os.makedirs(directory)

    print('Saving reconstructed images for iteration {0}... '.format(iteration_number))
    recons_saved_non_empty = 0 # false;
    for sample_index in range(0, reconstructed_samples.shape[0]):
        # 2. Write reconstructed model to file
        # Threshold at 0.5 (to have binary states for voxels)
        voxelgrid = reconstructed_samples[sample_index] > 0.5
        if (voxelgrid.sum()>0): # Check if the volums is not empty
            recons_saved_non_empty += 1

            # Write as voxelgrid (binvox format)
            data = voxelgrid
            dims = (64,64,64) # DEFAULT voxelgrid size here (not true size, which may vary)
            translate = (0,0,0)
            scale = 1
            axis_order = 'xyz'
            voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
            # Specify the file path
            filename = os.path.join(   directory,
                                       "sample_{0}_recon.binvox".format(sample_index)
                                       )
            filewriter = open(filename, 'w')
            # Write the binvox model to file (so that it can be seen through viewvow)
            binvox_rw.write(voxel_model, filewriter)

            # 1. Write original model to file
            # Write as voxelgrid (binvox format)
            data = original_samples[sample_index] #voxelgrid
            dims = (64,64,64) # DEFAULT voxelgrid size here (not true size, which may vary)
            translate = (0,0,0)
            scale = 1
            axis_order = 'xyz'
            voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
            # Specify the file path
            filename = os.path.join(
                            directory,
                            "sample_{0}_orig.binvox".format(sample_index))
            filewriter = open(filename, 'w')
            # Write the binvox model to file (so that it can be seen through viewvow)
            binvox_rw.write(voxel_model, filewriter)
    print('done')
    print("Recons non-empty: {0}".format(recons_saved_non_empty))
    return
