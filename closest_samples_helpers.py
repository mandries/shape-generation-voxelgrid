from __future__ import absolute_import, division, print_function

import os
import sys
import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt
import matplotlib as ml
import os
from tensorflow.contrib import layers
from tensorflow.contrib.framework import arg_scope

import model_vae_3d
import tools_dataset_processing
import tools_combination
import binvox_rw # to manipulate binvox files
import pcl_tools
import vae_aff_combine_multiple_samples_and_inspect as basics


latent_dim = 512
voxelized_input_width = 64
voxelized_input_height = 64
voxelized_input_depth = 64

# Parameters
learning_rate = 0.002
total_epochs = 10000
batch_size = 128

models_path = '/media/Data/mihai/models'
model_loading_path = os.path.join(models_path, 
                     'model_ModelNet40__t_1530762963__epoch_69__train_49643__test_89436__void_276')

def get_dataset_representations(layer=3):
  #dataset part:
  dataset_container = []
  dataset_root_location = '/media/Data/datasets/'
  dataset_name = 'ModelNet40_binvox_exact_center_4views/'
  object_classes = [\
                    'airplane',
                    'bathtub',
                    'bed',
                    'bench',
                    'bookshelf',
                    'bottle',
                    'bowl',
                    'car',
                    'chair',
                    'cone',
                    'cup',
                    'curtain',
                    'desk',
                    'door',
                    'dresser',
                    'flower_pot',
                    'glass_box',
                    'guitar',
                    'keyboard',
                    'lamp',
                    'laptop',
                    'mantel',
                    'monitor',
                    'night_stand',
                    'person',
                    'piano',
                    'plant',
                    'radio',
                    'range_hood',
                    'sink',
                    'sofa',
                    'stairs',
                    'stool',
                    'table',
                    'tent',
                    'toilet',
                    'tv_stand',
                    'vase',
                    'wardrobe',
                    'xbox']


  input_file_format_suffix = '_2.binvox'
  #dataset_train_paths = [ dataset_root_location + '/ModelNet10_binvox_exact_center_4views/bed/train/']
  dataset_train_paths = list()
  for index in range(len(object_classes)):
        # Training dataset
    dataset_train_paths.append(dataset_root_location + dataset_name + object_classes[index] + '/train/')

  #import pdb; pdb.set_trace()

  files_name_list = list()
  for index in range(len(dataset_train_paths)):
    files_name_list.extend(tools_dataset_processing.load_dataset_binvox_64(dataset_train_paths[index],
                                                    dataset_container,
                                                    input_file_format_suffix))

  dataset_container = np.asarray(dataset_container)
  iterator = tools_dataset_processing.data_iterator(dataset_container,  
                                                     batch_size, False)
  
  representation_tensor_name = 'model/conv3d_transpose_' + str(layer) + '/conv3d_transpose:0'


  with tf.Graph().as_default() as graph:
    input_placeholder = tf.placeholder(tf.float32,shape=[None, 
                                                        voxelized_input_width,
                                                        voxelized_input_height,
                                                        voxelized_input_depth])

    # Building the decoder
    target_placeholder = input_placeholder # the thing we are trying to reconstruct

    # Capture the output of the encoder
    with tf.variable_scope("model"):
      z_mean, z_log_var = model_vae_3d.encoder_net(input_placeholder)
      eps = tf.random_normal( tf.shape(z_log_var),dtype=tf.float32, mean=0., stddev=1.0, name='epsilon')
      z = tf.add(z_mean, tf.exp(z_log_var / 2) * eps) 
      output_voxelized_volume = model_vae_3d.decoder_net(z)

    dataset_representation_tensor = graph.get_tensor_by_name(representation_tensor_name)
    vars_vae_savable = [v for v in tf.trainable_variables() if v.name.startswith('model')]
    vars_for_init = [v for v in tf.global_variables() if v not in vars_vae_savable]
    saver = tf.train.Saver(var_list=vars_vae_savable)
    init= tf.variables_initializer(vars_for_init)
    with tf.Session(config=tf.ConfigProto(allow_soft_placement = True)) as sess:
      sess.run(init)
      saver.restore(sess, tf.train.latest_checkpoint(model_loading_path))
      train_dataset_representation = list()
      for batch_idx in range(0, len(dataset_container), batch_size):    
        batch_x = iterator.next()
        train_dataset_representation.append(dataset_representation_tensor.eval(feed_dict={input_placeholder: batch_x}))

  return np.vstack(train_dataset_representation), files_name_list

def get_combined_obj_representation(obj_1='chair', obj_2='table', percentage=0.8, layer=3):
  obj_1_paths=['/media/Data/datasets/ModelNet10_binvox_exact_center_4views/'+obj_1+'/test/']
  obj_2_paths=['/media/Data/datasets/ModelNet10_binvox_exact_center_4views/'+obj_2+'/test/']
  void_path = ['/media/Data/datasets/ModelNet10_binvox_exact_center_4views/void']
  obj_1_container = list()
  obj_2_container = list()
  void_container = list()
  tools_dataset_processing.load_dataset_binvox_64(
          obj_1_paths[0], 
          obj_1_container,
          basics.input_file_format_suffix)
  tools_dataset_processing.load_dataset_binvox_64(
          obj_2_paths[0], 
          obj_2_container,
          basics.input_file_format_suffix)
  tools_dataset_processing.load_dataset_binvox_64(
          void_path[0], 
          void_container,
          basics.input_file_format_suffix);
  obj_1_container = np.asarray(obj_1_container)
  obj_2_container = np.asarray(obj_2_container)
  void_container = np.asarray(void_container)
  representation_tensor_name = 'model/conv3d_transpose_' + str(layer) + '/conv3d_transpose:0'
  make_encoder = tf.make_template('', model_vae_3d.encoder_net)
  # make_decoder = tf.make_template('decoder', model_vae_3d.decoder_net)
  def epsilon_like(tensor, name='epsilon'):
    return tf.random_normal(tf.shape(tensor), dtype=tf.float32, name=name)


  with tf.Graph().as_default() as graph:
    input_placeholder = tf.placeholder(tf.float32, 
                                       shape=[None]+list(void_container.shape[1:]),
                                       name="input_placeholder")

    with tf.variable_scope("model", reuse=None):
      objects_z_means_and_log_vars = make_encoder(input_placeholder)
      avg_z_means_and_log_vars = [tf.reduce_mean(tt, axis=0, keepdims=True) 
                                  for tt in objects_z_means_and_log_vars]
      void = tf.constant(void_container, dtype=tf.float32, name='void')
      void_z_means_and_log_vars = make_encoder(void);
      avg_dist = tf.distributions.Normal(loc=avg_z_means_and_log_vars[0],
                                         scale=tf.exp(avg_z_means_and_log_vars[1]/2),
                                         validate_args=True,
                                         allow_nan_stats=False,
                                         name='avg_dist')

      void_dist = tf.distributions.Normal(loc=void_z_means_and_log_vars[0],
                                          scale=tf.exp(void_z_means_and_log_vars[1]/2),
                                          validate_args=True,
                                          allow_nan_stats=False,
                                          name='void_dist')
                                              
      prior_dist = tf.distributions.Normal(loc=tf.zeros_like(void_z_means_and_log_vars[0]),
                                           scale=tf.ones_like(void_z_means_and_log_vars[1]),
                                           validate_args=True,
                                           allow_nan_stats=False,
                                           name='void_dist')                                            
      kl_with_void = tf.distributions.kl_divergence(avg_dist, void_dist, 
                                                    allow_nan_stats=False, name='kl_with_void')
      kl_with_prior = tf.distributions.kl_divergence(avg_dist, prior_dist, 
                                                     allow_nan_stats=False, name='kl_with_void')
                                                          
      z_placeholder = tf.placeholder(tf.float32,
              shape=[None]+void_z_means_and_log_vars[0].get_shape().as_list()[1:],
              name="latent_placeholder")
      output = tf.nn.sigmoid(model_vae_3d.decoder_net(z_placeholder)) 
      combination_representation_tensor = graph.get_tensor_by_name(representation_tensor_name)

      var_dict = {v.name.replace('//','/').split(':')[0]:v
                  for v in tf.get_default_graph().get_collection(tf.GraphKeys.GLOBAL_VARIABLES)}
      saver = tf.train.Saver(var_dict)

      with tf.Session(config=tf.ConfigProto(allow_soft_placement = True)) as sess:
        saver.restore(sess, tf.train.latest_checkpoint(model_loading_path))
        kl_for_obj_1_void = kl_with_void.eval(session=sess, 
                                              feed_dict={input_placeholder: obj_1_container})
        kl_for_obj_2_void = kl_with_void.eval(session=sess, 
                                              feed_dict={input_placeholder: obj_2_container})
        kl_for_obj_1_prior = kl_with_prior.eval(session=sess, 
                                                feed_dict={input_placeholder: obj_1_container})
        kl_for_obj_2_prior = kl_with_prior.eval(session=sess, 
                                                feed_dict={input_placeholder: obj_2_container})
        kl_total_obj_1 = kl_for_obj_1_void + kl_for_obj_1_prior
        kl_total_obj_2 = kl_for_obj_2_void + kl_for_obj_2_prior
        top_percentage = percentage
        sorted_arg_max_obj_1 = np.argsort(kl_total_obj_1,axis=1)
        sorted_arg_max_obj_2 = np.argsort(kl_total_obj_2,axis=1)
        important_obj_1_idxs = sorted_arg_max_obj_1[0,:int(top_percentage*sorted_arg_max_obj_1.shape[1])]
        important_obj_2_idxs = sorted_arg_max_obj_2[0,:int(top_percentage*sorted_arg_max_obj_2.shape[1])]
        obj_1_avg_means_and_logvars=sess.run(avg_z_means_and_log_vars,
                                             feed_dict={input_placeholder: obj_1_container})
        obj_2_avg_means_and_logvars=sess.run(avg_z_means_and_log_vars, 
                                             feed_dict={input_placeholder: obj_2_container})
        void_avg_means_and_logvars=sess.run(avg_z_means_and_log_vars,
                                            feed_dict={input_placeholder: void_container})
        combination_means_and_logvars = np.copy(obj_1_avg_means_and_logvars)
        for component_idx, _ in enumerate(combination_means_and_logvars):
            combination_means_and_logvars[component_idx][0,important_obj_1_idxs] = \
              obj_1_avg_means_and_logvars[component_idx][0,important_obj_1_idxs]
            combination_means_and_logvars[component_idx][0,important_obj_2_idxs] = \
              obj_2_avg_means_and_logvars[component_idx][0,important_obj_2_idxs]

        combined_latent = combination_means_and_logvars[0] + \
                          np.exp(combination_means_and_logvars[1]/2) * \
                          np.random.normal(size=combination_means_and_logvars[1].shape)
        obj_1_latent = obj_1_avg_means_and_logvars[0] + \
                       np.exp(obj_1_avg_means_and_logvars[1]/2) * \
                       np.random.normal(size=obj_1_avg_means_and_logvars[1].shape)
        obj_2_latent = obj_2_avg_means_and_logvars[0] + \
                       np.exp(obj_2_avg_means_and_logvars[1]/2) * \
                       np.random.normal(size=obj_2_avg_means_and_logvars[1].shape)
        combined_latent = combination_means_and_logvars[0] + \
                          np.exp(combination_means_and_logvars[1]/2) * \
                          np.random.normal(size=combination_means_and_logvars[1].shape)
        obj_1_latent = obj_1_avg_means_and_logvars[0] + \
                       np.exp(obj_1_avg_means_and_logvars[1]/2) * \
                       np.random.normal(size=obj_1_avg_means_and_logvars[1].shape)
        obj_2_latent = obj_2_avg_means_and_logvars[0] + \
                       np.exp(obj_2_avg_means_and_logvars[1]/2) * \
                       np.random.normal(size=obj_2_avg_means_and_logvars[1].shape)

        return combination_representation_tensor.eval(session=sess, 
                                                feed_dict={z_placeholder: combined_latent})
