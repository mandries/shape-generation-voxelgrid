from __future__ import print_function, absolute_import, unicode_literals, division
import tensorflow as tf
import nn_blocks_enc_dec_dis_iters as helpers
import model_vae_3d #as model_vae_3d
import numpy as np
import time
import math # for ceiling function
from os.path import join as opjoin
import tools_reconstructions

voxelized_input_width = 64
voxelized_input_height = 64
voxelized_input_depth = 64

nn_img_input_channels = 1  # 4 # (3 color + 1 depth)
regularizer = 1e-3

################################################################################
# Logic of this class:
# TODO write description
################################################################################

class VAE(object):

	# NOTE: CHECKED BY MIHAI
	# Constructor of the VAE class
	def __init__(self,
               latent_dim=2 ** 9, # 2 ** 11,
               output_dim=(2 ** 6) ** 3,
               is_training=True,
               graph=None,
               reuse=False,
               verbose=False):
		# Assign the dimensions of:
		# - latent layer
		# - output layer
		self.latent_dim = latent_dim
		self.output_dim = output_dim
		self.is_training_py = is_training
		self.reuse = reuse
		self.graph = graph if graph else tf.Graph()
		self.verbose = verbose

		# TensorBoard summaries
		# https://stackoverflow.com/questions/5980042/how-to-implement-the-verbose-or-v-option-into-a-script
		self.verboseprint = print if verbose else lambda *a, **k: None
		self.hist_summary = tf.summary.histogram if is_training else lambda *a, **k: None
		self.scalar_summary = tf.summary.scalar if is_training else lambda *a, **k: None
		self.merge_summary = tf.summary.merge if is_training else lambda *a, **k: None
		#
		self.epoch_summ_list = list()  # After each epoch, mainly for losses
		self.minibatch_summ_list = list()  # once every 10 mini-batches, mainly weights, gradients, etc
		self.continuous_summ_list = list()  # Always, similar to epoch, but continuously

		with self.graph.as_default():
			with tf.device('/cpu:0'):
				self.input_placeholder = tf.placeholder(tf.float32,
														shape=[None,  # the size of the batch
																 voxelized_input_width,
																 voxelized_input_height,
																 voxelized_input_depth])
				self.is_training_tf = tf.placeholder_with_default(is_training, shape=[])


	# NOTE: CHECKED BY MIHAI
	# Build the graph for the encoder network
	def build_encoder(self, input_placeholder=None, reuse=None):

		# If given parameter "reuse", use it.
		# Else, use the default value from "self.reuse"
		# reuse = reuse if reuse else self.reuse

		# Define reuse as set in the parameters.
		if reuse is None:
			reuse = self.reuse

		# Use the given input_placeholder if it exists
		# Otherwise use the default input_placeholder
		if input_placeholder is None:
			input_placeholder_parameter = self.input_placeholder
		else:
			input_placeholder_parameter = input_placeholder

		self.z_means, self.z_log_vars, self.z_vars, summaries_list = \
			helpers.build_encoder(
				input_placeholder_parameter, # self.input_placeholder,
				self.latent_dim,
				self.graph,
				reuse,
				self.verbose,
				self.is_training_tf, # defines if it is Train mode or Test mode (required for batch normalisation)
				self.is_training_py) # defines if it is Train mode or Test mode (required for batch normalisation)

		self.minibatch_summ_list.extend(summaries_list)

	# NOTE: CHECKED BY MIHAI
	# Build the graph for the decoder network
	def build_decoder(self, reuse=None):

		# Define reuse as set in the parameters.
		if reuse is None:
			reuse = self.reuse

		self.logits, summaries_list = helpers.build_decoder(self.z,
															self.latent_dim,
															self.graph,
															reuse,
															self.verbose,
															self.is_training_tf,
															self.is_training_py)
		self.minibatch_summ_list.extend(summaries_list)

		# Reconstructions based on what the (Gaussian) prior
		# i.e. see what a Gaussian noise (in the latent layer) corresponds to as output
		self.logits_prior, _ = helpers.build_decoder(	self.z_prior,
														self.latent_dim,
														self.graph,
														True, # reuse=True; force the reuse of the same graph
														self.verbose,
														self.is_training_tf,
														self.is_training_py)

	# NOTE: CHECKED BY MIHAI
	# Return: void
	def build_model(self, input_placeholder=None, reuse=None, name='vae'):

		# Reuse variables if requested, otherwise use default setting
		# reuse = reuse if reuse else self.reuse
		if reuse is None:
			reuse = self.reuse

		# Build the VAE parts
		with tf.variable_scope(name_or_scope='vae', reuse=reuse):

			# Build the encoder
			if input_placeholder is None:
				self.build_encoder(reuse=reuse)
			else:
				# TODO it should be visibile here which (self) variables are filled by the build_encoder
				# The following vars get their values:
				# self.z_means, self.z_log_vars, self.z_vars, summaries_list
					self.build_encoder(input_placeholder=input_placeholder, reuse=reuse)

			# Sampling z_log_vars from a gaussian distribution with mean 0 and stddev 1
			# The z_log_vars come from the encoder, so the epsilon is related to it
			# (z_log_vars is coming from the posterior)
			eps = tf.random_normal(
														tf.shape(self.z_vars), # tf.shape(self.z_log_vars),
														 dtype=tf.float32,
														 mean=0.,
														 stddev=1.0,
														 name='epsilon')

			# self.z = tf.add(self.z_means, eps * self.z_vars / 2)
			# print("\nself.z_means.shape:")
			# print(self.z_means.shape)
			# print("\nself.z_vars.shape:")
			# print(self.z_vars.shape)
			# print("\neps shape:")
			# print(eps.shape)
			self.z = tf.add(self.z_means, eps * self.z_vars / 2)


			# Part for generating objects from a Gaussian distribution (0,1)
			self.z_prior = eps

			# Tensorboard
			self.minibatch_summ_list.append(self.hist_summary("latent_variable", self.z))

			# Build the decoder
			self.build_decoder(reuse)
			with self.graph.as_default():
				self.reconstructed_samples = tf.nn.sigmoid(self.logits)
				self.reconstructed_samples_from_prior = tf.nn.sigmoid(self.logits_prior)

		# Build the losses
		with tf.name_scope('losses'):

			if input_placeholder is None:
				self.reconstruction_loss_weighted()
			else:
				self.reconstruction_loss_weighted(input_placeholder=input_placeholder)

			# Create a softbits loss
			self.build_softbits_loss()
			with self.graph.as_default():
				# lossL2 = tf.add_n([tf.nn.l2_loss(v) for v in tf.trainable_variables()
				#                    if 'kernel' in v.name]) * regularizer
				# self.total_loss = tf.add_n([self.reconstruction_loss, self.prior_loss, lossL2])
				self.total_loss = tf.add_n([self.reconstruction_loss, self.prior_loss])

	# Return the variable initialiser
	def get_init(self):
		with self.graph.as_default():
			with tf.name_scope('initialization_op'):
				return tf.global_variables_initializer()

	# TODO write description
	def reconstruction_loss_weighted(
				self,
				input_placeholder=None,
				pos_weight=10.0):
		#
		with self.graph.as_default():
			with tf.name_scope('reco_loss'):
				pos_weight_value = tf.constant(pos_weight)
				if input_placeholder is None:
					# print('WE ARE SQUEEZING THIS A:') # last dimension = number of features
					# print(self.logits.shape)

					self.reconstruction_loss = \
					model_vae_3d.reconstruction_loss_weighted(
								# tf.squeeze(self.logits, axis=[-1]), # last dimension = number of features
								self.logits,
								self.input_placeholder,
								pos_weight_value)
				else:
					self.reconstruction_loss = \
					model_vae_3d.reconstruction_loss_weighted(
								# tf.squeeze(self.logits, axis=[-1]), # last dimension = number of features
								self.logits,
								input_placeholder,
								pos_weight_value)

				self.continuous_summ_list.append(
						self.scalar_summary('reco_loss', self.reconstruction_loss))

	# Create a softbits loss
	def build_softbits_loss(self):
		with self.graph.as_default():
			with tf.name_scope('prior_loss'):
				gamma_factor = tf.Variable(
					initial_value = np.ones(self.latent_dim) * 1e-2,
					trainable = False,
					name = "gamma_factor",
					dtype = tf.float32)

				kl_loss_per_dim = model_vae_3d.kl_loss_per_dim(
															self.z_means,
															self.z_log_vars)
				self.hist_summary('kl_with_prior', kl_loss_per_dim)

				lambda_per_dim = 0.01
				information_threshold = 0.1
				gamma_rate = 0.1

				factor_1 = tf.where(
						kl_loss_per_dim < (1 - information_threshold) * lambda_per_dim,
						tf.ones_like(gamma_factor) * (1. - gamma_rate),
						tf.ones_like(gamma_factor))

				factor_2 = tf.where(
						kl_loss_per_dim > (1 + information_threshold) * lambda_per_dim,
						tf.ones_like(gamma_factor) * (1. + gamma_rate),
						tf.ones_like(gamma_factor))

				factor = tf.multiply(factor_1, factor_2)

				gamma_update_op = gamma_factor.assign(tf.minimum(tf.multiply(gamma_factor, factor), tf.ones_like(gamma_factor)))
				# gamma_factor = tf.where(kl_loss_per_dim < (1 - information_threshold) * lambda_per_dim,
				#                         gamma_factor * (1 - gamma_rate),
				#                         gamma_factor)
				with tf.control_dependencies([gamma_update_op]):
					self.minibatch_summ_list.append(self.hist_summary("gamma_factor", gamma_factor))
					self.prior_loss = tf.reduce_sum(tf.multiply(gamma_factor, kl_loss_per_dim))

				self.continuous_summ_list.append(self.scalar_summary("prior_loss", self.prior_loss))


	def get_saver(self, var_list=None):
		with tf.name_scope('saver'):
			with self.graph.as_default():
				self.saver = tf.train.Saver(var_list)


	def _device_agnostic_train(	self,
								 training_operation,
								 quick_test,
								 train_dir,
								 continue_train=False,
								 sess=None,
								 epochs_to_train=100,
								 batch_size=32,
								 save_every=5):

		if not isinstance(training_operation, list):
			training_operation = [training_operation]

		sess_close = False
		if sess is None:
			config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
			run_options = tf.RunOptions(report_tensor_allocations_upon_oom=True)
			config.gpu_options.allow_growth = True
			# making sure Tensorflow doesn't overflow the GPU
			config.gpu_options.per_process_gpu_memory_fraction = 0.95  # TODO use a user-given parameter here
			sess = tf.Session(config=config, graph=self.graph)
			sess_close = True

		with self.graph.as_default():
			with tf.name_scope('epoch_performance'):
				prior_loss_ph = tf.placeholder(tf.float32, shape=None, name='KL_summary')
				self.epoch_summ_list.append(self.scalar_summary('KL_epoch', prior_loss_ph))
				reco_loss_ph = tf.placeholder(tf.float32, shape=None, name='reco_loss_summary')
				self.epoch_summ_list.append(self.scalar_summary('reco_loss_epoch', reco_loss_ph))
				total_loss_ph = tf.placeholder(tf.float32, shape=None, name='total_loss_summary')
				self.epoch_summ_list.append(self.scalar_summary('total_loss_epoch', total_loss_ph))

			merged_epoch = self.merge_summary(self.epoch_summ_list)
			merged_minibatch = self.merge_summary(self.minibatch_summ_list)
			merged_continuous = self.merge_summary(self.continuous_summ_list)
			initializable_vars = tf.trainable_variables() + \
													 [v for v in tf.global_variables()
														if (('B_N' in v.name and 'Adam' not in v.name) or v.name.endswith(
														 'gamma_factor:0')) and v not in tf.trainable_variables()] + \
													 [self.global_step]
			self.get_saver(initializable_vars)

		iterator_train, num_train_samples, \
		iterator_test, num_test_samples, \
		dataset_void_container = helpers.get_iterators(batch_size,
																									 quick_test=quick_test,
																									 verbose=self.verbose)
		# Use this parameter to load the values from checkpoint
		if continue_train:
			with self.graph.as_default():
				# import pdb;pdb.set_trace()
				self.saver.restore(sess, tf.train.latest_checkpoint(train_dir))
				vars_to_init = [v for v in tf.global_variables() if v not in initializable_vars] # stateful optimizer specific variables
				sess.run(tf.variables_initializer(var_list=vars_to_init)) #, options=run_options)
		else:
			# Clean the train directory and start anew
			if tf.gfile.Exists(train_dir):
				tf.gfile.DeleteRecursively(train_dir)
			tf.gfile.MakeDirs(train_dir + '/train')
			init = self.get_init()
			sess.run(init)

		train_writer = tf.summary.FileWriter(train_dir + '/train', graph=sess.graph, session=sess)
		test_writer = tf.summary.FileWriter(train_dir + '/test', graph=sess.graph, session=sess)

		total_train_batches = int(math.ceil(num_train_samples / batch_size))
		total_test_batches = int(math.ceil(num_test_samples / batch_size))

		# print("Global step:")
		# print(self.global_step)
		# print("Shape:")
		# print(self.global_step.shape)
		current_step = sess.run(self.global_step) # in number of batches seen
		current_step = int(current_step) # convert to integer
		# current_step = int(current_step / (total_train_batches + total_test_batches))
		gpus_list = self.get_available_gpus()
		current_epoch = int(current_step / (total_train_batches * len(gpus_list)))
		final_epoch = int(current_step + epochs_to_train)
		print("Global step:")
		print(current_step)
		for epoch_index in range(current_epoch, final_epoch):
			epoch_train_loss = 0.0
			epoch_test_loss = 0.0
			epoch_train_pl = 0.0
			epoch_test_pl = 0.0
			epoch_train_rl = 0.0
			epoch_test_rl = 0.0
			for batch_index in range(total_train_batches):
				percentage = (batch_index + 1) / total_train_batches
				timenow = time.strftime("%Y/%m/%d %H:%M:%S", time.gmtime(time.time()))
				print("Epoch {0}: {1}% (train) : Batch {2} / {3} \t {4} \t \n".format(
					epoch_index, int(percentage * 100), batch_index + 1, total_train_batches, timenow), end='')
				batch_x_train = iterator_train.next()
				feed_dict = {self.input_placeholder: batch_x_train,
										 self.is_training_tf: True}
				if batch_index % 10 == 0:
					outputs = sess.run(training_operation + [self.prior_loss,
																									 self.reconstruction_loss,
																									 self.total_loss,
																									 merged_minibatch,
																									 merged_continuous],
														 feed_dict=feed_dict)
					p_l, r_l, t_l, summary_minibatch, summary_cont = outputs[-5:]
					train_writer.add_summary(summary_minibatch, epoch_index * total_train_batches + batch_index)
				else:
					outputs = sess.run(training_operation + [self.prior_loss,
																									 self.reconstruction_loss,
																									 self.total_loss,
																									 merged_continuous], feed_dict=feed_dict)
					p_l, r_l, t_l, summary_cont = outputs[-4:]

				train_writer.add_summary(summary_cont, epoch_index * total_train_batches + batch_index)
				epoch_train_loss += t_l * len(batch_x_train)
				epoch_train_pl += p_l * len(batch_x_train)
				epoch_train_rl += r_l * len(batch_x_train)

			epoch_train_loss /= num_train_samples
			epoch_train_pl /= num_train_samples
			epoch_train_rl /= num_train_samples
			print('Epoch {0} train loss: {1}\n'.format(epoch_index, epoch_train_loss))
			epoch_summ = sess.run(merged_epoch, feed_dict={prior_loss_ph: epoch_train_pl,
																										 reco_loss_ph: epoch_train_rl,
																										 total_loss_ph: epoch_train_loss})
			train_writer.add_summary(epoch_summ, epoch_index)

			if epoch_index % save_every == 0:
				self.saver.save(
					sess,
					opjoin(train_dir, 'vae-model'), # save path
					global_step=epoch_index)
				for batch_index in range(total_test_batches):
					percentage = batch_index / total_test_batches
					timenow = time.strftime("%Y/%m/%d %H:%M:%S", time.gmtime(time.time()))
					print("Epoch {0}: {1}% (test) : Batch {2} / {3} \t {4} \t".format(
						epoch_index,
						int(percentage * 100),
						batch_index + 1,
						total_test_batches,
						timenow), end='')
					batch_x_test = iterator_test.next()
					feed_dict = {self.input_placeholder: batch_x_test, self.is_training_tf: False}
					p_l, r_l, t_l = sess.run(fetches=[self.prior_loss,
																						self.reconstruction_loss,
																						self.total_loss], feed_dict=feed_dict)
					print("Test batch loss: {0}".format(t_l))
					epoch_test_loss += t_l * len(batch_x_test)
					epoch_test_pl += p_l * len(batch_x_test)
					epoch_test_rl += r_l * len(batch_x_test)

				epoch_test_loss /= num_test_samples
				epoch_test_pl /= num_test_samples
				epoch_test_rl /= num_test_samples
				print('Epoch {0} test loss: {1}\n'.format(epoch_index, epoch_test_loss))
				epoch_summ = sess.run(merged_epoch, feed_dict={prior_loss_ph: epoch_test_pl,
																											 reco_loss_ph: epoch_test_rl,
																											 total_loss_ph: epoch_test_loss})
				test_writer.add_summary(epoch_summ, epoch_index)
				# void part
				batch_void_volume = np.array(dataset_void_container)
				batch_void_volume = np.broadcast_to(batch_void_volume,
																						shape=[2] + list(batch_void_volume.shape[1:]))
				feed_dict = {self.input_placeholder: batch_void_volume}
				void_loss = sess.run(fetches=self.total_loss, feed_dict=feed_dict)
				print("Epoch {0} Void batch loss: {1}\n".format(epoch_index, void_loss))
				# A dictionary matching reconstruction types with batches of the corresponding type
				# Dictionary
				reconstruction_types_and_batches_dictionary = {'train': batch_x_train,
										'test': batch_x_test,
										'void': batch_void_volume}

				# There are several types of reconstructions:
				# 1. from prior (from random)
				# 2a. from posterior train (train set)
				# 2b. from posterior test (test set)
				# 3. from void (reconstruction of the void)

				# 1. Get the reconstructed samples from a prior gaussian distribution
				feed_dict = {self.input_placeholder: reconstruction_types_and_batches_dictionary['train'], # uses the train samples only to know the size of the batch
										 self.is_training_tf: False}
				reco_samples_from_gaussian_prior = \
					sess.run(self.reconstructed_samples_from_prior,
									 feed_dict=feed_dict)
					# sess.run(tf.squeeze(self.reconstructed_samples_from_prior, axis=[-1]), feed_dict=feed_dict)
				# Save these reconstructions from a random gaussian distribution
				reconstructions_dir = opjoin(train_dir,
								 'reconstructions_epoch_{}'.format(epoch_index),
								 'gaussian_prior')
				tf.gfile.MakeDirs(reconstructions_dir)
				print("reco_samples_from_gaussian_prior shape: ")
				print(reco_samples_from_gaussian_prior.shape)

				tools_reconstructions.save_reconstructed_samples(
						reconstructions_dir,
						reco_samples_from_gaussian_prior[0][np.newaxis], # input
						epoch_index)

				# 2. Reconstruction from posterior
				for reco_type in reconstruction_types_and_batches_dictionary.keys():
					reconstructions_dir = opjoin(
						train_dir,
						'reconstructions_epoch_{}'.format(epoch_index),
						reco_type)
					tf.gfile.MakeDirs(reconstructions_dir)
					feed_dict = {self.input_placeholder: reconstruction_types_and_batches_dictionary[reco_type],
											 self.is_training_tf: False}
					# print("self.reconstructed_samples before squeeze: ")
					# print(self.reconstructed_samples.shape)
					# Get the reconstructed samples (from z obtained from the encoder)
					reco_samples_from_posterior = \
							sess.run(self.reconstructed_samples,
											 feed_dict=feed_dict)
						# sess.run(tf.squeeze(self.reconstructed_samples, axis=[-1]),
					print("self.reconstructed_samples after squeeze: ")
					print(self.reconstructed_samples.shape)
					print("reco_samples_from_posterior shape:")
					print(reco_samples_from_posterior.shape)

					# 3. Reconstructing from void
					if 'void' in reco_type:
						print("\n\nreco_samples_from_posterior.shape = ")
						print(reco_samples_from_posterior.shape)
						print("\n\n")
						# TODO clear the mess here
						tools_reconstructions.save_reconstructed_samples_pairs(
								reconstructions_dir,
								reco_samples_from_posterior[0][np.newaxis],
								reconstruction_types_and_batches_dictionary[reco_type][0][np.newaxis],
								epoch_index)

					else:
						print("\n\nreco_samples_from_posterior.shape = ")
						print(reco_samples_from_posterior.shape)
						print("\n\n")
						# reco_samples_from_posterior = reco_samples_from_posterior.reshape((-1, 64, 64, 64))
						tools_reconstructions.save_reconstructed_samples_non_empty(
							reconstructions_dir,
							reco_samples_from_posterior,
							reconstruction_types_and_batches_dictionary[reco_type][-reco_samples_from_posterior.shape[0]:],  # in multi-gpu, input ph is bigger than reco
							epoch_index)

		if sess_close:
			sess.close()




	def build_and_train_multi_gpu(self,
																train_dir,
																continue_train=False,
																sess=None,
																epochs=100,
																learning_rate=0.0004,
																batch_size=64,
																save_every=1):
		quick_test = False
		if quick_test:
			epochs = 1
			batch_size = 4

    # https://stackoverflow.com/questions/38559755/how-to-get-current-available-gpus-in-tensorflow


		gpus_list = self.get_available_gpus()
		# http://blog.s-schoener.com/2017-12-15-parallel-tensorflow-intro/

		reuse = False
		tower_grads = list()
		with self.graph.as_default():
			# Compute on CPU
			with tf.device('/cpu:0'):
				optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)

				#Prepare the data for each GPU, by splitting the input into as many parts as GPUs
				_x = tf.split(self.input_placeholder, num_or_size_splits=len(gpus_list))

			for idx, gpu in enumerate(gpus_list):
				# Assign operations to the GPU, variables to the CPU
				with tf.device(self.assign_to_device(gpu, ps_device='/cpu:0')):
					# _x = self.input_placeholder[idx * batch_size: (idx+1) * batch_size]

					self.build_model(input_placeholder=_x[idx], reuse=reuse)
					with tf.name_scope('grad_calc_gpu{}'.format(idx)):
						update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
						# Control dependencies specify what needs to be computed BEFORE
						# anything defined in the "with" block
						# Here: compute the update operations
						# 			before computing gradients
						with tf.control_dependencies(update_ops):
							grads_and_vars = optimizer.compute_gradients(self.total_loss)

					reuse = True
					# print("grads_and_vars len:")
					# print(len(grads_and_vars[0]))
					# print(len(grads_and_vars[1]))
					tower_grads.append(grads_and_vars)

			# Print total number of trainable parameters in the model
			self.print_total_parameters_number()

			with tf.device('/cpu:0'):
				with tf.name_scope('train_ops'):
					tower_grads = self.average_gradients(tower_grads)
					# Count how many times the optimizer was called (the number of steps)
					# FIXME check if this is the correct way to initialize the global_step
					# https://stackoverflow.com/questions/41166681/what-does-global-step-mean-in-tensorflow
					# global_step refers to the number of batches seen by the graph.
					# https://www.tensorflow.org/api_docs/python/tf/train/global_step
					self.global_step = tf.train.get_or_create_global_step()
					# FIXME: global_step should be replaced by epoch_index here
					training_operation = optimizer.apply_gradients(tower_grads, global_step=self.global_step)

		self._device_agnostic_train(training_operation=training_operation,
																quick_test=quick_test,
																train_dir=train_dir,
																continue_train=continue_train,
																sess=sess,
																epochs_to_train=epochs,
																batch_size=batch_size * len(gpus_list),
																save_every=save_every)


	def average_gradients(self, tower_grads_and_variables, name=""):
		average_grads = []
		# print("tower grads and variables length:")
		# print(len(tower_grads_and_variables))

		#f(*[1, 2, 3])  = f(1, 2, 3)
		#The * in a function call "unpacks" a list( or other iterable), making each of its elements a separate argument.
		# So without the *, you're doing zip( [[1,2,3],[4,5,6]] ). With the *, you're doing zip([1, 2, 3], [4, 5, 6]).
		#
		# the * unpacks the list
		# gradients and the corresponding variables
		for grad_and_vars in zip(*tower_grads_and_variables):
			grads = []
			for g, _ in grad_and_vars:
				# print("g len:")
				# print(len(g))
				# print("g shape:")
				# print(g.shape)
				expanded_g = tf.expand_dims(g, 0) #add a dimension of 1 as axis 0
				grads.append(expanded_g)

			grad = tf.concat(grads, 0)
			grad = tf.reduce_mean(grad, 0)

			# TODO why is this hard coded?
			v = grad_and_vars[0][1]
			if 'B_N' not in v.name:
				self.minibatch_summ_list.append(self.scalar_summary(name + v.name.split('/')[-2] + '_' + v.name.split('/')[-1][:-2] + '_grad_norm', tf.sqrt(tf.reduce_mean(grad ** 2))))
			grad_and_var = (grad, v)
			average_grads.append(grad_and_var)
		return average_grads

	@staticmethod
	def get_available_gpus():
		"""
				Returns a list of the identifiers of all visible GPUs.
		"""
		from tensorflow.python.client import device_lib
		local_device_protos = device_lib.list_local_devices()
		return [x.name for x in local_device_protos if x.device_type == 'GPU']

	# all variables should live on the controller device
	# all operations should run on the operating devices
		"""
	Returns a function to place variables on the ps_device.

	Args:
		device: Device for everything but variables (e.g. operations)
		ps_device: Device to put the variables on. Example values are /GPU:0 and /CPU:0.

	 If ps_device is not set then the variables will be placed on the default device.
	 The best device for shared varibles depends on the platform as well as the
	 model. Start with CPU:0 and then test GPU:0 to see if there is an
	 improvement.
	"""
	@staticmethod
	def assign_to_device(device, ps_device='/cpu:0'):
		# http://blog.s-schoener.com/207-12-15-parallel-tensorflow-intro/
		#
		# Tensorflow versions of variables
		# Tensorflow has variables and operations.
		# They are specified by a string name.
		# All variables must be placed on the CPU
		# All operations must be placed on the GPU
		# Variable strings can be one of these three:
		# (legacy code, don't think about it)
		PS_OPS = ['Variable', 'VariableV2', 'AutoReloadVariable']
		def _assign(op):
			node_def = op if isinstance(op, tf.NodeDef) else op.node_def
			if node_def.op in PS_OPS:
				return "/" + ps_device
			else:
				return device
		return _assign

	# https://stackoverflow.com/questions/38160940/how-to-count-total-number-of-trainable-parameters-in-a-tensorflow-model
	# Count the total number of trainable parameters in the model
	def print_total_parameters_number(self):
		print("\n---Total number of trainable parameters: ")
		total_parameters = 0
		for variable in tf.trainable_variables():
				# shape is an array of tf.Dimension
				shape = variable.get_shape()
				print(shape)
				# print(len(shape))
				variable_parameters = 1
				for dim in shape:
						# print(dim)
						variable_parameters *= dim.value
				print(variable_parameters)
				total_parameters += variable_parameters
		print("Total trainable parameters in model:")
		print(total_parameters)

if __name__ == "__main__":
	mymodel = VAE(verbose=False)
	# mymodel.build_model()
	mymodel.build_and_train_multi_gpu(train_dir='/media/Data/softbits2gpu')
