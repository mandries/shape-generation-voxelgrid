from __future__ import print_function, absolute_import, unicode_literals, division
import tensorflow as tf
import nn_blocks_enc_dec_dis_iters as helpers
import time # to know how long it takes for the code to run
import tools_arg_parser # to parse arguments to the program
# import model_vae_3d as mvd
# import numpy as np
# import time
# from os.path import join as opjoin
import os
# import tools_reconstructions
from model_vae_3d_class import VAE
import random # for random number generation (used to slow down the discriminator learning)


#voxelized_input_width = 64
#voxelized_input_height = 64
#voxelized_input_depth = 64

#nn_img_input_channels = 1  # 4 # (3 color + 1 depth)

#regularizer = 1e-3
saving_directory = '/media/Data/mihai/models/vaegan/'

# TODO:
# Include option to train
# a) VAE from scratch/savepoint
# b) VAE-GAN from VAE savepoint

TRAIN_VAE = 0
TRAIN_VAEGAN = 1
TRAINING_TYPE = TRAIN_VAE #TRAIN_VAEGAN

#####################################
# Structure and logic of this code: #
# TODO : write it here (describe the desired flow of the program)
#
#

class VAEGAN(VAE):
  def __init__(self, *args, **kwargs):
    # Declare all variables and placeholders
    super(VAEGAN, self).__init__(*args, **kwargs)

  # build_decoder
  def build_decoder(self, *args, **kwargs):
    super(VAEGAN, self).build_decoder(*args, **kwargs)

    eps = tf.random_normal(tf.shape(self.z_log_vars),
                           dtype=tf.float32,
                           mean=0.,
                           stddev=1.0,
                           name='epsilon_epsilon')
    self.prior_logits, _ = helpers.build_decoder(eps,
                                                 self.latent_dim,
                                                 self.graph,
                                                 True,  #reuse
                                                 self.verbose,
                                                 self.is_training_tf,
                                                 self.is_training_py)


  def discriminator(self, input_placeholder=None, reuse=None):

    reuse = reuse if reuse else self.reuse

    print("\n\nComputing judgement on reconstructed samples")
    self.reconstructed_judgement, self.reconstructed_feats, summaries_list = \
      helpers.build_discriminator(self.reconstructed_samples,
                                  self.graph,
                                  reuse,
                                  self.verbose,
                                  self.is_training_tf,
                                  self.is_training_py)

    if input_placeholder is None:
      print("\n\nComputing judgement on real samples (if input placeholder is None)")
      # Decision on real samples
      self.real_judgement, self.real_feats, _ = \
      helpers.build_discriminator(
        self.input_placeholder,
        self.graph,
        True, # reuse
        self.verbose,
        self.is_training_tf,
        self.is_training_py)
    else:
      print("\n\nComputing judgement on real samples (if input placeholder is not None)")
      # Decision on real samples
      self.real_judgement, self.real_feats, _ = \
      helpers.build_discriminator(
        input_placeholder,
        self.graph,
        True, # reuse
        self.verbose,
        self.is_training_tf,
        self.is_training_py)

    with self.graph.as_default():
      self.prior_voxels = tf.nn.sigmoid(self.prior_logits)

    print("\n\nComputing judgement on prior samples")
    # Decision on generated samples
    self.prior_judgement, _, _ = \
        helpers.build_discriminator(
            self.prior_voxels,
            self.graph,
            True, # reuse
            self.verbose,
            self.is_training_tf,
            self.is_training_py)

    self.minibatch_summ_list.extend(summaries_list)


  # Computes the
  # 1. Discriminator (judgement loss)
  # 2. Decoder loss (content loss, style loss)
  # and stores these losses in "self" object
  # *. Also computes stuff for tensorboard
  def build_GAN_loss(self):
    with self.graph.as_default():
      with tf.name_scope('GAN_loss'):
        self.judgement_loss = \
        tf.reduce_mean(-1.*(
            tf.log(tf.clip_by_value(self.real_judgement,1e-5,1.0)) +
            tf.log(tf.clip_by_value(1.0 - self.prior_judgement,1e-5,1.0)) +
            tf.log(tf.clip_by_value(1.0 - self.reconstructed_judgement,1e-5,1.0))))
        # Tensorboard
        self.continuous_summ_list.append(self.scalar_summary("judgement_loss", self.judgement_loss))
        # TODO Style loss = Loss for fooling the discriminator (applied to the decoder)
        self.decoder_style_loss = tf.reduce_mean(-1.*(
            tf.log(tf.clip_by_value(self.prior_judgement,1e-5,1.0)) +
            tf.log(tf.clip_by_value(self.reconstructed_judgement, 1e-5, 1.0))))
        # TODO Content loss = Loss (applied to the decoder)
        # Content loss
				# Calculated as difference between extracted features of originals vs reconstructions
        self.decoder_content_loss = tf.reduce_mean(
            tf.reduce_sum(
                tf.square(self.real_feats - self.reconstructed_feats),
                axis=[1, 2, 3, 4]))

        # TODO: "Autoencoding beyond pixels"
        # Tensorboard
        self.continuous_summ_list.append(
            self.scalar_summary("dec_style_loss", self.decoder_style_loss))
        self.continuous_summ_list.append(
            self.scalar_summary("dec_content_loss", self.decoder_content_loss))


  def build_model(self, *args, **kwargs):
    kwargs['name'] = 'vaegan'
    # Build the encoder + decoder
    super(VAEGAN, self).build_model(*args, **kwargs)

    # kwargs = keyword arguments passed to the build_model() and discriminator()
    reuse = kwargs['reuse'] if 'reuse' in kwargs else self.reuse
    if 'input_placeholder' not in kwargs:
      kwargs['input_placeholder'] = None

    with tf.variable_scope(name_or_scope=kwargs['name'], reuse=reuse):
      self.discriminator(kwargs['input_placeholder'])

    with tf.name_scope('losses'):
      self.build_GAN_loss()

  # TODO:
  # modify the coefficients of each component of the loss (weights)
  # and the update frequency of different parts of the network (update the discriminator less often than the generator)
  # and different learning rates (now: 0.01 for all of them)
  # try with log differences (0.1, 0.01, 0.001)
  # Discriminator should have a lower learning rate
  def build_and_train_multi_gpu(self,
            train_dir,
            continue_train=False,
            sess=None,
            epochs=100,
            learning_rate=0.0004,
            batch_size=64,
            save_every=5):

    # TODO add these as parameters
    # Slow done the learning rate of the discriminator, as compared to the lr of the generator
    learning_rate_discriminator = learning_rate * 0.1 # learning rate of the discriminator

    # TODO transform this into script parameters
    # quick_test = True #False
    if (self.is_quick_test):
      epochs = 1 #20
      batch_size = 4 #8 #32

    reuse = False
    gpus_list = self.get_available_gpus()
    # FIXME: check if this one is used correctly
    tower_grads_discriminator = list()
    tower_grads_decoder = list()
    tower_grads_encoder = list()

    with self.graph.as_default():
      # Define the learning rate
      # TODO use different learning rates here; optimize them with ADAM?
      lr = tf.placeholder_with_default(learning_rate, shape=[])
      lr_discriminator = tf.placeholder_with_default(learning_rate_discriminator, shape=[])
      # Optimize the discriminator, decoder, and encoder.
	  # Epsilon value should be in range [0.1, 1]
	  # https://stackoverflow.com/questions/43221065/how-does-paramater-epsilon-affects-adamoptimizer
      opt_discriminator = tf.train.AdamOptimizer(lr_discriminator, epsilon=1.0)
      opt_decoder = tf.train.AdamOptimizer(lr, epsilon=1.0)
      opt_encoder = tf.train.AdamOptimizer(lr, epsilon=1.0)
      #
      with tf.device('/cpu:0'):
        # LL_param = tf.placeholder(tf.float32)
        # G_param = tf.placeholder(tf.float32)
        # Split the input data into as many splits as there are GPUs
        _x = tf.split(self.input_placeholder, num_or_size_splits=len(gpus_list))
        #
        for idx, gpu in enumerate(gpus_list):
          with tf.device(self.assign_to_device(gpu, ps_device='/cpu:0')):
            with tf.name_scope('grad_calc_gpu{}'.format(idx)) as scope:
              # Build the model
              self.build_model(input_placeholder=_x[idx], reuse=reuse)

              # Print total number of trainable parameters in the model
              self.print_total_parameters_number()

              if not reuse: # trainable variables are shared, so we only collect them once
                params = tf.trainable_variables()
                discriminator_params = [i for i in params if 'discriminator' in i.name]
                decoder_params = [i for i in params if 'decoder' in i.name]
                encoder_params = [i for i in params if 'encoder' in i.name]

              # Clip the losses

              # Train the discriminator every 5 cycles, to let the generator time to improve.
              #if (self.global_step % 5 == 0):
              L_discriminator = tf.clip_by_value(self.judgement_loss, -100, 100)
              #else: #
              #  L_discriminator = tf.clip_by_value(self.judgement_loss, 0, 0)

              # TODO: add here a factor to one part of the summation
              L_decoder = tf.clip_by_value(tf.add(self.decoder_content_loss, self.decoder_style_loss), -100, 100)
              # TODO: add here a factor to one part of the summation
              L_encoder = tf.clip_by_value(tf.add(self.prior_loss, self.decoder_content_loss), -100, 100)

              # These are the update operations for batch normalisation for this GPU (defined by the "scope" parameter)
              update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope)

              # Compute the gradients
              with tf.control_dependencies([ops for ops in update_ops if 'discriminator' in ops.name]):
                grads_discriminator = opt_discriminator.compute_gradients(L_discriminator, var_list=discriminator_params)

              # Update operations for updating the variables of Batch_normalisation
              # FIXME here the update operations may affect only 1 GPU out of many
              # But the "scope" was taken care of before
              # i.e. the update operations may not match the corresponding GPU
              # Atabak suspects that when it updates operations for GPU 2, it re-updates variables for GPU 1, which is wrong
              with tf.control_dependencies([ops for ops in update_ops if 'decoder' in ops.name]):
                # A. Version which is equivalent to train the full VAE-GAN
                if (TRAINING_TYPE == TRAIN_VAEGAN):
                    grads_decoder = opt_decoder.compute_gradients(L_decoder, var_list=decoder_params)
                # B. Version which is equivalent to training only the VAE part of the VAEGAN network:
                elif (TRAINING_TYPE == TRAIN_VAE):
                    grads_decoder = opt_decoder.compute_gradients(self.total_loss, var_list=decoder_params)

              with tf.control_dependencies([ops for ops in update_ops if 'encoder' in ops.name]):
                # A. Version which is equivalent to train the full VAE-GAN
                if (TRAINING_TYPE == TRAIN_VAEGAN):
                    grads_encoder = opt_encoder.compute_gradients(L_encoder, var_list=encoder_params)
                # B. Version which is equivalent to training only the VAE part of the VAEGAN network:
                elif (TRAINING_TYPE == TRAIN_VAE):
                    grads_encoder = opt_encoder.compute_gradients(self.total_loss, var_list=encoder_params)

          # Store the gradients
          reuse = True
          tower_grads_discriminator.append(grads_discriminator)
          tower_grads_decoder.append(grads_decoder)
          tower_grads_encoder.append(grads_encoder)

        # # Write the graph to file
        # with tf.Session() as sess:
        #   # `sess.graph` provides access to the graph used in a <a href="./../api_docs/python/tf/Session"><code>tf.Session</code></a>.
        #   writer = tf.summary.FileWriter(train_dir, sess.graph)
        #   writer.close();

        # Average the gradients of the GPUs, to have a single gradient
        with tf.name_scope('train_ops'):
          # Discriminator
          grads_discriminator = self.average_gradients(tower_grads_discriminator, name="dis")
          # Decoder
          grads_decoder = self.average_gradients(tower_grads_decoder, name="dec")
          # Encoder
          grads_encoder = self.average_gradients(tower_grads_encoder, name="enc")
          # TODO ?
          self.global_step = tf.train.get_or_create_global_step()

          # Slow down the learning of the discriminator
          # update_frequency = 1.0 / 4.0 # update every 4th iteration
          # decision = random.random() # Pick a number in range [0.0, 1.0]
          # if (update_frequency < decision):
          # if (self.global_step % 5 == 0):

            # TODO put back the discriminator loss when training the whole VAE-GAN
            # if (TRAINING_TYPE == TRAIN_VAEGAN):
          train_discriminator = opt_discriminator.apply_gradients(grads_discriminator, global_step=self.global_step)

          # TODO ?
          train_decoder = opt_decoder.apply_gradients(grads_decoder, global_step=self.global_step)

          # TODO ?
          train_encoder = opt_encoder.apply_gradients(grads_encoder, global_step=self.global_step)

    # TODO also train the discriminator when training the full VAE-GAN
    if (TRAINING_TYPE == TRAIN_VAEGAN):
        if (self.global_step % 5 == 0):
            training_operation_arg = [train_discriminator, train_decoder, train_encoder]
        else:
            training_operation_arg = [train_decoder, train_encoder]
    # Train only the decoder+encoder when training the VAE (without the GAN)
    elif (TRAINING_TYPE == TRAIN_VAE):
        training_operation_arg = [train_decoder, train_encoder]

    # TODO ?
    # import pdb;pdb.set_trace()
    self._device_agnostic_train(
        # TODO also train the discriminator when training the full VAE-GAN
        # training_operation=[train_discriminator, train_decoder, train_encoder],
        # Train only the decoder+encoder when training the VAE (without the GAN)
        # training_operation=[train_decoder, train_encoder],
        training_operation=training_operation_arg,
        quick_test=self.is_quick_test,
        train_dir=train_dir,
        continue_train=continue_train,
        sess=sess,
        epochs_to_train=epochs,
        batch_size=batch_size * len(gpus_list),
        save_every=save_every)


# Allow the file to be used as a library
# (call main only if this file was executed)
if __name__ == "__main__":

    # Parse the arguments of the program
    parser = tools_arg_parser.build_parser()
    options = parser.parse_args()
    # Apply the options entered as arguments
    tools_arg_parser.apply_options(options)

    # TRAIN_VAE = 0
    # TRAIN_VAEGAN = 1
    # TRAINING_TYPE = TRAIN_VAEGAN
    # TRAINING_TYPE = TRAIN_VAE

    # Create a new instance of a VAEGAN
    mymodel = VAEGAN(verbose=True)
    # mymodel.build_model()
    mymodel.is_quick_test = options.is_quick_test
    print("Quick test is {}".format(mymodel.is_quick_test))

    # Create the folder where reconstructions will be saved
    folder_name = "exp_" + str(time.time())
    directory = saving_directory + folder_name
    if not os.path.exists(directory):
        os.makedirs(directory)

        # TODO specify here the correct path
    # /media/Data/mihai/models/vaegan/
    # mymodel.build_and_train_multi_gpu(train_dir=directory)

    # print("Loading model from directory: " + directory)
    # Train the VAEGAN from the savepoint where the VAE training stopped
    # if (TRAINING_TYPE == TRAIN_VAEGAN):
        # TODO remove this hard-coded value assignment after you use it for testing
        # directory = '/media/Data/mihai/models/vaegan/exp_1545239510.8'
        # directory = '/tmp/mihai/models/vaegan/exp_1545239510.8'
        # directory = '/tmp/mihai/vaegan/exp_1545239510.8'
        # mymodel.build_and_train_multi_gpu(
        #     train_dir=directory,
        #     continue_train=True)
    # Train from scratch the VAE
    # elif (TRAINING_TYPE == TRAIN_VAE):
        # directory = '/tmp/mihai/vaegan/exp_1545239510.8'
    mymodel.build_and_train_multi_gpu(
        train_dir=directory,
        continue_train=False)#True)
