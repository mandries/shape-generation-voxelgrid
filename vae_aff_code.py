# This Variational AutoEncoder learns a latent representation for
# objects from the input datasets.
# TODO: It should work on depth images of size:
# 512 x 424 x 4 (kinect 2.0 depth resolution)
# former: images of size 320x200 (color ones, 3 colors)

from __future__ import division, print_function, absolute_import
#~ from array import *
#~ from random import shuffle

import argparse # for parsing arguments
import sys # for counting the number of arguments

import numpy as np
#import matplotlib.pyplot as plt
#~ import scipy
#~ import scipy.stats
#~ from scipy.stats import norm
import tensorflow as tf
# import time # for pausing the program
import os # to walk through directories, to rename files
import pcl_tools

import time # to know how long it takes for the code to run
# Libraries for working with images
#import cv2

# Libraries for working with point clouds
# import pcl # http://pointclouds.org/news/tags/python


# The welcome message
print("Start: AutoEncoder for Learning affordances. Program is running")

# Introduce the argument parser (useful if you want to load pre-learned parameters, or start learning from zero)
parser = argparse.ArgumentParser()
parser.add_argument("-r", "--restore_params",
                help="Restore the parameters in the graph from the given input file.",
                action="store_true")
parser.add_argument("-s", "--save_params",
                help="Save the parameters in the graph to some output file.",
                action="store_true")
args = parser.parse_args()

if (args.restore_params):
    print ("Restore params enabled")
else:
    print ("Restore params disabled: learning from zero")

saver = 0
if (args.save_params):
    print("Saving parameters enabled")
else:
    print("Saving parameters disabled")


# sys.exit(0)

################################################################################
################################## Constants ###################################
################################################################################

##### Constants #####
# The array containing the (3d) image data
dataset_container = []
# The counter used for counting the number of images in the dataset
dataset_image_count = 0

# Parameters
learning_rate = 0.001
total_epochs = 10000
batch_size = 2

# Network Parameters
latent_dim = 512

#inv_frequency_of_hallucinations_generation = 100 # check reconstruction quality every N batches (from noise)
#total_hallucinations_generated_per_check = 20
inv_frequency_of_reconstruction_evaluations = 2 # check reconstruction quality every N batches (from real images)
inv_frequency_of_parameters_saving = 10

### Dataset data ###
# Description of the input
voxelized_input_width = 64
voxelized_input_height = 64
voxelized_input_depth = 64

# nn_img_input_width = 320 #28
# nn_img_input_height = 200 #28
nn_img_input_channels = 1 # 4 # (3 color + 1 depth)

vislab_dataset_path = './pcd_files/'
input_file_format_suffix = '.pcd' # point cloud (is not voxelized representation)

# Model saving
model_saving_path = 'vae_model_feature_learning'
model_saving_path_backup = model_saving_path + '_auto_backup.ckpt'
model_saving_path = model_saving_path + '.ckpt'

################################################################################
################################# Functions ####################################
################################################################################

# Parse a dataset directory and pick the input dataset files
def get_dataset_files_from_path(
            dataset_path,
            dataset,
            input_file_format_suffix,
            total_input_files_processed,
            sample_input_width,
            sample_input_height,
            sample_input_depth,
            img_input_channels):
    print("\nDataset path: " + dataset_path)
	# Walk through the directory, and pick the files
    for root, dirs, files in os.walk(dataset_path): # directory
        for directory in dirs:
            print('\nFound: ' + directory + '/')
        for filename in files:
            print('\nFound: ' + filename)
            if filename.endswith(input_file_format_suffix):
                # Process the image and give it as input to the VAE
				# input_file
                global_filename = os.path.join(root, filename)
                # print("Processing:" + global_filename)
                pointcloud = pcl_tools.parse_pcd_file(global_filename)
                voxelized_pointcloud = pcl_tools.voxelize_pointcloud(
                                            pointcloud,
                                            sample_input_width,
                                            sample_input_height,
                                            sample_input_depth)
                dataset.append(voxelized_pointcloud)
                #pointcloud = voxelized_to_pointcloud(voxelized)
                #write_pointcloud_to_file(pointcloud)
        		#
                #add_file_to_dataset(voxelized_pointcloud, dataset)
                # TODO select a default orientation for the voxelized pointcloud?
                # (so as to avoid learning different variations of it)
                # Flip horizontally
				# img_h_flip = cv2.flip(img_resized, 0)
				# add_image_to_processed_dataset(img_h_flip, dataset)
                total_input_files_processed += 1
	return total_input_files_processed

# Automatic function to get randomized data batches
def data_iterator (input_dataset, batch_size):
	#
	batch_index = 0
	# Generate an index for the data [0..size]: (from, to_exclusive, step)
	indexes = np.arange(0, len(input_dataset))
	#
	while True:
		# Shuffle the indexes
		print("Data iterator: indexes shuffling...")
		np.random.shuffle(indexes)
		print("done")
		shuffled_data = input_dataset[indexes]
		# shuffled_labels ?
		# batch_size
		#
		# jumping in batch_size steps
		print("Looping through the input dataset...")
		for batch_index in range (0, len(input_dataset), batch_size):
			# batch_index = 0
			# print("batch: {0}".format(batch_index))
			samples_batch = shuffled_data[batch_index:batch_index + batch_size]
			#~ images_batch = images_batch.astype("float32")
			yield samples_batch # return a generator (a one-time usable iterator)

# Create model
def encoder_net(x): #, img_width, img_height, img_channels):
    # Reshape to match picture format [Width x Height x Channel]
    # Tensor input becomes 4-D: [Batch Size, Width, Height, Channel]
    # X = [Batch Size, Width, Height, Channel]
    #x = tf.reshape(x, shape=[-1, img_height, img_width, img_channels])

    print('\nEncoder input shape:')
    print(x.shape)

    x = tf.expand_dims(x, -1) # Now it becomes [Batch_size x (1)]
    print('\nEncoder input shape after dim expansion:')
    print(x.shape)

    # Convolution Layer 1
    # input:    (64 x 64 x 64 x 1)
    # output:   (32 x 32 x 32 x 8)
    conv1 = tf.layers.conv3d(
                inputs=x,
                filters=8,
                kernel_size=(3,3,3),
                strides=2,
                padding='same'
                )
    # Conv1 = [Batch Size, Height / 2, Width / 2, #Channels as defined by the weights of conv1]
    # Height and width are reduced by 2 because we use a stride=2

    # Apply non-linear function
    conv1 = tf.nn.relu(conv1)

    print('\nEncoder shape after conv1:')
    print(conv1.shape)

    # Convolution Layer 2
    # input:    (32 x 32 x 32 x 8)
    # output:   (16 x 16 x 16 x 64)
    conv2 = tf.layers.conv3d(
                inputs=conv1,
                filters=16,
                kernel_size=(3,3,3),
                strides=2,
                padding='same'
                )
    # Conv2 = [Batch Size, Height / 4, Width / 4, #Channels as defined by the weights of conv2]
    # Height and width are reduced by 2 because we use a stride=2

    # Apply non-linear function
    conv2 = tf.nn.relu(conv2)

    print('\nEncoder shape after conv2:')
    print(conv2.shape)


    # Convolution Layer 3
    # input:    (16 x 16 x 16 x 64)
    # output:   (8 x 8 x 8 x 256)
    # Conv3 = [Batch Size, Height / 8, Width / 8, #Channels as defined by the weights of conv3]
    conv3 = tf.layers.conv3d(
                inputs=conv2,
                filters=32, # should be 64 * 2^3 = 512
                kernel_size=(3,3,3),
                strides=2,
                padding='same'
                )

    # Apply non-linear function (not here, because we are now predicting means and log-variances)
    conv3 = tf.nn.relu(conv3)

    print('\nEncoder shape after conv3:')
    print(conv3.shape)

    # Convolution Layer 4
    # input:   (8 x 8 x 8 x 256)
    # output:  (4 x 4 x 4 X 512)
    # Conv3 = [Batch Size, Height / 8, Width / 8, #Channels as defined by the weights of conv3]
    conv4 = tf.layers.conv3d(
                inputs=conv3,
                filters=512, #512, # should be previous_filter_size * 2^3 = 4096
                kernel_size=(3,3,3),
                strides=2,
                padding='same'
                )

    print('\nEncoder shape after conv4:')
    print(conv4.shape)

    # TODO the kernel size does not cover the whole object here
    # For this reason,
    #   when randomly sampling from the obtained feature distribution functions,
    #   correlations may be lost between features that co-occur

    # Weights_for_log_var
    # Weights_for_means

    # input:
    # output: (Batch_size x 512)
    # Average pooling
    conv4 = tf.reduce_mean(
                    input_tensor=conv4,
                    axis=[1,2,3],
                    keep_dims=False
                    )
    print("Conv4 size after reduce mean: ")
    print(conv4.shape)

    # Splitting the layer into 2 branches
    means_hidden = tf.layers.dense(
                        inputs=conv4,
                        units=latent_dim,
                        activation=tf.nn.relu,
                        use_bias=True,
                        #kernel_initializer=tf.layers.xavier_initializer (use default)
                        #trainable=True,
                        name="means_hidden"
                        )
    means_layer = tf.layers.dense(
                        inputs=means_hidden,
                        units=latent_dim,
                        # activation=tf.nn.relu,
                        use_bias=True,
                        #kernel_initializer=tf.layers.xavier_initializer (use default)
                        #trainable=True,
                        name="means_layer"
                        )

    log_vars_hidden = tf.layers.dense(
                        inputs=conv4,
                        units=latent_dim,
                        activation=tf.nn.relu,
                        use_bias=True,
                        #kernel_initializer=tf.layers.xavier_initializer (use default)
                        #trainable=True,
                        name="log_vars_hidden"
                        )
    log_vars_layer = tf.layers.dense(
                        inputs=log_vars_hidden,
                        units=latent_dim,
                        # activation=tf.nn.relu,
                        use_bias=True,
                        #kernel_initializer=tf.layers.xavier_initializer (use default)
                        #trainable=True,
                        name="log_vars_layer"
                        )

    print('\nEncoder shape after splitting conv4 into means and log_vars:')
    print(means_layer.shape)
    print(log_vars_layer.shape)

    return means_layer, log_vars_layer


# z is a sample from the N(latent_mean, latent_log_var):
def decoder_net(z): # z is of size (Batch_Size x 512)

    # Aymeric tutorial (http://localhost:8888/notebooks/dcgan.ipynb)

    layer = tf.expand_dims(z, 1) # Now it becomes [Batch_size x (1) x 512]
    layer = tf.expand_dims(layer, 1) # Now it becomes [Batch_size x (1) x 1 x 512]
    layer = tf.expand_dims(layer, 1) # Now it becomes [Batch_size x (1) x 1 x 1 x 512]
    # expand_dims always puts a 1 dimension in the "second" position
    # (1 position, if you count from 0)

    print('\nDecoder layer shape at start: ')
    print(layer.shape)

    # Deconvolution,
    # image shape before:     [1 x 1 x 1 x 512]
    # image shape afterwards: [4 x 4 x 4 x 4096]
    layer = tf.layers.conv3d_transpose(
                inputs=layer,
                filters=256, #512,#4096,
                kernel_size=(4,4,4),
                strides=1,
                use_bias=False, # to avoid the conversion error, https://github.com/tensorflow/tensorflow/issues/10520
                padding='valid')  # valid = without padding
    print('Decoder layer shape after deconv 01: ')
    print(layer.shape)

    # Apply non-linear function
    layer = tf.nn.relu(layer)

    # Deconvolution,
    # image shape before:     [4 x 4 x 4 x 4096]
    # image shape afterwards: [8 x 8 x 8 x 512]
    layer = tf.layers.conv3d_transpose(
                inputs=layer,
                filters=128, #512,
                kernel_size=(5,5,5), # o = i + (k-1) # for no zero padding, unit strides
                # out_height = ceil(float(in_height - filter_height + 1) / float(strides[1]))
                # out_width  = ceil(float(in_width - filter_width + 1) / float(strides[2]))
                strides=1,
                use_bias=False,  # to avoid the conversion error, https://github.com/tensorflow/tensorflow/issues/10520
                padding='valid') # valid = without padding
    print('Decoder layer shape after deconv 02: ')
    print(layer.shape)

    # Apply non-linear function
    layer = tf.nn.relu(layer)

    # Deconvolution,
    # image shape before:     [ 8 x  8 x  8 x 512]
    # image shape afterwards: [16 x 16 x 16 x 64]
    layer = tf.layers.conv3d_transpose(
                inputs=layer,
                filters=64,
                kernel_size=(9,9,9), # o = s*(i-1) + k # for no zero padding, non-unit strides
                # out_height = ceil(float(in_height - filter_height + 1) / float(strides[1]))
                # out_width  = ceil(float(in_width - filter_width + 1) / float(strides[2]))
                strides=1,
                use_bias=False, # to avoid the conversion error, https://github.com/tensorflow/tensorflow/issues/10520
                padding='valid') # valid = without padding
    print('Decoder layer shape after deconv 03: ')
    print(layer.shape)

    # Apply non-linear function
    layer = tf.nn.relu(layer)

    # Deconvolution,
    # image shape before:     [16 x 16 x 16 x 64]
    # image shape afterwards: [32 x 32 x 32 x 8]
    layer = tf.layers.conv3d_transpose(
                inputs=layer,
                filters=8,
                kernel_size=(17,17,17), # o = s*(i-1) + k # for no zero padding, non-unit strides
                # out_height = ceil(float(in_height - filter_height + 1) / float(strides[1]))
                # out_width  = ceil(float(in_width - filter_width + 1) / float(strides[2]))
                strides=1,
                use_bias=False,  # to avoid the conversion error, https://github.com/tensorflow/tensorflow/issues/10520
                padding='valid') # valid = without padding
    print('Decoder layer shape after deconv 04: ')
    print(layer.shape)

    # Apply non-linear function
    layer = tf.nn.relu(layer)

    # Deconvolution,
    # image shape before:     [32 x 32 x 32 x 8]
    # image shape afterwards: [64 x 64 x 64 x 1]
    layer = tf.layers.conv3d_transpose(
                inputs=layer,
                filters=1,
                kernel_size=(33,33,33), # o = s*(i-1) + k # for no zero padding, non-unit strides
                # out_height = ceil(float(in_height - filter_height + 1) / float(strides[1]))
                # out_width  = ceil(float(in_width - filter_width + 1) / float(strides[2]))
                strides=1,
                use_bias=False, # to avoid the error
                padding='valid') # valid = without padding
    print('Decoder layer shape after deconv 05: ')
    print(layer.shape)

    # Apply non-linear function
    # layer = tf.nn.relu(layer)


    layer = tf.reshape(layer, [-1, 64, 64, 64])
    print('Decoder layer shape after deconv 05: ')
    print(layer.shape)

    # # Reshape the layer to [40x25x50]
    # # layer = tf.reshape(layer, shape=[-1, 25, 40, 50])
    #
    # # Deconvolution, image shape afterwards: [80 x 50 x 48]
    # layer = tf.layers.conv2d_transpose(
    #             inputs=layer,
    #             filters=48,
    #             kernel_size=(5,5),
    #             # kernel_size doesn't matter here,
    #             # because we are just doubling the size of each dimension,
    #             # and thus the kernel size doesn't really matter (but should be bigger than 2)
    #             # but it's better to use a small one (to have less parameters to optimize)
    #             strides=2,
    #             padding='same')

    # Let's try the reshaping up to our desired output image size
    # (but you could also use convolutions, which would learn something additional,
    #   as compared to the reshaping operations)

    # Apply non-linear function (not here)
    #layer = tf.nn.relu(layer)

    # Reshape the layer from [80x50x48] to [320x200x3]
    # layer = tf.reshape(layer, shape=[-1, 200, 320, 3])

    return  layer

# Define VAE Loss
def reconstruction_loss(x_reconstructed, x_true):
    # Reconstruction loss
    reconstruction_loss = tf.nn.sigmoid_cross_entropy_with_logits(
                                #_sentinel=None,
                                labels=x_true,
                                logits=x_reconstructed,
                                name="Reconstruction_loss"
                            )
    reconstruction_loss = tf.reduce_sum(reconstruction_loss)
    return reconstruction_loss

def kl_loss(z_mean, z_log_var):
    # KL Divergence loss
    kl_div_loss = 0.5 * tf.reduce_sum(tf.square(z_mean) + tf.exp(z_log_var) - z_log_var -1)
    #kl_div_loss = 1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var)
    #kl_div_loss = -0.5 * tf.reduce_sum(kl_div_loss, 1)
    #return tf.reduce_mean(encode_decode_loss + kl_div_loss)
    return kl_div_loss

################################################################################
############################# Building the graph ###############################
################################################################################

# Building the encoder
input_placeholder = tf.placeholder(
                    tf.float32,
                    shape=[
                            None, # the size of the batch
                            voxelized_input_width,
                            voxelized_input_height,
                            voxelized_input_depth
                          ]
                          # nn_img_input_channels
                    )

# Building the decoder
target_placeholder = input_placeholder # the thing we are trying to reconstruct

# Capture the output of the encoder
with tf.variable_scope("model"):
    z_mean, z_log_var = encoder_net(input_placeholder)
                            #nn_img_input_height,
                            #nn_img_input_width,
                            #nn_img_input_channels)
    print("shape of z_mean (decoder output): {}".format(z_mean.shape))
    print("shape of z_log_var (decoder output): {}".format(z_log_var.shape))
    # Sampler: Normal (gaussian) random distribution
    eps = tf.random_normal(
                tf.shape(z_log_var),
                dtype=tf.float32,
                mean=0.,
                stddev=1.0,
                name='epsilon')
    print("shape of eps (encoder output): {}".format(eps.shape))
    z = z_mean + tf.exp(z_log_var / 2) * eps
    print("shape of z (decoder input): {}".format(z.shape))

    # the actual reconstruction
    #with tf.variable_scope("model", reuse=True):
    output_voxelized_volume = decoder_net(z)



# Compute the loss
reconstruction_loss = reconstruction_loss(
                            x_reconstructed = output_voxelized_volume,
                            x_true = target_placeholder)
#
kl_loss = kl_loss(
                z_mean = z_mean,
                z_log_var = z_log_var
                )
#
total_loss = kl_loss + reconstruction_loss


#optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate)
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)

training_operation = optimizer.minimize(total_loss)

# What are these nodes?
z_mean_avg = tf.reduce_mean(z_mean, axis=0)
z_log_var_avg = tf.reduce_mean(z_log_var, axis=0)

# Placeholder for the noise that will be used for checking the quality of the reconstruction
hallucination_noise_input_placeholder = \
        tf.placeholder(tf.float32, shape=[None, latent_dim]) # None = Batch size
with tf.variable_scope("model", reuse=True):
    reconstructed_hallucinations = \
    tf.nn.sigmoid(decoder_net(hallucination_noise_input_placeholder))


# Node for reconstructed images
# (output_voxelized_volume is the output of the decoder)
# TODO probably error here, since points need to be in a certain range
reconstructed_samples_tensor = tf.nn.sigmoid(output_voxelized_volume)
# reconstructed_samples_tensor = output_voxelized_volume

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()

# The model (graph) saver (needs to be instanciated after the graph is built)
if ((args.save_params) or (args.restore_params)):
    saver = tf.train.Saver()

################################################################################
############################## Running the graph ###############################
################################################################################

# This function is OK for 3d
def save_reconstructed_samples(reconstructed_samples, original_samples, iteration_number):

    # Generate a folder to store the images
    directory = "reconstructions_iter_{0}".format(iteration_number)
    if not os.path.exists(directory):
        os.makedirs(directory)

    print('Saving reconstructed images for iteration {0}... '.format(iteration_number))
    for sample_index in range(0, reconstructed_samples.shape[0]):
        # Write the image to disk
        filename = os.path.join(   directory,
                                   "sample_{0}_recon.pcd".format(sample_index)
                                   )
        # pointcloud = voxelized_to_pointcloud(voxelized)
        # np.multiply(reconstructed_images[image_index], 255.)
        # voxelized_cloud = reconstructed_samples[sample_index]
        voxelized_cloud = reconstructed_samples[sample_index] > 0.5
        # recon = reconstructed_image>0.5
        # for x in range (voxelized_input_width):
        #     for y in range (voxelized_input_height):
        #         for z in range (voxelized_input_depth):
        #             if (voxelized_cloud[x][y][z] > 0.5):
        #                 print("{0}, {1}, {2}: {3}".format(x,y,z,voxelized_cloud[x][y][z]))
        # pointcloud = pcl_tools.voxelized_to_pointcloud(reconstructed_samples[sample_index])
        pointcloud = pcl_tools.voxelized_to_pointcloud(voxelized_cloud)
        pcl_tools.write_pointcloud_to_file(pointcloud, filename)

        filename = os.path.join(   directory,
                                   "sample_{0}_orig.pcd".format(sample_index)
                                   )
        pointcloud = pcl_tools.voxelized_to_pointcloud(original_samples[sample_index])
        pcl_tools.write_pointcloud_to_file(pointcloud, filename)

    print('done')
    return

# TODO
# Function for computing the effects of an action


# def compute_average_Gaussian_distribution_values_over_dataset(
#         z_means_list,
#         z_log_vars_list
#         ):
#
#     z_means_dataset_avg = 0.
#     z_log_vars_dataset_avg = 0.
#
#     # print("Shape of z_means_list and z_log_vars_list: ")
#     # print(len(z_means_list))
#     # print(len(z_log_vars_list))
#     # Convert the lists into numpy arrays.
#
#     z_means_array = np.array(z_means_list)
#     z_log_vars_array = np.array(z_log_vars_list)
#
#     #print("Shapes of the arrays:")
#     #print(z_means_array.shape)
#     #print(z_log_vars_array.shape)
#
#     z_means_dataset_avg = np.average(z_means_array, axis=0)
#     # TODO maybe use max here
#     z_log_vars_dataset_avg = np.average(z_log_vars_array, axis=0)
#
#     #print("Shapes of the averages:")
#     #print(z_means_dataset_avg.shape)
#     #print(z_log_vars_dataset_avg.shape)
#
#     return  z_means_dataset_avg, \
#             z_log_vars_dataset_avg
#
#     # size of computed_z_mean: 128 x 500 = batch_size x latent_layer_dim
#     # size of computed_z_log_var: 128 x 500 = batch_size x latent_layer_dim




# start_time = time.time()
# print("Program started at: {0}".format(start_time))
# print(start_time)

print("Processing the dataset files...")
# Go through the dataset
dataset_image_count = get_dataset_files_from_path(
	vislab_dataset_path,
	dataset_container,
    input_file_format_suffix,
	dataset_image_count,
    voxelized_input_width,
    voxelized_input_height,
    voxelized_input_depth,
    nn_img_input_channels)

# dataset_processing_time = time.time() - start_time
# print("Dataset processing time: ")
# time.strftime("%H:%M:%S", time.gmtime(dataset_processing_time))
# print()


# Do this before you iterate (because we need to feed in an array to tensorflow)
dataset_container = np.asarray(dataset_container)
# Checks
print("Samples in dataset (de facto): {0}".format(len(dataset_container)))
print("Samples in dataset (processed): {0}".format(dataset_image_count))

# Declare the iterator
iterator = data_iterator(dataset_container, batch_size)

#~ # Start Training
#~ # Start a new TF session
sess = tf.Session()

# Run the initializer
if (args.restore_params):
    print("Restoring model from file... ")
    saver.restore(sess, model_saving_path)
    print("done.")
else:
    sess.run(init)

# Training
# dataset_image_count = dataset_size
total_batches = np.floor(dataset_image_count/batch_size).astype(np.int16) + 1

# Initialize the lists
z_means_list = list()
z_log_vars_list = list()

for epoch_index in range(0, total_epochs):

    # if (epoch_index % inv_frequency_of_hallucinations_generation == 0):
    #     z_means_list = list()
    #     z_log_vars_list = list()
    # epoch_start_time = time.time()
    for batch_index in range(total_batches):

        print("Batch {0}".format(batch_index))
        # batch_start_time = time.time()
        # print("Dataset processing start time: {0}".format(batch_start_time))

        batch_x = iterator.next()

        # Train
        #print('Iteration {0}'.format(i))
        # print("Batch shape:")
        # print(batch_x.shape)
        feed_dict = {input_placeholder: batch_x}
        _, l = sess.run([training_operation, total_loss], feed_dict=feed_dict)
        #if i % 1000 == 0 or i == 1:
        #    print('Step %i, Loss: %f' % (i, l))
        # batch_duration = time.time() - batch_start_time
        # print("Batch processing duration:")
        # time.strftime("%H:%M:%S", time.gmtime(batch_duration))
        # print()

    # if (epoch_index % 10 == 0):
        #print('Epoch {0} batch {1} loss: {2}'.format(epoch_index, batch_index, l))
    print('Epoch {0} loss: {1}'.format(epoch_index, l))
    # epoch_duration = time.time() - epoch_start_time
    # print("Epoch processing duration:")
    # time.strftime("%H:%M:%S", time.gmtime(epoch_duration))
    # print()

    if (args.save_params):
        if (epoch_index % inv_frequency_of_parameters_saving == 0):
            # 1. Rename the previous backup, if it exists (overwrite any ancient ones)
            if (os.path.exists(model_saving_path)):
                os.rename(model_saving_path, model_saving_path_backup)
            # 2. Save the variables to disk.
            #save_path = saver.save(sess, "/tmp/model.ckpt")
            save_path = saver.save(sess, model_saving_path)
            print("(epoch {0}) Model saved in file: {1}".format(epoch_index, save_path))
            # 3. Once saved, remove the old backup
            # os.remove(model_saving_path_backup)

# After running the optimization, save and display the values of the
# learned parameters of the Gaussian, from which we sample the z's for the latent layer.
# These are the parameters define the Gaussians from which the latent variables' values are sampled
