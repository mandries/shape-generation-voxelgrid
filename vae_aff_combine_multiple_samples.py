# This Variational AutoEncoder
# combines latent descriptions of given objects into
# a single latent description of a new object

from __future__ import division, print_function, absolute_import
#~ from array import *
#~ from random import shuffle

import argparse # for parsing arguments
import sys # for counting the number of arguments

import numpy as np
import tensorflow as tf
# import time # for pausing the program
import time # to know how long it takes for the code to run
# Libraries for working with images
import os # to walk through directories, to rename files
import binvox_rw # to manipulate binvox files
import pcl_tools
import tools_dataset_processing
import tools_combination
import model_vae_3d

# The welcome message
print("Start: AutoEncoder for Combining latent vector representations of objects. Requires valid path for loading the VAE model (defined in code). Program is running")

# Introduce the argument parser (useful if you want to load pre-learned parameters, or start learning from zero)
parser = argparse.ArgumentParser()
# parser.add_argument("-r", "--restore_params",
#                 help="Restore the parameters in the graph from the given input file.",
#                 action="store_true")
args = parser.parse_args()

# if (args.restore_params):
#     print ("Restore params enabled")
# else:
#     print ("Restore params disabled: learning from zero")

saver = 0


# sys.exit(0)

################################################################################
################################## Constants ###################################
################################################################################

##### Constants #####
dataset_container = []  # The array containing the (3d) image data
dataset_image_count = 0 # The counter used for counting the number of images in the dataset

# Parameters
learning_rate = 0.001
total_epochs = 10000
batch_size = 6

#inv_frequency_of_hallucinations_generation = 100 # check reconstruction quality every N batches (from noise)
#total_hallucinations_generated_per_check = 20
inv_frequency_of_reconstruction_evaluations = 2 # check reconstruction quality every N batches (from real images)
inv_frequency_of_parameters_saving = 1 # save after every epoch

### Dataset data ###
# Description of the input
voxelized_input_width = 64
voxelized_input_height = 64
voxelized_input_depth = 64
nn_img_input_channels = 1 # 4 # (3 color + 1 depth)

input_file_format_suffix = '.binvox' # point cloud (is not voxelized representation)

# Model saving
# model_saving_path = '/home/mihai/Documents/VAE_code/learned_model_vae_3d/model_feature_learning_3D'
# model_saving_path_backup = model_saving_path + '_auto_backup.ckpt'
# model_saving_path = model_saving_path + '.ckpt'

# Path from where the most recent model is loaded
models_path = '/media/Data/mihai/models/' + 'models_successful_run_2018.03.01-05/'
model_loading_path = models_path + 'model__t_1520250223.7__epoch_121__loss_28140.39802886573' + '/model_feature_learning_3D.ckpt'

combinations_path = '/media/Data/mihai/combinations/'

################################################################################
################################# Functions ####################################
################################################################################
# This function is OK for 3d
def save_reconstructed_samples(reconstructed_samples, original_samples, directory):

    # Generate a folder to store the images
    # directory = "reconstructions_" + str(time.time()) + "iter_{0}".format(iteration_number)
    # if not os.path.exists(directory):
        # os.makedirs(directory)

    print('Saving originals and reconstructions... ')
    for sample_index in range(0, reconstructed_samples.shape[0]):
        # 1. Write original model to file

        # Write as point cloud
        # filename = os.path.join(   directory,
        #                            "sample_{0}_orig.pcd".format(sample_index)
        #                            )
        # pointcloud = pcl_tools.voxelized_to_pointcloud(original_samples[sample_index])
        # pcl_tools.write_pointcloud_to_file(pointcloud, filename)

        # Write as voxelgrid (binvox format)
        data = original_samples[sample_index] #voxelgrid
        dims = (64,64,64) # DEFAULT voxelgrid size here (not true size, which may vary)
        translate = (0,0,0)
        scale = 1
        axis_order = 'xyz'
        voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
        # Specify the file path
        filename = os.path.join(   directory,
                                   "sample_{0}_orig.binvox".format(sample_index)
                                   )
        filewriter = open(filename, 'w')
        # Write the binvox model to file (so that it can be seen through viewvow)
        binvox_rw.write(voxel_model, filewriter)


        # 2. Write reconstructed model to file
        # Threshold at 0.5 (to have binary states for voxels)
        voxelgrid = reconstructed_samples[sample_index] > 0.5
        # Write as voxelgrid (binvox format)
        data = voxelgrid
        dims = (64,64,64) # DEFAULT voxelgrid size here (not true size, which may vary)
        translate = (0,0,0)
        scale = 1
        axis_order = 'xyz'
        voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
        # Specify the file path
        filename = os.path.join(   directory,
                                   "sample_{0}_recon.binvox".format(sample_index)
                                   )
        filewriter = open(filename, 'w')
        # Write the binvox model to file (so that it can be seen through viewvow)
        binvox_rw.write(voxel_model, filewriter)
    print('done')
    return
################################################################################
############################# Building the graph ###############################
################################################################################


def build_graph():
    # Building the encoder
    input_placeholder = tf.placeholder(
                        tf.float32,
                        shape=[
                                None, # the size of the batch
                                voxelized_input_width,
                                voxelized_input_height,
                                voxelized_input_depth
                              ]
                              # nn_img_input_channels
                        )

    # input_placeholder_2 = tf.placeholder(
    #                     tf.float32,
    #                     shape=[
    #                             None, # the size of the batch
    #                             voxelized_input_width,
    #                             voxelized_input_height,
    #                             voxelized_input_depth
    #                           ]
    #                           # nn_img_input_channels
    #                     )

    # Building the decoder
    # target_placeholder = input_placeholder_1 # the thing we are trying to reconstruct

    # Capture the output of the encoder
    with tf.variable_scope("model"):
        object_z_means, object_z_log_vars = model_vae_3d.encoder_net(input_placeholder)
    # with tf.variable_scope("model", reuse=True):
    #     z_mean_2, z_log_var_2 = model_vae_3d.encoder_net(input_placeholder_2)

    # TODO check with Atabak if this part is correct
    with tf.variable_scope("model"):
        combined_mean, combined_log_var = \
            tools_combination.combine_multiple_object_descriptions_through_averaging_tf(
                object_z_means,
                object_z_log_vars,
                batch_size
            )
        #tools_combination.combine_multiple_object_descriptions_through_gaussian_intersection_tf(

        # print("shape of z_mean (decoder output): {}".format(z_mean.shape))
        # print("shape of z_log_var (decoder output): {}".format(z_log_var.shape))
        # Sampler: Normal (gaussian) random distribution
        eps = tf.random_normal(
                    # or z_log_var_2 (we only care about the shape, which is the same)
                    tf.shape(object_z_means), #combined_mean), #object_z_log_vars[0]),
                    dtype=tf.float32,
                    mean=0.,
                    stddev=1.0,
                    name='epsilon')
        print("shape of eps (encoder output): {}".format(eps.shape))
        #z_combined = combined_mean + tf.exp(combined_log_var / 2) * eps
        z_combined = tf.add(combined_mean, tf.exp(combined_log_var / 2) * eps)
        print("shape of z (decoder input): {}".format(z_combined.shape))

        # Generate reconstructions for all the samples in the batch
        # z_1 = z_mean_1 + tf.exp(z_log_var_1 / 2) * eps
        # z_2 = z_mean_2 + tf.exp(z_log_var_2 / 2) * eps
        z_reconstructions = tf.add(object_z_means, tf.exp(object_z_log_vars/ 2) * eps)

        # The combined object shape:
        #with tf.variable_scope("model", reuse=True):
        output_voxelized_volume_combined = model_vae_3d.decoder_net(z_combined)

    # combined_samples_tensor = tf.nn.sigmoid(output_voxelized_volume)

    # Generate reconstructions for all the samples in the batch
    with tf.variable_scope("model", reuse=True):
        output_voxelized_volume_reconstructions = model_vae_3d.decoder_net(z_reconstructions)

    # Initialize the variables (i.e. assign their default value)
    # init = tf.global_variables_initializer()
    # The model (graph) saver (needs to be instanciated after the graph is built)
    # if ((args.save_params) or (args.restore_params)):
    saver = tf.train.Saver()

    return  \
        input_placeholder, \
        output_voxelized_volume_reconstructions, \
        output_voxelized_volume_combined, \
        saver
        # output_voxelized_reconstructions, \

        # input_placeholder_2, \
        # output_voxelized_volume_recon1, \
        # output_voxelized_volume_recon2, \
        # output_voxelized_volume_combined, \
        # saver

################################################################################
############################## Running the graph ###############################
################################################################################

# dataset_train_paths = (
#     '/media/Data/datasets/ModelNet10_binvox/bathtub/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/bed/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/chair/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/dresser/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/desk/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/monitor/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/night_stand/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/sofa/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/table/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/toilet/train/')
#
# dataset_test_paths = (
#     '/media/Data/datasets/ModelNet10_binvox/bathtub/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/bed/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/chair/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/dresser/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/desk/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/monitor/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/night_stand/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/sofa/test/',\
#     '/media/Data/datasets/ModelNet10_binvox/table/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/toilet/test/')

#dataset_paths = ['/media/Data/datasets/combination_datasets_test/bathtubs/']
#dataset_paths = ['/media/Data/datasets/combination_datasets_test/chairs/']
#dataset_paths = ['/media/Data/datasets/combination_datasets_test/monitors/']
#dataset_paths = ['/media/Data/datasets/combination_datasets_test/tables/']
#dataset_paths = ['/media/Data/datasets/combination_datasets_test/dressers/']
#dataset_paths = ['/media/Data/datasets/combination_datasets_test/sofas/']
dataset_paths = ['/media/Data/datasets/combination_datasets_test/combine_chairs_tables/']

print("Processing the dataset files...")
# Go through the dataset
for index in range(len(dataset_paths)):
    print("Processing the dataset: " + dataset_paths[index])
    # get_dataset_files_from_path(
    tools_dataset_processing.load_dataset_binvox_64(
        dataset_paths[index], # vislab_dataset_path,
    	dataset_container,
        input_file_format_suffix)

# Do this before you iterate (because we need to feed in an array to tensorflow)
dataset_container = np.asarray(dataset_container)
# Checks
print("Samples in dataset (de facto): {0}".format(len(dataset_container)))

# Declare the iterator
iterator = tools_dataset_processing.data_iterator(dataset_container, batch_size)

#~ # Start a new TF session
sess = tf.Session()

# if __name__ = main():
# input_placeholder_1, \
# input_placeholder_2, \
# output_voxelized_volume_recon1, \
# output_voxelized_volume_recon2, \

input_placeholder, \
output_voxelized_volume_reconstructions, \
output_voxelized_volume_combined, \
saver = build_graph()

# Run the initializer
print("Restoring model from file... ")
saver.restore(sess, model_loading_path)
print("done.")

batch_x_1 = iterator.next()
# batch_x_2 = iterator.next()
print("Extracted batch 1")
feed_dict = {   input_placeholder: batch_x_1}
print("Prepared the feed_dict")


# voxelized outputs
input_samples, \
output_voxelized_reconstructions, \
output_reconstruction_combined_category = sess.run(
        [
            input_placeholder,
            output_voxelized_volume_reconstructions,
            output_voxelized_volume_combined,
        ],
        feed_dict = feed_dict
        )
                    # output_voxelized_reconstructions,
print("Generated the combination of {0} samples.".format(batch_size))

# print("Shapes of i1, i2, o1, o2, comb\n")
# (1x 64x64x64)
# print(input_sample_1.shape)
# print(input_sample_2.shape)
# print(output_recon1_sample_1.shape)
# print(output_recon1_sample_2.shape)
# print(output_recon_combined.shape)

# Save the models for checks
directory = combinations_path + "/category_combination_" + str(time.time()) # + "iter_{0}".format(iteration_number)
if not os.path.exists(directory):
    os.makedirs(directory)
filename = "category_combination"

 # Save the reconstructions
save_reconstructed_samples(output_voxelized_reconstructions, input_samples, directory)
# Save the combination
tools_combination.save_voxelgrid(output_reconstruction_combined_category[0], directory, filename)
print("Saved the combination files.")
