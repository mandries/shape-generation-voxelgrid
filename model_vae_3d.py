import numpy as np
import tensorflow as tf
import math as math

# Network Parameters
latent_dim = 2**11 #4096 # 2^12
output_dim = (2**6)**3
print("output_dim = {0} = 2^{1}".format(output_dim, math.log(output_dim, 2)))
denivelation = math.log((output_dim / latent_dim), 2) # (number, base)
print("denivelation: 2^{0}".format(denivelation))
# encoding_steps = 4
# decoding_steps = 5
conv3d_3cube_kernel_dim_step = 2**3
# Check these formulas:
# encoding_steps_without_dim_decrease = encoding_steps - math.floor(denivelation / encoding_steps)
# decoding_steps_without_dim_increase = decoding_steps - math.floor(denivelation / decoding_steps)


# Create model
def encoder_net(x): #, img_width, img_height, img_depth, img_channels):
    # Tensor input becomes 4-D: [Batch Size, Width, Height, Channel]
    # X = [Batch Size, Width, Height, Channel]
    #x = tf.reshape(x, shape=[-1, img_height, img_width, img_channels])

    print('\nEncoder input shape:')
    print(x.shape)

    x = tf.expand_dims(x, -1) # Now it becomes [Batch_size x (1)]
    print('\nEncoder input shape after dim expansion:')
    print(x.shape)

    # The input has not feature layers
    feature_layers = 1
    # Convolution Layer 1
    # input:    (64 x 64 x 64 x 1) = (2^6)^3 x 2^0 = 2^18
    # output:   (32 x 32 x 32 x 8) = (2^5)^3 x 2^3 = 2^18
    feature_layers *= conv3d_3cube_kernel_dim_step
    conv1 = tf.layers.conv3d(
                inputs=x,
                filters=feature_layers, # 8
                kernel_size=(3,3,3),
                strides=2,
                padding='same'
                )
    # Conv1 = [Batch Size, Height / 2, Width / 2, #Channels as defined by the weights of conv1]
    # Height and width are reduced by 2 because we use a stride=2

    # Apply non-linear function
    conv1 = tf.nn.relu(conv1)

    print('\nEncoder shape after conv1:')
    print(conv1.shape)

    # Convolution Layer 2
    # input:    (32 x 32 x 32 x 8) = (2^5)^3 x 2^3 = 2^18
    # output:   (16 x 16 x 16 x 32) = (2^4)^3 x 2^5 = 2^17
    feature_layers *= conv3d_3cube_kernel_dim_step # 8*8
    conv2 = tf.layers.conv3d(
                inputs=conv1,
                filters=feature_layers, # 2^3^2 = 2^6 = 32
                kernel_size=(3,3,3),
                strides=2,
                padding='same'
                )
    # Conv2 = [Batch Size, Height / 4, Width / 4, #Channels as defined by the weights of conv2]
    # Height and width are reduced by 2 because we use a stride=2

    # Apply non-linear function
    conv2 = tf.nn.relu(conv2)

    print('\nEncoder shape after conv2:')
    print(conv2.shape)


    # Convolution Layer 3
    # input:    (16 x 16 x 16 x 32) = 2^17
    # output:   (8 x 8 x 8 x 64) = (2^3)^3 x 2^6 = 2^15
    # Conv3 = [Batch Size, Height / 8, Width / 8, #Channels as defined by the weights of conv3]
    conv3 = tf.layers.conv3d(
                inputs=conv2,
                filters=(latent_dim * 2) / (2**4), #2^(11+1-3) = 2^8 = 256
                kernel_size=(3,3,3),
                strides=2,
                padding='same'
                )

    # Apply non-linear function (not here, because we are now predicting means and log-variances)
    conv3 = tf.nn.relu(conv3)

    print('\nEncoder shape after conv3:')
    print(conv3.shape)

    # Convolution Layer 4
    # input:   (8 x 8 x 8 x 64)
    # output:  (4 x 4 x 4 X LVx2) = 4^3 x 2^12
    # Conv3 = [Batch Size, Height / 8, Width / 8, #Channels as defined by the weights of conv3]
    conv4 = tf.layers.conv3d(
                inputs=conv3,
                filters=latent_dim * 2, #256, #512, # should be previous_filter_size * 2^3 = 4096
                kernel_size=(3,3,3),
                strides=2,
                padding='same'
                )

    print('\nEncoder shape after conv4:')
    print(conv4.shape)

    # TODO the kernel size does not cover the whole object here
    # For this reason,
    #   when randomly sampling from the obtained feature distribution functions,
    #   correlations may be lost between features that co-occur

    # What to do: use a feed-forward NN, to get biases and parameter weights here
    # Weights_for_log_var
    # Weights_for_means


    # slice, assuming there are (latent_dim x 2) features
    # that we can split into two slices with (latent_dim) features
    means_slice = tf.slice( input_ = conv4,
                            begin = [0,0,0,0,0],
                            size = [-1,4,4,4,latent_dim],
                            name = "means_slice")
    log_vars_slice = tf.slice( input_ = conv4,
                            begin = [0,0,0,0,latent_dim],
                            size = [-1,4,4,4,latent_dim],
                            name = "log_vars_slice")

    means_layer =  tf.reduce_max(
                    input_tensor=means_slice,
                    axis=[1,2,3],
                    keepdims=False #keep_dims=False
                    )
    log_vars_layer =  tf.reduce_max(
                    input_tensor=log_vars_slice,
                    axis=[1,2,3],
                    keepdims=False #keep_dims=False
                    )

    print('\nEncoder shape after splitting conv4 into means and log_vars:')
    print(means_layer.shape)
    print(log_vars_layer.shape)

    # return latent_mean, latent_log_var
    return means_layer, log_vars_layer


# Aymeric tutorial (http://localhost:8888/notebooks/dcgan.ipynb)
# z is a sample from the N(latent_mean, latent_log_var):
def decoder_net(z): # z is of size (Batch_Size x 512)

    print('\nDecoder layer shape at start: ')
    print(z.shape)

    layer = tf.expand_dims(z, 1) # Now it becomes [Batch_size x (1) x 512]
    layer = tf.expand_dims(layer, 1) # Now it becomes [Batch_size x (1) x 1 x 512]
    layer = tf.expand_dims(layer, 1) # Now it becomes [Batch_size x (1) x 1 x 1 x 512]
    # expand_dims always puts a 1 dimension in the "second" position
    # (1 position, if you count from 0)

    print('\nDecoder layer shape at start, after expansion: ')
    print(layer.shape)

    # Deconvolution,
    # image shape before:     [1 x 1 x 1 x latent_dim=512]
    # image shape afterwards: [4 x 4 x 4 x (2^8 = 256)]
    layer = tf.layers.conv3d_transpose(
                inputs=layer,
                # min: latent_dim_size / volume_size;
                # max: output_volume_size / latent_dim_size
                filters=latent_dim / (2**3), # 2^(11-3) = 2^8 = 256
                kernel_size=(4,4,4),
                strides=1,
                use_bias=False, # to avoid the conversion error, https://github.com/tensorflow/tensorflow/issues/10520
                padding='valid')  # valid = without padding
    print('Decoder layer shape after deconv 01: ')
    print(layer.shape)

    # Apply non-linear function
    layer = tf.nn.relu(layer)

    # Deconvolution,
    # image shape before:     [4 x 4 x 4 x 4096]
    # image shape afterwards: [8 x 8 x 8 x 64]
    layer = tf.layers.conv3d_transpose(
                inputs=layer,
                filters=64, # 2**6 #latent_dim / (2**6), #64, #128
                kernel_size=(5,5,5), # o = i + (k-1) # for no zero padding, unit strides
                # out_height = ceil(float(in_height - filter_height + 1) / float(strides[1]))
                # out_width  = ceil(float(in_width - filter_width + 1) / float(strides[2]))
                strides=1,
                use_bias=False,  # to avoid the conversion error, https://github.com/tensorflow/tensorflow/issues/10520
                padding='valid') # valid = without padding
    print('Decoder layer shape after deconv 02: ')
    print(layer.shape)

    # Apply non-linear function
    layer = tf.nn.relu(layer)


    # Deconvolution,
    # image shape before:     [ 8 x  8 x  8 x 512]
    # image shape afterwards: [16 x 16 x 16 x 64]
    layer = tf.layers.conv3d_transpose(
                inputs=layer,
                filters=32, # #64,
                kernel_size=(9,9,9), # o = s*(i-1) + k # for no zero padding, non-unit strides
                strides=1,
                use_bias=False, # to avoid the conversion error, https://github.com/tensorflow/tensorflow/issues/10520
                padding='valid') # valid = without padding
    print('Decoder layer shape after deconv 03: ')
    print(layer.shape)

    # Apply non-linear function
    layer = tf.nn.relu(layer)


    # Deconvolution,
    # image shape before:     [16 x 16 x 16 x 64]
    # image shape afterwards: [32 x 32 x 32 x 8]
    layer = tf.layers.conv3d_transpose(
                inputs=layer,
                filters=8, #conv3d_3cube_kernel_dim_step,
                kernel_size=(17,17,17), # o = s*(i-1) + k # for no zero padding, non-unit strides
                strides=1,
                use_bias=False,  # to avoid the conversion error, https://github.com/tensorflow/tensorflow/issues/10520
                padding='valid') # valid = without padding
    print('Decoder layer shape after deconv 04: ')
    print(layer.shape)

    # Apply non-linear function
    layer = tf.nn.relu(layer)

    # Deconvolution,
    # image shape before:     [32 x 32 x 32 x 8]
    # image shape afterwards: [64 x 64 x 64 x 1]
    layer = tf.layers.conv3d_transpose(
                inputs=layer,
                filters=1,
                kernel_size=(33,33,33), # o = s*(i-1) + k # for no zero padding, non-unit strides
                strides=1,
                use_bias=False, # to avoid the error
                padding='valid') # valid = without padding
    print('Decoder layer shape after deconv 05: ')
    print(layer.shape)

    # Apply non-linear function
    # layer = tf.nn.relu(layer)

    layer = tf.reshape(layer, [-1, 64, 64, 64])
    print('Decoder layer shape after deconv 05: ')
    print(layer.shape)

    return  layer

# Define VAE Loss
def reconstruction_loss(x_reconstructed, x_true):
    # Reconstruction loss
    reconstruction_loss = tf.reduce_sum(
                            tf.nn.sigmoid_cross_entropy_with_logits(
                                #_sentinel=None,
                                labels=x_true,
                                logits=x_reconstructed,
                                name="Reconstruction_loss"
                            ),
                            axis=[1,2,3])
    # reconstruction_loss = tf.reduce_sum(reconstruction_loss)
    reconstruction_loss = tf.reduce_mean(reconstruction_loss)
    return reconstruction_loss

def reconstruction_loss_false_pos(x_reconstructed, x_true):
    # Compute the !(x_true)
    # meaning that x_true + !x_true = ones
    orig_neg = tf.subtract(
                    tf.ones(x_true.shape, tf.float32),
                    x_true,
                    name="orig_neg"
                    )
    #
    # Compute the threshold (a matrix filled with 0.5)
    threshold = tf.scalar_mul(
                    0.5,
                    tf.ones(x_true.shape, tf.float32)
                )
    #
    # Generate the reconstruction's positive mask (select everything larger_or_equal than 0.5)
    # as a {0,1} matrix
    recon_pos_mask = tf.greater_equal(
                        x_reconstructed,
                        threshold
                        )

    # Select the reconstruction values that predict 1s (which are >= 0.5)
    recon_pos = tf.multiply(
                    recon_pos_mask,
                    x_reconstructed
                    )

    # Leave only the cells that are false positives
    false_pos = tf.multiply(
                    orig_neg,
                    recon_pos # distance from 0
                )
    # return false_pos, false_neg

    reconstruction_loss_false_pos = tf.reduce_sum(
                            tf.nn.sigmoid_cross_entropy_with_logits(
                                #_sentinel=None,
                                labels=x_true,
                                logits=tf.multiply(orig_neg, recon_pos),
                                name="Reconstruction_loss_false_positives"
                            ),
                            axis=[1,2,3])

    # reconstruction_loss = tf.reduce_sum(reconstruction_loss)
    # reconstruction_loss = tf.reduce_mean(reconstruction_loss)
    #return reconstruction_loss_false_pos, reconstruction_loss_false_neg
    return reconstruction_loss_false_pos


def reconstruction_loss_false_neg(x_reconstructed, x_true):
    # Compute the threshold (a matrix filled with 0.5)
    threshold = tf.scalar_mul(
                    0.5,
                    tf.ones(x_true.shape, tf.float32)
                )

    # Generate the reconstruction's negative mask (select everything less than 0.5)
    # as a {0,1} matrix
    recon_neg_mask = tf.less(
                        x_reconstructed,
                        threshold
                        )

    # Select the reconstruction values that predict 0s (which are < 0.5)
    recon_neg = tf.multiply(
                    recon_neg_mask,
                    x_reconstructed
                )

    # Leave only the cells that are false negatives
    false_neg = tf.multiply(
                    x_true,
                    tf.subtract(
                        recon_neg_mask, # distance from 1
                        recon_neg
                    )
                )

    reconstruction_loss_false_neg = tf.reduce_sum(
                            tf.nn.sigmoid_cross_entropy_with_logits(
                                #_sentinel=None,
                                labels=tf.multiply(x_true, recog_neg_mask),
                                logits=tf.multiply(x_true, recon_neg),
                                name="Reconstruction_loss_false_positives"
                            ),
                            axis=[1,2,3])

    return reconstruction_loss_false_neg



# This loss is to be used
# to encourage the network to generate at least some filled voxels,
# instead of preferring to generate empty volumes.
def reconstruction_loss_weighted(x_reconstructed, x_true, pos_weight_value):
    # Reconstruction loss
    reconstruction_loss = tf.reduce_sum(
                            tf.nn.weighted_cross_entropy_with_logits(
                                #_sentinel=None,
                                targets=x_true, # labels
                                logits=x_reconstructed,
                                pos_weight=pos_weight_value,
                                name="Reconstruction_loss_weighted"
                            ),
                            axis=[1,2,3])
    # reconstruction_loss = tf.reduce_sum(reconstruction_loss)
    reconstruction_loss = tf.reduce_mean(reconstruction_loss)
    return reconstruction_loss


def reconstruction_loss_uniformly_sampled(x_reconstructed, x_true):
    space_shape = tf.shape(x_true)[1:] # 64x64x64 (the cube)
    space_size = tf.reduce_prod(space_shape) # 64*64*64 = 2^18
    x_true_flatten = tf.reshape(x_true, [-1, space_size]) # the flattened cube
    #mean_probabilities = tf.reduce_sum(x_true_flatten, axis=1)/tf.cast(space_size, dtype=tf.float32)
    total_filled_voxels = tf.reduce_sum(x_true_flatten, axis=1)
    mean_probabilities = total_filled_voxels/(tf.cast(space_size, dtype=tf.float32) - total_filled_voxels)
    mean_probabilities = tf.clip_by_value(
                            t=mean_probabilities,
                            clip_value_min=1e-6, #0.0,
                            clip_value_max=(1.0 - 1e-6),
                            name="Clipping_to_get_only_valid_values")
    # tf.cast(space_size, dtype=tf.float32)
    coin_tosses = tf.distributions.Bernoulli(probs=mean_probabilities,
                                             dtype=tf.float32)
    tossing_outcome = tf.transpose(coin_tosses.sample(sample_shape=space_size))
    samples = tf.reshape(tossing_outcome, tf.shape(x_true))
    #mask = tf.zeros([64,64,64])
    # tf.where(tf.cast(x_true, tf.bool), tf.ones_like(x_true), samples)
    mask = tf.where(tf.greater(x_true, 0.5), tf.ones_like(x_true), samples)
    reconstruction_loss_raw = tf.nn.sigmoid_cross_entropy_with_logits(labels=x_true,
                                                                      logits=x_reconstructed,
                                                                      name="Reconstruction_loss_raw"
                                                                      )
    reconstruction_loss = tf.reduce_sum(tf.multiply(reconstruction_loss_raw, mask),
                                        axis=[1, 2, 3])
    reconstruction_loss = tf.reduce_mean(reconstruction_loss)
    return reconstruction_loss #, mask


# The KL divergence loss to be used when not using softbits
def kl_loss(z_mean, z_log_var):
    kl_div_loss = 0.5 * tf.reduce_sum(tf.square(z_mean) + tf.exp(z_log_var) - z_log_var -1)
    return kl_div_loss

# Computes the KL loss per dimension
def kl_loss_per_dim(z_mean, z_log_var):
    # KL Divergence loss
    kl_div_loss_per_dim = 0.5 * tf.reduce_mean(tf.square(z_mean) + tf.exp(z_log_var) - z_log_var -1, axis=0)
    # Expectation [ log P(x) - log Q(x) ] =
    # Expectation [ log(z_mean) - log(0) ] =
    # Expectation [ log(z_mean) - log(0) ]
    #
    #
    # kl_div_loss = 0.5 * tf.reduce_sum(tf.square(z_mean) + tf.exp(z_log_var) - z_log_var -1)
    #
    #kl_div_loss = 1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var)
    #kl_div_loss = -0.5 * tf.reduce_sum(kl_div_loss, 1)
    #return tf.reduce_mean(encode_decode_loss + kl_div_loss)
    return kl_div_loss_per_dim

# Computes the KL loss per dimension, per sample
def kl_loss_per_dim_per_sample(z_mean, z_log_var):
    kl_loss_per_dim_per_sample = 0.5 * (tf.square(z_mean) + tf.exp(z_log_var) - z_log_var -1)
    return kl_loss_per_dim_per_sample

# Increases the epoch counter given as input
def increase_epoch_counter(epoch_counter):
    return tf.add(epoch_counter, 1)
