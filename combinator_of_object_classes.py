# This Variational AutoEncoder
# combines latent descriptions of given objects into
# a single latent description of a new object

from __future__ import division, print_function, absolute_import
#~ from array import *
#~ from random import shuffle

import argparse # for parsing arguments
import sys # for counting the number of arguments

import numpy as np
import tensorflow as tf
# import time # for pausing the program
import time # to know how long it takes for the code to run
# Libraries for working with images
import os # to walk through directories, to rename files
from matplotlib import pyplot as plt

import binvox_rw # to manipulate binvox files
import pcl_tools
import tools_dataset_processing
import tools_combination
import model_vae_3d

# import vae_aff_combine_multiple_samples_and_inspect as basics
import global_values
import csv # to read the file containing the (functionality -> object class) mapping
import tools_sdf_generator

import trimesh # for converting voxel grids to meshes (to import objects into simulators)

# The welcome message
# print("Start: AutoEncoder for Combining latent vector representations of objects. Requires valid path for loading the VAE model (defined in code). Program is running")

saver = 0

os.environ["CUDA_VISIBLE_DEVICES"]="1"
# sys.exit(0)

global_values.dataset_root_location = '/media/Data/datasets/'
global_values.dataset_name = 'ModelNet40_binvox_exact_center_4views/'

model_path = '/media/Data/mihai/models/' + 'model_ModelNet40__t_1530677023__epoch_66__train_52701__test_83066__void_755' + '/model_feature_learning_3D.ckpt'

object_to_functionalities_mapping = {}
functionality_to_objects_mapping = {}
combination_percentages = [0.5, 0.6, 0.7, 0.8, 0.95]

################################################################################
####################### # Helper functions (Atabak code)########################
################################################################################
def load_functionality_to_object_mapping(mapping_filename):

    print("Loading the mapping of objects to functionalities and viceversa (from input CSV: {0})".format(mapping_filename))

    with open(mapping_filename, 'rb') as csvfile:
        filereader = csv.reader(csvfile, delimiter='\n', quotechar='"')
        for row in filereader:
            # row is a list, containing a single element: the row string content
            # print(row[0])
            wordreader = row[0].split(",")
            wordId = 0
            objectName=""
            for word in wordreader:
                if (wordId == 0):
                    objectName = word
                    print("object name: {0}".format(objectName))
                elif (word != ""):
                    functionalityName = word
                    # Map functionality to object
                    add_functionality_to_object(
                        object_to_functionalities_mapping,
                        objectName,
                        functionalityName)
                    # Map object to functionality
                    add_object_to_functionality(
                        functionality_to_objects_mapping,
                        functionalityName,
                        objectName
                        )
                wordId = wordId + 1
            print("\n")
        # Print the two created lists
        print("\nObjects to functionalities mapping:")
        # object_to_functionalities_mapping = collections.OrderedDict(sorted(object_to_functionalities_mapping.items()))
        for index in object_to_functionalities_mapping:
            print("{0}: {1}".format(index, object_to_functionalities_mapping[index]))

        # functionality_to_objects_mapping = collections.OrderedDict(sorted(functionality_to_objects_mapping.items()))
        print("\nFunctionality to objects mapping:")
        for index in functionality_to_objects_mapping:
            print("{0}: {1}".format(index, functionality_to_objects_mapping[index]))

# Adds a functionality to an object
def add_functionality_to_object(
    object_to_functionalities_mapping,
    object_in,
    functionality_in):
    # print("Adding functionality [{1}] to object [{0}]".format(object_in, functionality_in))
    if (object_in in object_to_functionalities_mapping):
        # Retrieve the list of functionalities assigned to this object
        functionalities = object_to_functionalities_mapping.get(object_in)
        # Add the new functionality to the list of functionalities assigned to this object
        functionalities.append(functionality_in)
    else:
        # Add object-functionality pair
        object_to_functionalities_mapping[object_in] = [functionality_in]
    return

# Adds an object to a functionality
def add_object_to_functionality(
    functionality_to_objects_mapping,
    functionality_in,
    object_in):
    # print("Adding object [{0}] to functionality [{1}]".format(object_in, functionality_in))
    if functionality_in in functionality_to_objects_mapping:
        # Retrieve the list of objects with this functionality
        objects_with_this_functionality = functionality_to_objects_mapping.get(functionality_in)
        # Add the object to the list of objects possessing this functionality
        objects_with_this_functionality.append(object_in)
    else:
        functionality_to_objects_mapping[functionality_in] = [object_in]
    return

def epsilon_like(tensor, name='epsilon'):
    return tf.random_normal(tf.shape(tensor), dtype=tf.float32, name=name)

# Function for loading two object classes
def load_two_object_classes(
                                object_class_1_name,
                                object_class_2_name
                            ):

    # From vae_code_3d.py
    # dataset_root_location = '/media/Data/datasets/'
    # dataset_name = 'ModelNet40_binvox_exact_center_4views/'

    # Object class 1
    # Use the test dataset as it has fewer examples (otherwise we get "out of  memory" error)
    object_class_1_path = [global_values.dataset_root_location +  global_values.dataset_name + object_class_1_name + '/test/']

    # Object class 2
    object_class_2_path = [global_values.dataset_root_location +  global_values.dataset_name + object_class_2_name + '/test/']

    # Void volume
    void_path = [global_values.dataset_root_location +  global_values.dataset_name + 'void']

    # Containers for object classes
    object_class_1_container = list()
    object_class_2_container = list()
    void_container = list()

    # input_file_format_suffix = '.binvox'
    input_file_format_suffix = '_1.binvox' # Use samples without their rotations

    # Load the object samples into the corresponding containers
    tools_dataset_processing.load_dataset_binvox_64(
        object_class_1_path[0],
        object_class_1_container,
        input_file_format_suffix)
    tools_dataset_processing.load_dataset_binvox_64(
        object_class_2_path[0],
        object_class_2_container,
        input_file_format_suffix)
    tools_dataset_processing.load_dataset_binvox_64(
        void_path[0],
        void_container,
        input_file_format_suffix);
    # Convert the object containers into numpy arrays
    object_class_1_container = np.asarray(object_class_1_container)
    object_class_2_container = np.asarray(object_class_2_container)
    void_container = np.asarray(void_container)

    return object_class_1_container, object_class_2_container, void_container



################################################################################
############################# Building the graph ###############################
################################################################################

# make_decoder = tf.make_template('decoder', model_vae_3d.decoder_net)
def epsilon_like(tensor, name='epsilon'):
    return tf.random_normal(tf.shape(tensor), dtype=tf.float32, name=name)

def load_model():
    # Prepare a list of results to return
    results = dict()
    make_encoder = tf.make_template('', model_vae_3d.encoder_net)


    _, _, void_container = load_two_object_classes('', '') # you should

    with tf.variable_scope("model", reuse=None):
        results["input_placeholder"] = tf.placeholder(tf.float32,
                                        shape=[None]+list(void_container.shape[1:]),
                                        name="input_placeholder")
        objects_z_means_and_log_vars = make_encoder(results["input_placeholder"])
        # Add the first result
        results["objects_z_means_and_log_vars"] = objects_z_means_and_log_vars

        avg_z_means_and_log_vars = [tf.reduce_mean(tt, axis=0, keepdims=True) for tt in objects_z_means_and_log_vars]
        # Add the second result
        results["avg_z_means_and_log_vars"] = avg_z_means_and_log_vars
        void = tf.constant(void_container, dtype=tf.float32, name='void')
        void_z_means_and_log_vars = make_encoder(void);
        # Add the third result
        results["void_z_means_and_log_vars"] = void_z_means_and_log_vars

        avg_dist = tf.distributions.Normal(loc=avg_z_means_and_log_vars[0],
                                           scale=tf.exp(avg_z_means_and_log_vars[1]/2),
                                           validate_args=True,
                                           allow_nan_stats=False,
                                           name='avg_dist')
        # Append to results
        results["avg_dist"] = avg_dist

        void_dist = tf.distributions.Normal(loc=void_z_means_and_log_vars[0],
                                           scale=tf.exp(void_z_means_and_log_vars[1]/2),
                                           validate_args=True,
                                           allow_nan_stats=False,
                                           name='void_dist')
        # Append to results
        results["void_dist"] = void_dist


        prior_dist = tf.distributions.Normal(loc=tf.zeros_like(void_z_means_and_log_vars[0]),
                                           scale=tf.ones_like(void_z_means_and_log_vars[1]),
                                           validate_args=True,
                                           allow_nan_stats=False,
                                           name='void_dist')
        # Append to results
        results["prior_dist"] = prior_dist

        kl_with_void = tf.distributions.kl_divergence(avg_dist,
                                                      void_dist,
                                                      allow_nan_stats=False,
                                                      name='kl_with_void')
        # Append to results
        results["kl_with_void"] = kl_with_void

        kl_with_prior = tf.distributions.kl_divergence(avg_dist,
                                                      prior_dist,
                                                      allow_nan_stats=False,
                                                      name='kl_with_void')
        # Append to results
        results["kl_with_prior"] = kl_with_prior

        z_placeholder = tf.placeholder(tf.float32,
                                    shape=[None]+void_z_means_and_log_vars[0].get_shape().as_list()[1:],
                                    name="latent_placeholder")
        # Append to results
        results["z_placeholder"] = z_placeholder

        target_reconstruction = tf.nn.sigmoid(model_vae_3d.decoder_net(z_placeholder))
        # Append to results
        results["target_reconstruction"] = target_reconstruction

    var_dict = {v.name.replace('//','/').split(':')[0]:v
                for v in tf.get_default_graph().get_collection(tf.GraphKeys.GLOBAL_VARIABLES)}
    saver = tf.train.Saver(var_dict)
    # Append to results
    results["saver"] = saver
    #
    return results

# Saves the reconstructions for the combination of functional essences of
# input "object_class_1_name" and "object_class_2_name"
def generate_and_save_reconstructions(
        object_class_1_name,
        object_class_2_name,
        functionality_names,
        top_percentages):
    model_tensor_dict = load_model()

    sess = tf.InteractiveSession()
    model_tensor_dict["saver"].restore(sess=sess, save_path=model_path)

    object_class_1_container, object_class_2_container, void_container = load_two_object_classes(object_class_1_name, object_class_2_name)

    # KL with void
    kl_object_class_1_vs_void = model_tensor_dict["kl_with_void"].eval(session=sess, feed_dict={model_tensor_dict["input_placeholder"]: object_class_1_container})

    kl_object_class_2_vs_void = model_tensor_dict["kl_with_void"].eval(session=sess, feed_dict={model_tensor_dict["input_placeholder"]: object_class_2_container})

    # KL with prior
    kl_object_class_1_vs_prior = model_tensor_dict["kl_with_prior"].eval(session=sess, feed_dict={model_tensor_dict["input_placeholder"]: object_class_1_container})

    kl_object_class_2_vs_prior = model_tensor_dict["kl_with_prior"].eval(session=sess, feed_dict={model_tensor_dict["input_placeholder"]: object_class_2_container})

    kl_object_class_1_vs_void /= np.linalg.norm(kl_object_class_1_vs_void)
    kl_object_class_2_vs_void /= np.linalg.norm(kl_object_class_2_vs_void)

    kl_object_class_1_vs_prior /= np.linalg.norm(kl_object_class_1_vs_prior)
    kl_object_class_2_vs_prior /= np.linalg.norm(kl_object_class_2_vs_prior)

    # Compute combined KL with void and with prior, for each object class
    # This allows to identify which variables are important, and which are not
    kl_total_object_class_1 = kl_object_class_1_vs_void + kl_object_class_1_vs_prior
    #
    kl_total_object_class_2 = kl_object_class_2_vs_void + kl_object_class_2_vs_prior

    sorted_arg_max_object_class_1 = np.argsort(kl_total_object_class_1,axis=1)
    sorted_arg_max_object_class_2 = np.argsort(kl_total_object_class_2,axis=1)

    # AA
    object_class_1_avg_means_and_logvars = sess.run(model_tensor_dict["avg_z_means_and_log_vars"], feed_dict={model_tensor_dict["input_placeholder"]: object_class_1_container})

    object_class_2_avg_means_and_logvars = sess.run(model_tensor_dict["avg_z_means_and_log_vars"], feed_dict={model_tensor_dict["input_placeholder"]: object_class_2_container})

    void_avg_means_and_logvars = sess.run(model_tensor_dict["avg_z_means_and_log_vars"], feed_dict={model_tensor_dict["input_placeholder"]: void_container})
    ## /AA

    # Generate a folder to store the images
    print("Generating a folder to save the object_class_essences and combinations")
    functionality_suffix = \
            "_func_" + functionality_names[0] + "_" + functionality_names[1]

    combinations_path = '/media/Data/mihai/combinations/'
    folder_name = "category_combination_" + str(time.time()) + functionality_suffix
    directory = combinations_path + folder_name
    if not os.path.exists(directory):
        os.makedirs(directory)
    save_class_essences = True

    # The script that will contain the commands for spawning the models in gazebo
    spawning_script_for_gazebo = '#!/bin/bash \n'

    # Separate here
    for index in range (0, len(top_percentages)):

        important_object_class_1_idxs = sorted_arg_max_object_class_1[0,:int(top_percentages[index]*sorted_arg_max_object_class_1.shape[1])]

        important_object_class_2_idxs = sorted_arg_max_object_class_2[0,:int(top_percentages[index]*sorted_arg_max_object_class_2.shape[1])]

        # Taken from here (AA)

        # base combination is NOT built on top of void, but on one of the objects
        combination_means_and_logvars = np.copy(object_class_1_avg_means_and_logvars)

        for component_idx, _ in enumerate(combination_means_and_logvars):

            combination_means_and_logvars[component_idx][0, important_object_class_1_idxs] = object_class_1_avg_means_and_logvars[component_idx][0, important_object_class_1_idxs]

            combination_means_and_logvars[component_idx][0, important_object_class_2_idxs] = object_class_2_avg_means_and_logvars[component_idx][0, important_object_class_2_idxs]

        # Computing the combined latent vector
        combined_latent = combination_means_and_logvars[0] + np.exp(combination_means_and_logvars[1]/2)*np.random.normal(size=combination_means_and_logvars[1].shape)

        object_class_1_latent = object_class_1_avg_means_and_logvars[0] + np.exp(object_class_1_avg_means_and_logvars[1]/2)*np.random.normal(size=object_class_1_avg_means_and_logvars[1].shape)

        object_class_2_latent = object_class_2_avg_means_and_logvars[0] + np.exp(object_class_2_avg_means_and_logvars[1]/2)*np.random.normal(size=object_class_2_avg_means_and_logvars[1].shape)

        decoded_combined = model_tensor_dict["target_reconstruction"].eval(session=sess, feed_dict={model_tensor_dict["z_placeholder"]: combined_latent})

        decoded_object_class_1 = model_tensor_dict["target_reconstruction"].eval(session=sess, feed_dict={model_tensor_dict["z_placeholder"]: object_class_1_latent})

        decoded_object_class_2 = model_tensor_dict["target_reconstruction"].eval(session=sess, feed_dict={model_tensor_dict["z_placeholder"]: object_class_2_latent})

        # Thresholding the generated voxel volume, to get 1/0 outputs
        decoded_combined_threshed = (decoded_combined>0.5).astype(np.int)
        # Computing how many filled voxels we have in the output (for the laugh)
        # decoded_combined_threshed.sum()

        # Threshold the decoded object class 1 essence
        decoded_object_class_1_threshed = (decoded_object_class_1>0.5).astype(np.int)
        # Threshold the decoded object class 2 essence
        decoded_object_class_2_threshed = (decoded_object_class_2>0.5).astype(np.int)

        decoded_object_class_threshed = np.stack([
            decoded_object_class_1_threshed,
            decoded_object_class_2_threshed],
            axis=0)

        # Provide additional data for intuitive naming of generated folders
        # functionality_names = []
        object_names = [object_class_1_name, object_class_2_name]
        # Saving the generated combinations
        tools_combination.save_object_class_combination_in_directory(
            decoded_object_class_threshed, # object class (1,2) essences
            decoded_combined_threshed,
            top_percentages[index],
            directory,
            save_class_essences,
            functionality_names,
            object_names
            )
        save_class_essences = False


        ### MESH GENERATION PHASE FOLLOWS ###

        # Generate an empty voxelgrid, and fill its voxels where necessary
        # (conversion from Integer to Boolean voxelgrid)
        voxelgrid = np.full((64,64,64), False, dtype=bool)
        # filled_voxels=0
        for x in range(0,len(decoded_combined_threshed[0])):
            for y in range(0,len(decoded_combined_threshed[0][0])):
                for z in range(0,len(decoded_combined_threshed[0][0][0])):
                    if (decoded_combined_threshed[0][x][y][z] == 1):
                        voxelgrid[x][y][z] = True
                        # filled_voxels+=1
        # print("{0} = {1}".format(np.sum(voxelgrid),filled_voxels))
        mass = np.sum(voxelgrid) * 0.008 # steel density = 8g / cm3
        scaling_factor = 0.1

        # Convert voxel grid to mesh
        obj_combined_mesh = trimesh.voxel.matrix_to_marching_cubes(
            matrix=voxelgrid, #decoded_combined_threshed[0],
            pitch=1.0,
            # origin=(32,32,0)) # TODO replace hard-coded values with variables
            origin=np.zeros(3))

        print("Merging vertices closer than a pre-set constant...")
        obj_combined_mesh.merge_vertices()
        print("Removing duplicate faces...")
        obj_combined_mesh.remove_duplicate_faces()
        #
        # # Rescale the object, so that it occupies a correct amount of space.
        # print("Rescaling the mesh...")
        obj_combined_mesh.apply_scale(scaling=scaling_factor)
        # Warning: this does not rescale the origin
        #
        print("Extracting the first connected component (may not be the largest!)")
        # print("Connected components:")
        # print(connected_components)
        # for i in range (len(connected_components)):
        #     print("{0} component size: {1}".format(i, len(connected_components[0])))
        # # print(len(connected_components))
        connected_components = obj_combined_mesh.split()
        obj_combined_mesh = connected_components[0]
        #
        print("Making the mesh watertight...")
        trimesh.repair.fill_holes(obj_combined_mesh)
        # is_watertight = False
        # while (is_watertight == False):
        #     print("repairing mesh...")
        #     is_watertight = trimesh.repair.fill_holes(obj_combined_mesh)

        # # Some more fixes
        print("Fixing inversion and winding...")
        trimesh.repair.fix_inversion(obj_combined_mesh)
        trimesh.repair.fix_winding(obj_combined_mesh)

        print("Computing the center of mass: ")
        center_of_mass = obj_combined_mesh.center_mass
        print(center_of_mass)

        print("Computing moments of inertia: ")
        moments_of_inertia = obj_combined_mesh.moment_inertia
        print(moments_of_inertia) # inertia tensor in meshlab

        # # Export the mesh
        # Prepare the combination_percentage string for the file name
        percentage = str(top_percentages[index]).replace(".","")
        # obj_combined_mesh.export(directory + "/combined_mesh_" + percentage + ".ply")
        trimesh.io.export.export_mesh(
            mesh=obj_combined_mesh,
            file_obj=directory + "/combined_mesh_" + percentage + ".stl",
            file_type="stl"
        )
        # Display the mesh
        # obj_combined_mesh.show()

        print("Generating the SDF file...")
        object_model_name = "comb_" + functionality_suffix + "_" + percentage
        tools_sdf_generator.generate_model_sdf(
            directory=directory,
            object_name= object_model_name,
            center_of_mass=center_of_mass,
            inertia_tensor=moments_of_inertia,
            mass=mass,
            model_stl_path=directory + "/combined_mesh_" + percentage + ".stl")

        spawning_script_for_gazebo += 'rosrun gazebo_ros spawn_model -sdf -file ' + directory + '/' + object_model_name + '.sdf -model ' + object_model_name + ' \n'

    # Create the spawning script for gazebo
    f = open(directory + "/spawning.sh", "w")
    # Write the content to file
    f.write(spawning_script_for_gazebo)
    # Close the file
    f.close()

    return

# Execute this code if you explicitly run this function (and not if you load it)
if __name__ == "__main__":

    print("Running this code:")

    # Loading the mapping (functionality -> object)
    load_functionality_to_object_mapping('/home/mihai/Documents/VAE_code/ModelNet40_mapping_affordances.csv')

    print("\n")
    # Select functionality 1
    input_is_invalid = True
    functionality_1 = ""
    while input_is_invalid:
        functionality_1 = raw_input("Please select desired Functionality 1: ")
        if (functionality_1 in functionality_to_objects_mapping):
            input_is_invalid = False
        else:
            print("Invalid functionality")
    print("Functionality 1: {0}\n".format(functionality_1))

    # Select functionality 2
    input_is_invalid = True
    functionality_2 = ""
    while input_is_invalid:
        functionality_2 = raw_input("Please select desired Functionality 2: ")
        if (functionality_2 in functionality_to_objects_mapping):
            input_is_invalid = False
        else:
            print("Invalid functionality")
    print("Functionality 2: {0}\n".format(functionality_2))
    # 0 to stop

    print("Functionality {0} is present in objects: {1}\n".format(
        functionality_1,
        functionality_to_objects_mapping.get(functionality_1)))
    print("Functionality {0} is present in objects: {1}\n".format(
        functionality_2,
        functionality_to_objects_mapping.get(functionality_2)))

    ### Choosing objects providing desired functionalities
    # You can probably generate multiple solutions, by choosing each category in part and making something out of it.
    print("Selection of object categories providing desired functionalities")
    object_1_index = 0
    object_2_index = 0

    input_is_invalid = True
    while input_is_invalid:
        object_1_index = int(
                raw_input("Please choose object index for functionality 1 ("
                          + functionality_1
                          + ") [0-"
                          + str(len(functionality_to_objects_mapping.get(functionality_1)) - 1)
                          + "]: "))
        if (object_1_index in range(0, len(functionality_to_objects_mapping.get(functionality_1)))):
            input_is_invalid = False
        else:
            print("Invalid object index")

    input_is_invalid = True
    while input_is_invalid:
        object_2_index = int(
                    raw_input("Please choose object index for functionality 2 ("
                    + functionality_2
                    + ") [0-"
                    + str(len(functionality_to_objects_mapping.get(functionality_2)) -1)
                    + "]: "))
        if (object_2_index in range(0, len(functionality_to_objects_mapping.get(functionality_2)))):
            input_is_invalid = False
        else:
            print("Invalid object index")
    ###

    # object_1 = functionality_to_objects_mapping.get(functionality_1)[0]
    # object_2 = functionality_to_objects_mapping.get(functionality_2)[0]
    object_1 = functionality_to_objects_mapping.get(functionality_1)[object_1_index]
    object_2 = functionality_to_objects_mapping.get(functionality_2)[object_2_index]
    print("Objects selected for combination: {0} and {1}".format(object_1, object_2))

    # tools_reconstructions
    functionality_names = [functionality_1, functionality_2]

    # Loop here through the combination/overwrite percentages that you need.
    generate_and_save_reconstructions(
        object_1, #'laptop', # object_class_1_name,
        object_2, #'chair', #object_class_2_name,
        functionality_names,
        combination_percentages)
        #combination_percentages[index]) #top_percentage,0.8
