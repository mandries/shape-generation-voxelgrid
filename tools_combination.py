# This class contains the tools for combining object descriptions

from __future__ import division, print_function, absolute_import
import argparse # for parsing arguments
import sys # for counting the number of arguments

import tensorflow as tf
import os # to walk through directories, to rename files
import time
import numpy as np
from matplotlib import pyplot as plt

# See which of these imports are really used
import binvox_rw # to manipulate binvox files
import pcl_tools
import tools_dataset_processing
import tools_combination
import model_vae_3d
# I guess we should not import this one
import vae_aff_combine_multiple_samples_and_inspect as basics

def save_voxelgrid(voxelgrid, directory, filename):
    # Write as voxelgrid (binvox format)
    print("Voxelgrid shape for saving: ")
    print(voxelgrid.shape)
    voxelgrid = voxelgrid > 0.5 # thresholding
    data = voxelgrid
    dims = (64,64,64) # DEFAULT voxelgrid size here (not true size, which may vary)
    translate = (0,0,0)
    scale = 1
    axis_order = 'xyz'
    voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
    # Specify the file path
    filename = os.path.join(directory, filename + ".binvox")
    filewriter = open(filename, 'w')
    # Write the binvox model to file (so that it can be seen through viewvow)
    binvox_rw.write(voxel_model, filewriter)

def save_combined_samples(
        input_sample_1,
        input_sample_2,
        output_recon1_sample_1,
        output_recon1_sample_2,
        output_recon_combined):

    # Generate a folder to store the images
    directory = "combinations/combination_" + str(time.time()) # + "iter_{0}".format(iteration_number)
    if not os.path.exists(directory):
        os.makedirs(directory)

    save_voxelgrid(input_sample_1, directory, "sample_1_orig") # filename
    save_voxelgrid(input_sample_2, directory, "sample_2_orig") # filename
    save_voxelgrid(output_recon1_sample_1, directory, "sample_1_recon") # filename
    save_voxelgrid(output_recon1_sample_2, directory, "sample_2_recon") # filename
    save_voxelgrid(output_recon_combined, directory, "sample_combined") # filename

    print('done')
    return


def save_affordance_combination_in_directory(
        functionality_forms,
        combination_form,
        top_percentage,
        directory,
        save_class_essences,
        functionality_names=[""]):
        #object_names=[""]):
  if (save_class_essences):
    print('Saving object class essences and their combinations... ')
    save_voxelgrid(functionality_forms[0][0], directory,
                   "functionality_0_essence_" + functionality_names[0])
    save_voxelgrid(functionality_forms[1][0], directory,
                   "functionality_1_essence_" + functionality_names[1])

  # for sample_index in range(0, combinations.shape[0]):
  # filename = "combination_" + str(sample_index)
  filename = "combination_" + str(top_percentage)
  # save_voxelgrid(combinations[sample_index], directory, filename)
  save_voxelgrid(combination_form[0], directory, filename)

  print('done')
  return


def save_object_class_combination_in_directory(
        object_class_essences,
        combination,
        top_percentage,
        directory,
        save_class_essences,
        functionality_names=[""],
        object_names=[""]):

    if (save_class_essences):
        print('Saving object class essences and their combinations... ')
        save_voxelgrid(object_class_essences[0][0], directory, "object_class_0_essence_" + object_names[0])
        save_voxelgrid(object_class_essences[1][0], directory, "object_class_1_essence_" + object_names[1])

    # for sample_index in range(0, combinations.shape[0]):
    # filename = "combination_" + str(sample_index)
    filename = "combination_" + str(top_percentage)
    # save_voxelgrid(combinations[sample_index], directory, filename)
    save_voxelgrid(combination[0], directory, filename)

    print('done')
    return


def save_object_class_combination(
        object_class_essences,
        combination,
        top_percentage,
        functionality_names=[""],
        object_names=[""]):

    functionality_suffix = \
        "_func_" + functionality_names[0] + "_" + functionality_names[1]

    # print("Object essences:")
    # print(object_class_essences.shape)

    # Generate a folder to store the images
    print("Generating a folder to save the object_class_essences and combinations")
    combinations_path = '/media/Data/mihai/combinations/'
    directory = combinations_path + "category_combination_" + str(time.time()) + functionality_suffix
    if not os.path.exists(directory):
        os.makedirs(directory)

    print('Saving object class essences and their combinations... ')
    save_voxelgrid(object_class_essences[0][0], directory, "object_class_0_essence_" + object_names[0])
    save_voxelgrid(object_class_essences[1][0], directory, "object_class_1_essence_" + object_names[1])

    # for sample_index in range(0, combinations.shape[0]):
    # filename = "combination_" + str(sample_index)
    filename = "combination_" + str(top_percentage)
    # save_voxelgrid(combinations[sample_index], directory, filename)
    save_voxelgrid(combination[0], directory, filename)

    print('done')
    return

# This function computes, for a given object latent vector representation (means, log_vars), the KL divergence between this object and the representation of a void volume.
# All the variables with a KL_divergence above the given threshold will be marked as important. The rest will be marked as non-important.
#
# These important/non-important marks are useful when combining the representations of 2 different objects.
def compute_KL_div_with_void(
    object_z_means,
    object_z_log_vars,
    void_z_means,
    void_z_log_vars,
    kl_div_threshold):

    # Vector that will contain the KL divs
    kl_divs = []

    # for variable_index in range(0, len(object_z_means)):
    for variable_index in range(0, object_z_means.shape[0]):

        mean1 = object_z_means[variable_index] # object mean
        mean2 =   void_z_means[variable_index] # void mean
        var1 = np.exp(object_z_log_vars[variable_index]) # object variance
        var2 = np.exp(  void_z_log_vars[variable_index]) # void variance
        # Compute the KL div
        kl_divs[variable_index] = kl_div_between_two_gaussians(mean1, mean2, var1, var2)

    # This vector will contain the importance value
    # for each variable in the latent representation of the given object
    importance_vector = []
    importance_vector = kl_divs > kl_div_threshold # vector > scalar = thresholding into values [1/0]

    return importance_vector


# Compute the KL div between two gaussians
# WARNING: feed the variances (sigma^2) here, not the log_vars
def kl_div_between_two_gaussians(mean1, mean2, var1, var2):

    stddev1 = np.sqrt(var1)
    stddev2 = np.sqrt(var2)

    kldiv = np.log( stddev1 / stddev2 ) + \
            (
                (stddev1 + np.power(mean1 - mean2, 2) )
                /
                (2.0 * var2)
            ) \
            - (1.0/2)

    return kldiv


# This function attempts to combine/merge 2 object descriptions, if possible.
# The precise manner in which this combination/merging is done remains to be defined.
# def combine_object_descriptions(description1, description2, latent_dim_size)
# source: https://www.desmos.com/calculator/3whdhggc4u
def combine_object_descriptions(
        desc1_means_layer, desc1_log_vars_layer,
        desc2_means_layer, desc2_log_vars_layer):

    combined_means_layer = \
        np.divide   (
                    np.add  (
                            np.multiply(desc1_means_layer, desc2_log_vars_layer),
                            np.multiply(desc2_means_layer, desc1_log_vars_layer)
                            ),
                    np.add(desc1_log_vars_layer, desc2_log_vars_layer)
                    )
    combined_log_vars_layer = \
        np.divide   (
                    1,
                    np.add  (
                                np.divide(1, desc1_log_vars_layer),
                                np.divide(1, desc2_log_vars_layer)
                            )
                    )

    return combined_means_layer, combined_log_vars_layer


def combine_object_descriptions_tf(
        desc1_means_layer, desc1_log_vars_layer,
        desc2_means_layer, desc2_log_vars_layer):

    combined_means_layer = \
        tf.divide   (
                    tf.add  (
                            tf.multiply(desc1_means_layer, desc2_log_vars_layer),
                            tf.multiply(desc2_means_layer, desc1_log_vars_layer)
                            ),
                    tf.add(desc1_log_vars_layer, desc2_log_vars_layer)
                    )

    combined_log_vars_layer = \
        tf.divide   (
                    1,
                    tf.add  (
                                tf.divide(1, desc1_log_vars_layer),
                                tf.divide(1, desc2_log_vars_layer)
                            )
                    )

    printf("Combined means layer shape:")
    print(combined_means_layer.shape)

    printf("Combined log vars layer shape:")
    print(combined_log_vars_layer.shape)

    return combined_means_layer, combined_log_vars_layer

# Code for combining multiple object descriptions,
# by multiplicating their corresponding gaussian probability distributions
def combine_multiple_object_descriptions_through_gaussian_intersection_tf(
    objects_z_means,      # objects (batch_size) x latent dim
    objects_z_log_vars,
    total_objects):  # objects (batch_size) x latent dim

    # Switch from log_vars to variances
    objects_variances = tf.exp(objects_z_log_vars)

    # Equations 4 and 5 of the file "product_of_gaussians_2003-003.pdf"
    inv_log_vars_fg_sum_n = 0. # log_vars
    sum_means_n = 0. # means

    # TODO correct here the code, removing the loop, but keeping the same functionality
    # for index_object in range (0, vae_aff_combine_multiple_samples.batch_size): #len(object_descriptions)):
    for index_object in range (0, total_objects):
        # 1. Compute the log vars: sum all the inv_log_vars
        # inv_log_vars_fg_n += tf.div(1, object_descriptions[index_log_vars].log_vars)
        inv_vars_fg_sum_n = \
            tf.add(
                inv_log_vars_fg_sum_n,
                tf.div(1., objects_variances[index_object])
            )
        # 2. Compute the means
        sum_means_n = \
            tf.add(
                sum_means_n,
                tf.div(
                    # object_descriptions[index_object].means,
                    # object_descriptions[index_object].log_vars,
                    objects_z_means[index_object],
                    objects_variances[index_object]
                )
            )

    # Invert the value, to get the final result
    combined_vars_layer = tf.div(1., inv_vars_fg_sum_n)
    # Switch back to log_vars from variances
    combined_log_vars_layer = tf.log(combined_vars_layer)
    combined_means_layer = tf.multiply( \
                        sum_means_n, \
                        combined_log_vars_layer # log_vars_fg_n \
                    )

    return combined_means_layer, combined_log_vars_layer


# Code for combining multiple object descriptions,
# by multiplicating their corresponding gaussian probability distributions
def combine_multiple_object_descriptions_through_averaging_tf(
    objects_z_means,      # objects (batch_size) x latent dim
    objects_z_log_vars):  # objects (batch_size) x latent dim

    # # Switch from log_vars to variances
    # objects_variances = tf.exp(objects_z_log_vars)
    #
    # # Equations 4 and 5 of the file "product_of_gaussians_2003-003.pdf"
    # sum_variances_n = 0. # log_vars
    # sum_means_n = 0. # means
    #
    # # for index_object in range (0, vae_aff_combine_multiple_samples.batch_size): #len(object_descriptions)):
    # for index_object in range (0, total_objects):
    #     # 1. Compute the log vars: sum all the inv_log_vars
    #     # inv_log_vars_fg_n += tf.div(1, object_descriptions[index_log_vars].log_vars)
    #     sum_variances_n = \
    #         tf.add(
    #             sum_variances_n,
    #             objects_variances[index_object]
    #         )
    #     # 2. Compute the means
    #     sum_means_n = \
    #         tf.add(
    #             sum_means_n,
    #             objects_z_means[index_object]
    #         )
    # # Invert the value, to get the final result
    # combined_vars_layer = tf.div(sum_variances_n, total_objects)
    #
    # # Switch back to log_vars from variances
    # combined_log_vars_layer = tf.log(combined_vars_layer)
    #
    # combined_means_layer = tf.div( \
    #                     sum_means_n, \
    #                     total_objects
    #                 )
    # return combined_means_layer, combined_log_vars_layer

    combined_log_vars_vector = tf.reduce_mean(objects_z_log_vars, axis=0, keepdims=True)
    combined_means_vector = tf.reduce_mean(objects_z_means, axis=0, keepdims=True)

    print("Shapes of log_vars_vector and means_vector:")
    print(combined_log_vars_vector.shape)
    print(combined_means_vector.shape)

    return combined_means_vector, combined_log_vars_vector


# Shape arithmetic on latent vector representations of objects: add
def shape_arithmetic_on_latent_vector_add(
        desc1_means_layer, desc1_log_vars_layer,
        desc2_means_layer, desc2_log_vars_layer):

    combined_means_layer = tf.add(desc1_means_layer, desc2_means_layer)
    combined_log_vars_layer = tf.add(desc1_log_vars_layer, desc2_log_vars_layer)

    return combined_means_layer, combined_log_vars_layer

# Shape arithmetic on latent vector representations of objects: subtract
def shape_arithmetic_on_latent_vector_subtract(
        desc1_means_layer, desc1_log_vars_layer,
        desc2_means_layer, desc2_log_vars_layer):

    combined_means_layer = tf.subtract(desc1_means_layer, desc2_means_layer)
    combined_log_vars_layer = tf.subtract(desc1_log_vars_layer, desc2_log_vars_layer)

    return combined_means_layer, combined_log_vars_layer

#
# Input: a pair of type (means, log_variances)
# Output: multiple samples? AND NOT pairs of means, log_variances)
def explore_latent_variable_effect(
        combined_object_description,
        # The means and log_vars from which the combined object description was sampled
        combined_means_vector,
        combined_log_vars_vector,
        # The identifier of variable whose role needs to be inspected
        top_kl_indices # latent_variable_id_input
        # session
        ):

    print("CALL")

    # print("Shape of combined_means_vector")
    # print(combined_means_vector.shape) # (1,512)

    # print("Shape of combined_means_vector[latent_variable_id]")
    # print(combined_means_vector[0,latent_variable_id].shape) # (1,512) -> ()
    #
    # print("combined_object_description shape:")
    # print(combined_object_description.shape) # (1, 512)

    # Create a tensor that would point where to cut
    # variable_tensor = top_kl_indices[0]


    # begin_location = tf.slice (input_ = top_kl_indices, begin=[0], size=[1]) # shape: (1,)
    # print("begin_location.shape:")
    # print(begin_location.shape)
    top_variable_location_in_latent_vector = \
        tf.slice (input_ = top_kl_indices, begin=[0], size=[1]) # shape: (1,)

    # Get the mean value corresponding to the (most important) variable
    mean_value_tensor = tf.slice(   input_ = combined_means_vector[0], # (512)
                                    begin = top_variable_location_in_latent_vector, #top_kl_indices[0],
                                    size = [1] )
    # Get the log var value corresponding to the (most important) variable
    log_var_value_tensor = tf.slice(input_ = combined_log_vars_vector[0], # (512)
                                    begin = top_variable_location_in_latent_vector, #top_kl_indices[0],
                                    size = [1] )

    print("Shape of combined_object_description:")
    print(tf.shape(combined_object_description))
    print(combined_object_description.shape)
    # print(tf.shape(combined_object_description[0]))
    print(combined_object_description[0].shape)


    z_combined = combined_object_description[0] # shape = (1,512) -> (512)
    print("Shape of z_combined:")
    print(tf.shape(z_combined))

    tensor_before_value = tf.slice(
                            input_ = z_combined,
                            begin = [0],
                            size = top_variable_location_in_latent_vector)
    tensor_at_value = tf.slice(
                            input_ = z_combined,
                            begin = top_variable_location_in_latent_vector,
                            size = [1])
    tensor_after_value = tf.slice(
                            input_ = z_combined,
                            begin = tf.add(top_variable_location_in_latent_vector, 1),
                            size = tf.abs(tf.subtract(top_variable_location_in_latent_vector, (model_vae_3d.latent_dim-1))))
    # how many values are left after top value?
    # tf.abs(tf.subtract(top_variable_location_in_latent_vector, z_combined.shape()[0]))
    print("\nSize of Tensor before value:")
    print(tf.shape(tensor_before_value))
    print(tensor_before_value.shape)
    print("Size of Tensor at value:")
    print(tf.shape(tensor_at_value))
    print(tensor_at_value.shape)
    print("Size of Tensor after value:")
    print(tf.shape(tensor_after_value))
    print(tensor_after_value.shape)


    # Compute the new values for the most important variable (judging by KL divergence)
    new_value_is_mean_less_stddev = tf.subtract(mean_value_tensor, log_var_value_tensor)
    new_value_is_mean = mean_value_tensor
    new_value_is_mean_plus_stddev = tf.add(mean_value_tensor, log_var_value_tensor)

    # Create the first object (mean less stddev)
    object_mean_less_stddev = tf.concat(
        values = [tensor_before_value,
        new_value_is_mean_less_stddev, #tensor_at_value
        tensor_after_value],
        axis=0
        )

    # Create the second object (mean)
    object_mean = tf.concat(
        values = [tensor_before_value,
        new_value_is_mean, #tensor_at_value
        tensor_after_value],
        axis=0
        )

    # Create the third object (mean plus stddev)
    object_mean_plus_stddev = tf.concat(
        values = [tensor_before_value,
        new_value_is_mean_plus_stddev, #tensor_at_value
        tensor_after_value],
        axis=0
        )

    print("\nShape of generated object (object_mean_less_stddev):")
    print(object_mean_less_stddev.shape)
    print("Shape of generated object (object_mean):")
    print(object_mean.shape)
    print("Shape of generated object (object_mean_plus_stddev):")
    print(object_mean_plus_stddev.shape)

    # Stack the three objects to generate the result
    resulting_samples = tf.stack([  object_mean_less_stddev,
                                    object_mean,
                                    object_mean_plus_stddev])
    print("\nShape of resulting_samples:")
    print(resulting_samples.shape)
    print(tf.shape(resulting_samples))

    # Return the resulting objects, and their number
    return resulting_samples, 3 #resulting_samples.shape()[0]

    # z_combined_modified = combined_object_description[0] # shape = (1,512) -> (512)
    # z_combined_modified_list = tf.unstack(z_combined_modified) #, axis=1) # len = 512
    #
    # print("z_combined_modified_list length:")
    # print(len(z_combined_modified_list))
    #
    # # Our expectation is to see the following sizes:
    # z_combined_modified_list[latent_variable_id] = \
    #     tf.subtract(mean_value_tensor, tf.exp(log_var_value_tensor))
    #     # tf.subtract(mean_value_tensor, 0.5)
    # # z_combined_modified_list[latent_variable_id] = tf.reshape(z_combined_modified_list[latent_variable_id], shape=[1])
    # z_combined_modified_list[latent_variable_id] = \
    #     tf.reshape(z_combined_modified_list[latent_variable_id], shape=[])
    # object_1 = tf.squeeze(tf.stack(z_combined_modified_list)) #, axis=-1) # to have again (1,512)
    #
    # print("Shape of z_combined_modified_list[latent_variable_id]")
    # print(z_combined_modified_list[latent_variable_id].shape)
    # #
    # #
    # z_combined_modified_list[latent_variable_id] = mean_value_tensor
    #         # combined_means_vector[0,latent_variable_id] - \
    #         # tf.exp(combined_log_vars_vector[0,latent_variable_id])
    # # z_combined_modified_list[latent_variable_id] = tf.reshape(z_combined_modified_list[latent_variable_id], shape=[1])
    # z_combined_modified_list[latent_variable_id] = tf.reshape(z_combined_modified_list[latent_variable_id], shape=[])
    # object_3 = tf.squeeze(tf.stack(z_combined_modified_list)) #, axis=-1) # to have again (1,512)
    # #
    # #
    # z_combined_modified_list[latent_variable_id] = \
    #     tf.add(mean_value_tensor, tf.exp(log_var_value_tensor))
    #     # tf.add(mean_value_tensor, 0.5)
    #         # combined_means_vector[0,latent_variable_id] - \
    #         # tf.exp(combined_log_vars_vector[0,latent_variable_id])
    # # z_combined_modified_list[latent_variable_id] = tf.reshape(z_combined_modified_list[latent_variable_id], shape=[1])
    # z_combined_modified_list[latent_variable_id] = tf.reshape(z_combined_modified_list[latent_variable_id], shape=[])
    # object_5 = tf.squeeze(tf.stack(z_combined_modified_list)) #, axis=-1) # to have again (1,512)
    #
    # print("Shapes: {0} = {1}".format(object_1.shape, combined_object_description.shape))
    #
    # # Put the obtained samples into the result
    # resulting_samples = tf.stack([object_1, object_3, object_5])
    # # resulting_samples = tf.stack([object_1, object_1, object_1])
    # print("Variations: Resulting_samples shape:")
    # print(resulting_samples.shape)
    #
    # return resulting_samples, 3 # 5 = total samples
