# set terminal pngcairo  transparent enhanced font "arial,8" fontscale 1.0 size 660, 320
# set output 'finance.1.png'

# set terminal postscript landscape color 20;
# set terminal postscript eps
set terminal postscript enhanced color eps

set out "myplot.ps";
#set multiplot;# allows multiple plots on the same page
#set nokey;#omits the legend in the right hand corner
set ylabel 'error';
set xlabel 'epoch';

# set xtics 0,10
# set ytics  norangelimit
# set ytics   (80.0000, 85.0000, 90.0000, 95.0000, 100.000, 105.000)
set title "VAE Traning vs Test error (3D ShapeNets dataset)"
# set xrange [ 50.0000 : 253.000 ] noreverse nowriteback
set yrange [ 0 : ] noreverse nowriteback
# set lmargin  9
# set rmargin  2
# DEBUG_TERM_HTIC = 95
# DEBUG_TERM_VTIC = 95
## Last datafile plotted: "finance.dat"
plot \
    'training_log 1525795930.txt' using 0:2 with lines title 'Training error', \
    'training_log 1525795930.txt' using 0:3 with lines title 'Test error', \
    'training_log 1525795930.txt' using 0:4 with lines title 'Void reconstruction error', \
# pause -1
# quit
