from argparse import ArgumentParser
import os

# default arguments
CONTENT_WEIGHT = 5e0
CONTENT_WEIGHT_BLEND = 1
STYLE_WEIGHT = 5e2
TV_WEIGHT = 1e2
STYLE_LAYER_WEIGHT_EXP = 1
LEARNING_RATE = 1e1
BETA1 = 0.9
BETA2 = 0.999
EPSILON = 1e-08
STYLE_SCALE = 1.0
ITERATIONS = 1000
VGG_PATH = 'imagenet-vgg-verydeep-19.mat'
POOLING = 'max'

def build_parser():
    parser = ArgumentParser()

    parser.add_argument("-b", "--with_softbits",
                        help="Activate training with softbits (makes the network use all of its capacity).",
                        action="store_true")
    parser.add_argument("-r", "--restore_params",
                        help="Restore the parameters in the graph from the given input file.",
                        action="store_true")
    parser.add_argument("-s", "--save_params",
                        help="Save the parameters in the graph to some output file.",
                        action="store_true")
    parser.add_argument("-q", "--quick_test",
                        dest="is_quick_test",
                        help="Quick test mode (batch_size=2; dataset: small).",
                        default=False,
                        action="store_true")
    parser.add_argument("-g", "--single_gpu",
                        dest="single_gpu",
                        default=False,
                        help="Run the training on a single GPU (instead of blocking all GPUs).",
                        action="store_true")
    parser.add_argument("-u", "--uniform_sampling",
                        help="Run the training with uniform sampling when computing the loss.",
                        action="store_true")

    # parser.add_argument('--content',
    #         dest='content', help='content image',
    #         metavar='CONTENT', required=True)
    #
    # parser.add_argument('--styles',
    #         dest='styles',
    #         nargs='+', help='one or more style images',
    #         metavar='STYLE', required=True)
    #
    # parser.add_argument('--output',
    #         dest='output', help='output path',
    #         metavar='OUTPUT', required=True)

    parser.add_argument('--iterations', type=int,
            dest='iterations', help='iterations (default %(default)s)',
            metavar='ITERATIONS', default=ITERATIONS)

    # parser.add_argument('--print-iterations', type=int,
    #         dest='print_iterations', help='statistics printing frequency',
    #         metavar='PRINT_ITERATIONS')
    #
    # parser.add_argument('--checkpoint-output',
    #         dest='checkpoint_output',
    #         help='checkpoint output format, e.g. output_{:05}.jpg or '
    #              'output_%%05d.jpg',
    #         metavar='OUTPUT', default=None)
    #
    # parser.add_argument('--checkpoint-iterations', type=int,
    #         dest='checkpoint_iterations', help='checkpoint frequency',
    #         metavar='CHECKPOINT_ITERATIONS', default=None)
    #
    # parser.add_argument('--progress-write', default=False, action='store_true',
    #         help="write iteration progess data to OUTPUT's dir",
    #         required=False)
    #
    # parser.add_argument('--progress-plot', default=False, action='store_true',
    #         help="plot iteration progess data to OUTPUT's dir",
    #         required=False)
    #
    # parser.add_argument('--width', type=int,
    #         dest='width', help='output width',
    #         metavar='WIDTH')
    #
    # parser.add_argument('--style-scales', type=float,
    #         dest='style_scales',
    #         nargs='+', help='one or more style scales',
    #         metavar='STYLE_SCALE')
    #
    # parser.add_argument('--network',
    #         dest='network', help='path to network parameters (default %(default)s)',
    #         metavar='VGG_PATH', default=VGG_PATH)
    #
    # parser.add_argument('--content-weight-blend', type=float,
    #         dest='content_weight_blend',
    #         help='content weight blend, conv4_2 * blend + conv5_2 * (1-blend) '
    #              '(default %(default)s)',
    #         metavar='CONTENT_WEIGHT_BLEND', default=CONTENT_WEIGHT_BLEND)
    #
    # parser.add_argument('--content-weight', type=float,
    #         dest='content_weight', help='content weight (default %(default)s)',
    #         metavar='CONTENT_WEIGHT', default=CONTENT_WEIGHT)
    #
    # parser.add_argument('--style-weight', type=float,
    #         dest='style_weight', help='style weight (default %(default)s)',
    #         metavar='STYLE_WEIGHT', default=STYLE_WEIGHT)
    #
    # parser.add_argument('--style-layer-weight-exp', type=float,
    #         dest='style_layer_weight_exp',
    #         help='style layer weight exponentional increase - '
    #              'weight(layer<n+1>) = weight_exp*weight(layer<n>) '
    #              '(default %(default)s)',
    #         metavar='STYLE_LAYER_WEIGHT_EXP', default=STYLE_LAYER_WEIGHT_EXP)
    #
    # parser.add_argument('--style-blend-weights', type=float,
    #         dest='style_blend_weights', help='style blending weights',
    #         nargs='+', metavar='STYLE_BLEND_WEIGHT')

    # parser.add_argument('--tv-weight', type=float,
    #         dest='tv_weight',
    #         help='total variation regularization weight (default %(default)s)',
    #         metavar='TV_WEIGHT', default=TV_WEIGHT)

    # parser.add_argument('--learning-rate', type=float,
    #         dest='learning_rate', help='learning rate (default %(default)s)',
    #         metavar='LEARNING_RATE', default=LEARNING_RATE)

    # parser.add_argument('--beta1', type=float,
    #         dest='beta1', help='Adam: beta1 parameter (default %(default)s)',
    #         metavar='BETA1', default=BETA1)
    #
    # parser.add_argument('--beta2', type=float,
    #         dest='beta2', help='Adam: beta2 parameter (default %(default)s)',
    #         metavar='BETA2', default=BETA2)
    #
    # parser.add_argument('--eps', type=float,
    #         dest='epsilon', help='Adam: epsilon parameter (default %(default)s)',
    #         metavar='EPSILON', default=EPSILON)

    # parser.add_argument('--initial',
    #         dest='initial', help='initial image',
    #         metavar='INITIAL')

    # parser.add_argument('--initial-noiseblend', type=float,
    #         dest='initial_noiseblend',
    #         help='ratio of blending initial image with normalized noise '
    #              '(if no initial image specified, content image is used) '
    #              '(default %(default)s)',
    #         metavar='INITIAL_NOISEBLEND')
    #
    # parser.add_argument('--preserve-colors', action='store_true',
    #         dest='preserve_colors',
    #         help='style-only transfer (preserving colors) - if color transfer '
    #              'is not needed')
    #
    # parser.add_argument('--pooling',
    #         dest='pooling',
    #         help='pooling layer configuration: max or avg (default %(default)s)',
    #         metavar='POOLING', default=POOLING)

    parser.add_argument('--overwrite', action='store_true', dest='overwrite',
            help='write file even if there is already a file with that name')

    # TODO:
    # use this directory name: directory
    # use this much memory percentage: memo
    #

    return parser

def apply_options(options):

    # Number of GPUs used
    if (options.single_gpu):
        print("Using (blocking) only one GPU.")
        # Use the memory of only 1 GPU
        os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    else:
        print("Using (blocking) ALL GPUs.")

    # width = options.width
    # print("Width arg = {}".format(width))



# Example of usage:
if __name__ == '__main__':
    parser = build_parser()
    options = parser.parse_args()
