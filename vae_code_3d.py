# This Variational AutoEncoder learns a latent representation for
# objects from the input datasets.

from __future__ import division, print_function, absolute_import
#~ from array import *
#~ from random import shuffle

import argparse # for parsing arguments
import sys # for counting the number of arguments

import numpy as np
#import matplotlib.pyplot as plt
#~ import scipy
#~ import scipy.stats
#~ from scipy.stats import norm
import tensorflow as tf
# import time # for pausing the program
# import pcl_tools
import time # to know how long it takes for the code to run
# Libraries for working with images
#import cv2
import os # to walk through directories, to rename files

import binvox_rw # to manipulate binvox files
import tools_dataset_processing
import tools_reconstructions
import model_vae_3d

# Libraries for working with point clouds
# import pcl # http://pointclouds.org/news/tags/python

# The welcome message
print("Start: AutoEncoder for Learning features. Program is running.")
print("Recommended way to run the training code is:")
print("python vae_code_3d.py --single_gpu --with_softbits --save_params \n")
# --uniform_sampling
# Introduce the argument parser
# (useful if you want to load pre-learned parameters, or start learning from zero)
parser = argparse.ArgumentParser()
parser.add_argument("-b", "--with_softbits",
                help="Activate training with softbits (makes the network use all of its capacity).",
                action="store_true")
parser.add_argument("-r", "--restore_params",
                help="Restore the parameters in the graph from the given input file.",
                action="store_true")
parser.add_argument("-s", "--save_params",
                help="Save the parameters in the graph to some output file.",
                action="store_true")
parser.add_argument("-q", "--quick_test",
                help="Quick test mode (batch_size=2; dataset: small).",
                action="store_true")
parser.add_argument("-g", "--single_gpu",
                help="Run the training on a single GPU (instead of blocking all GPUs).",
                action="store_true")
parser.add_argument("-u", "--uniform_sampling",
                help="Run the training with uniform sampling when computing the loss.",
                action="store_true")

# Name to be used by the subroutines saving data to disk (recons, logs, etc.)
execution_name = "generated_vae_code_3d.py_" + str(int(time.time()))
epoch_offset = 62 #0 # 24

args = parser.parse_args()

if (args.restore_params):
    print ("Restore params enabled")
    execution_name += "_r"
else:
    print ("Restore params disabled: learning from zero")

saver = 0
if (args.save_params):
    print("Saving parameters enabled")
    execution_name += "_s"
else:
    print("Saving parameters disabled")

if (args.with_softbits):
    print("Softbits enabled")
    execution_name += "_b"
else:
    print("Softbits disabled")

if (args.quick_test):
    print("Quick testing enabled")
    execution_name += "_q"
else:
    print("Quick testing disabled")

if (args.single_gpu):
    print("Using (blocking) only one GPU.")
    # Use the memory of only 1 GPU
    os.environ["CUDA_VISIBLE_DEVICES"]="0"
else:
    print("Using (blocking) ALL GPUs.")

if (args.uniform_sampling):
    print("Uniform sampling for computing the loss enabled")
    execution_name += "_u"
else:
    print("Uniform sampling for computing the loss disabled")

################################################################################
################################## Constants ###################################
################################################################################

##### Constants #####
# The array containing the (3d) image data
dataset_train_container = [] # training data
dataset_test_container = [] # testing data
dataset_void_container = [] # void volume

# Parameters
learning_rate = 0.002
total_epochs = 10000
batch_size = 128

# Check if this is a quick launch
if (args.quick_test):
    # Batch_size: 2 (for fast loading)
    batch_size = 2

#inv_frequency_of_hallucinations_generation = 100
# check reconstruction quality every N batches (from noise)
#total_hallucinations_generated_per_check = 20
# check reconstruction quality every N batches (from real images)
inv_frequency_of_reconstruction_evaluations = 1
inv_frequency_of_parameters_saving = 1 # save after every epoch

### Dataset data ###
# Description of the input
voxelized_input_width = 64
voxelized_input_height = 64
voxelized_input_depth = 64

nn_img_input_channels = 1 # 4 # (3 color + 1 depth)


# Dataset root location
dataset_root_location = '/media/Data/datasets/'
dataset_name = 'ModelNet40_binvox_exact_center_4views/'
short_dataset_name = dataset_name[:10]
#dataset_root_location_on_plinio = '/home/mihai/Data/datasets';
#dataset_root_location = dataset_root_location_on_plinio; # TODO change here if required

# Input file format
input_file_format_suffix = '.binvox' # (voxelized representation)

# Path from where the most recent model is loaded
models_path = '/media/Data/mihai/models/'
model_loading_path = models_path + \
                    'model_ModelNet40__t_1530717673__epoch_72__train_52105__test_90524__void_998/' + \
                    'model_feature_learning_3D.ckpt'

################################################################################
################################# Functions ####################################
################################################################################


################################################################################
############################# Building the graph ###############################
################################################################################


# with tf.device('/gpu:0'):
# sys.exit(0)
# The input of the encoder
input_placeholder = tf.placeholder(
                    tf.float32,
                    shape=[
                            None, # the size of the batch
                            voxelized_input_width,
                            voxelized_input_height,
                            voxelized_input_depth
                          ]
                          # nn_img_input_channels
                    )

# The output of the decoder
target_placeholder = input_placeholder # the thing we are trying to reconstruct

# Capture the output of the encoder
with tf.variable_scope("model"):
    z_mean, z_log_var = model_vae_3d.encoder_net(input_placeholder)
    print("shape of z_mean (decoder input): {}".format(z_mean.shape))
    print("shape of z_log_var (decoder input): {}".format(z_log_var.shape))
    # Sampler: Normal (gaussian) random distribution
    eps = tf.random_normal(
                tf.shape(z_log_var),
                dtype=tf.float32,
                mean=0.,
                stddev=1.0,
                name='epsilon')
    print("shape of eps (encoder output): {}".format(eps.shape))
    # z = z_mean + tf.exp(z_log_var / 2) * eps
    z = tf.add(z_mean, tf.exp(z_log_var / 2) * eps) # TODO WHY do we divide here by 2 ?

    print("shape of z (decoder input): {}".format(z.shape))

    # the actual reconstruction
    #with tf.variable_scope("model", reuse=True):
    output_voxelized_volume = model_vae_3d.decoder_net(z)

gamma_factor = tf.Variable(
                    initial_value=np.ones((model_vae_3d.latent_dim)) * 1e-2,
                    trainable=False,
                    name="gamma_factor_for_softbits",
                    dtype=tf.float32)

# The weight for giving more feedback for correctly filled voxels
# to encourage generation of objects instead of void volumes
reconstruction_loss_pos_weight_placeholder = tf.placeholder(
                                                tf.float32,
                                                shape=[] # scalar
                                                )

# Compute the loss
reconstruction_loss = 0.0
if (args.uniform_sampling):
    reconstruction_loss = model_vae_3d.reconstruction_loss_uniformly_sampled(
                                x_reconstructed = output_voxelized_volume,
                                x_true = target_placeholder)
else:
    reconstruction_loss = model_vae_3d.reconstruction_loss_weighted(
                                x_reconstructed = output_voxelized_volume,
                                x_true = target_placeholder,
                                pos_weight_value=reconstruction_loss_pos_weight_placeholder)
#
# reconstruction_loss = model_vae_3d.reconstruction_loss(
#                             x_reconstructed = output_voxelized_volume,
#                             x_true = target_placeholder)
#
kl_loss = 0.0

# IF USING SOFTBITS, THEN THIS
if (args.with_softbits):
    kl_loss_per_dim = model_vae_3d.kl_loss_per_dim(
                    z_mean = z_mean,
                    z_log_var = z_log_var
                    )
    #
    lambda_per_dim = 0.01
    information_threshold = 0.1
    gamma_rate = 0.1
    gamma_factor = tf.where(kl_loss_per_dim<(1-information_threshold)*lambda_per_dim,
                            gamma_factor*(1-gamma_rate),
                            gamma_factor)

    gamma_factor = tf.where(kl_loss_per_dim>(1+information_threshold)*lambda_per_dim,
                            tf.minimum(gamma_factor*(1+gamma_rate),1.0),
                            gamma_factor)

    kl_loss = tf.reduce_sum(tf.multiply(gamma_factor, kl_loss_per_dim))
    print("KL loss (with softbits):")
    print(kl_loss)
    print("Shape of KL loss (with softbits):")
    print(kl_loss.shape)
    print(tf.shape(kl_loss))


# IF NOT USING SOFTBITS
else:
    if (args.with_softbits == False):
        kl_loss = model_vae_3d.kl_loss(
                        z_mean = z_mean,
                        z_log_var = z_log_var
                        )
        print("KL loss (without softbits):")
        print(kl_loss)
        print("Shape of KL loss (without softbits):")
        print(kl_loss.shape)
        print(tf.shape(kl_loss))

# TODO magic here (find a proper solution, maybe using Pareto front and solution domination)
print("KL loss:")
print(kl_loss)
total_loss = kl_loss + reconstruction_loss
# total_loss = kl_loss*1e-6 + reconstruction_loss



#optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate)
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
training_operation = optimizer.minimize(total_loss)

# epoch_counter = model_vae_3d.increase_epoch_counter(epoch_counter)

# Attempt to make a counter of training epochs
# epoch_counter = tf.Variable(initial_value=0, trainable=False, name="epoch_counter")
#epoch_counter = tf.Variable(initial_value=0, validate_shape=False, trainable=False, name="epoch_counter")

# What are these nodes? Answer: the means and variances over all the learned samples
# z_mean_avg = tf.reduce_mean(z_mean, axis=0)
# z_log_var_avg = tf.reduce_mean(z_log_var, axis=0)

# Placeholder for the noise that will be used for checking the quality of the reconstruction
# hallucination_noise_input_placeholder = \
#         tf.placeholder(tf.float32, shape=[None, model_vae_3d.latent_dim]) # None = Batch size
# with tf.variable_scope("model", reuse=True):
#     reconstructed_hallucinations = \
#     tf.nn.sigmoid(model_vae_3d.decoder_net(hallucination_noise_input_placeholder))


# Node for reconstructed images
# (output_voxelized_volume is the output of the decoder)
# TODO probably error here, since points need to be in a certain range
reconstructed_samples_tensor = tf.nn.sigmoid(output_voxelized_volume)
# reconstructed_samples_tensor = output_voxelized_volume

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()

# The model (graph) saver (needs to be instanciated after the graph is built)
if ((args.save_params) or (args.restore_params)):
    saver = tf.train.Saver(max_to_keep = 1000)
    # See default parameters of the saver here:
    # https://www.tensorflow.org/api_docs/python/tf/train/Saver

################################################################################
############################## Running the graph ###############################
################################################################################


# def save_hallucinated_images(reconstructed_samples, iteration_number):
#
#     # Generate a folder to store the images
#     directory = "hallucinations_iter_{0}".format(iteration_number)
#     if not os.path.exists(directory):
#         os.makedirs(directory)
#
#     print('Saving hallucinated images for iteration {0}... '.format(iteration_number))
#     for sample_index in range(0, reconstructed_samples.shape[0]):
#         # Write the image to disk
#         filename = os.path.join(   directory,
#                                    "image_loaded_{0}.jpg".format(sample_index)
#                                    )
#         # Convert the image back from [0..1] values to [0..255]
#         reconstructed_image = np.multiply(reconstructed_samples[sample_index], 255.)
#
#         cv2.imwrite(    filename,
#                         reconstructed_image.astype(np.uint8))
#     print('done')
#     return



# def compute_average_Gaussian_distribution_values_over_dataset(
#         z_means_list,
#         z_log_vars_list
#         ):
#
#     z_means_dataset_avg = 0.
#     z_log_vars_dataset_avg = 0.
#
#     # print("Shape of z_means_list and z_log_vars_list: ")
#     # print(len(z_means_list))
#     # print(len(z_log_vars_list))
#     # Convert the lists into numpy arrays.
#
#     z_means_array = np.array(z_means_list)
#     z_log_vars_array = np.array(z_log_vars_list)
#
#     #print("Shapes of the arrays:")
#     #print(z_means_array.shape)
#     #print(z_log_vars_array.shape)
#
#     z_means_dataset_avg = np.average(z_means_array, axis=0)
#     # TODO maybe use max here
#     z_log_vars_dataset_avg = np.average(z_log_vars_array, axis=0)
#
#     #print("Shapes of the averages:")
#     #print(z_means_dataset_avg.shape)
#     #print(z_log_vars_dataset_avg.shape)
#
#     return  z_means_dataset_avg, \
#             z_log_vars_dataset_avg
#
#     # size of computed_z_mean: 128 x 500 = batch_size x latent_layer_dim
#     # size of computed_z_log_var: 128 x 500 = batch_size x latent_layer_dim
#

# start_time = time.time()
# print("Program started at: {0}".format(start_time))
# print(start_time)

object_classes = [\
    'airplane',
    'bathtub',
    'bed',
    'bench',
    'bookshelf',
    'bottle',
    'bowl',
    'car',
    'chair',
    'cone',
    'cup',
    'curtain',
    'desk',
    'door',
    'dresser',
    'flower_pot',
    'glass_box',
    'guitar',
    'keyboard',
    'lamp',
    'laptop',
    'mantel',
    'monitor',
    'night_stand',
    'person',
    'piano',
    'plant',
    'radio',
    'range_hood',
    'sink',
    'sofa',
    'stairs',
    'stool',
    'table',
    'tent',
    'toilet',
    'tv_stand',
    'vase',
    'wardrobe',
    'xbox']
    # + void


# Employ the test sets for evaluating the reconstruction precision
dataset_train_paths = [dataset_root_location + dataset_name + 'void/']
dataset_test_paths = []
# The file containing a void volume
dataset_void_path = [dataset_root_location + dataset_name + 'void/']

# Use small datasets when debugging the code
if (args.quick_test):
    dataset_train_paths.append(dataset_root_location +dataset_name +'sofa/test/')
    dataset_test_paths.append(dataset_root_location +dataset_name +'bathtub/test/')
# Use the big dataset when running the code in production mode
else:
    # Defining the training and testing dataset
    for index in range(len(object_classes)):
        # Training dataset
        dataset_train_paths.append(dataset_root_location + dataset_name + object_classes[index] + '/train/')
        # Testing dataset
        dataset_test_paths.append(dataset_root_location + dataset_name + object_classes[index] + '/test/')

# Go through the dataset
print("Processing the dataset (train) files...")
# Training dataset
for index in range(len(dataset_train_paths)):
    print("Processing the dataset (train): " + dataset_train_paths[index])
    # get_dataset_files_from_path(
    tools_dataset_processing.load_dataset_binvox_64(
        dataset_train_paths[index],
    	dataset_train_container,
        input_file_format_suffix)
assert (len(dataset_train_container) > 0), "The train dataset is empty."

# Go through the dataset
print("\n\nProcessing the dataset (test) files...")
# Testing dataset (used to evaluate the quality of reconstructions for a general case)
for index in range(len(dataset_test_paths)):
    print("Processing the dataset (test): " + dataset_test_paths[index])
    # get_dataset_files_from_path(
    tools_dataset_processing.load_dataset_binvox_64(
        dataset_test_paths[index],
    	dataset_test_container,
        input_file_format_suffix)
assert (len(dataset_test_container) > 0), "The test dataset is empty."

# Load the void volume
print("\n\nLoading the void volume...")
tools_dataset_processing.load_dataset_binvox_64(
    dataset_void_path[0],
    dataset_void_container,
    input_file_format_suffix)
assert (len(dataset_void_container) == 1), "Failed to load the void volume."


# dataset_processing_time = time.time() - start_time
# print("Dataset processing time: ")
# time.strftime("%H:%M:%S", time.gmtime(dataset_processing_time))
# print()


# Do this before you iterate (because we need to feed in an array to tensorflow)
dataset_train_container = np.asarray(dataset_train_container)
dataset_test_container = np.asarray(dataset_test_container)

# Checks
print("Samples in training dataset (de facto): {0}".format(len(dataset_train_container)))
print("Samples in testing dataset (de facto): {0}".format(len(dataset_test_container)))
# print("Samples in dataset (processed): {0}".format(dataset_samples_count))

# Declare the iterator_train
iterator_train = tools_dataset_processing.data_iterator(dataset_train_container, batch_size)
# Declare the iterator_test
iterator_test = tools_dataset_processing.data_iterator(dataset_test_container, batch_size)

#~ # Start Training
#~ # Start a new TF session
sess = tf.Session()

# Run the initializer
if (args.restore_params):
    print("Restoring model from file... {0}".format(model_loading_path))
    saver.restore(sess, model_loading_path) #model_saving_path
    print("done.")
else:
    print("Initializing variables with random numbers")
    sess.run(init)
    print("done.")

# Training
total_train_batches = np.floor(len(dataset_train_container)/batch_size).astype(np.int16) + 1
# Testing
total_test_batches = np.floor(len(dataset_test_container)/batch_size).astype(np.int16) + 1

# Initialize the lists
# z_means_list = list()
# z_log_vars_list = list()

min_train_loss = np.inf # float

timenow = time.strftime("%Y/%m/%d %H:%M:%S", time.gmtime(time.time()))
print("Total batches: {0} \t {1} \t".format(total_train_batches, timenow))

# Compute the ratio of filled vs empty voxels in the dataset
# and use the inverse of that as a multiplier.
# Thus filled volumes would be as important as the complementary empty volumes.
pos_weight = 1.0 / tools_dataset_processing.compute_ratio_of_filled_vs_empty_voxels(
                dataset_train_container,
                voxelized_input_width,
                voxelized_input_height,
                voxelized_input_depth)
pos_weight = 10 # 10 works well for starting, 23 does not work well
print("Ratio of empty to full voxels (over all dataset): {0}".format(pos_weight))


# Generate a folder that will contain all data generated in this run.
print("Generating a folder for storing generated data: {0}".format(execution_name))
if not os.path.exists(execution_name):
    os.makedirs(execution_name)
print("Done.")

# Start the log
timenow = time.strftime("%Y/%m/%d %H:%M:%S", time.gmtime(time.time()))
#training_log_filename = execution_name + "/training_log " + str(int(time.time())) + ".txt"
training_log_filename = execution_name + "/training_log.txt"
training_log_file = open(training_log_filename, "w")
training_log_file.write("# " + timenow + "\n")
training_log_file.write("#Epoch \t Train loss \t Test loss \t Void reconstruction loss \t Time \n")
training_log_file.close()



for epoch_index in range(0+epoch_offset, total_epochs):
#for epoch_index in range(epoch_counter.eval(sess), total_epochs): # epoch counter is loaded from the model

    # epoch_start_time = time.time()
    epoch_train_loss = 0.0
    epoch_test_loss = 0.0

    # Compute train loss
    for batch_index in range(total_train_batches):
        percentage = batch_index / total_train_batches
        timenow = time.strftime("%Y/%m/%d %H:%M:%S", time.gmtime(time.time()))
        print("Epoch {0}: {1}% (train) : Batch {2} / {3} \t {4} \t".format( \
            epoch_index, int(percentage*100), batch_index, total_train_batches, timenow), end='')
        batch_x_train = iterator_train.next()
        # print("Batch shape:")
        # print(batch_x_train.shape)
        # TODO update here the value of the pos_weight
        feed_dict = {input_placeholder: batch_x_train, reconstruction_loss_pos_weight_placeholder: pos_weight}
        # Train and compute the loss for the given batch of training samples
        _, l = sess.run([training_operation, total_loss], feed_dict=feed_dict)
        print("Batch loss: {0}".format(l))
        # The normalisation helps make the epoch_train_loss comparable to the batch_loss
        epoch_train_loss += l * len(batch_x_train) # this is the correct version
    #
    epoch_train_loss /= len(dataset_train_container)
    print('Epoch {0} train loss: {1}\n'.format(epoch_index, epoch_train_loss))

    # Compute test loss
    for batch_index in range(total_test_batches):
        percentage = batch_index / total_test_batches
        timenow = time.strftime("%Y/%m/%d %H:%M:%S", time.gmtime(time.time()))
        print("Epoch {0}: {1}% (test) : Batch {2} / {3} \t {4} \t".format( \
            epoch_index, int(percentage*100), batch_index, total_test_batches, timenow), end='')
        batch_x_test = iterator_test.next()
        # TODO update here the value of the pos_weight
        feed_dict = {input_placeholder: batch_x_test, reconstruction_loss_pos_weight_placeholder: pos_weight}
        # Compute the loss for the given batch of test samples
        l = sess.run(fetches=total_loss, feed_dict=feed_dict)
        print("Batch loss: {0}".format(l))
        # print("l = {0}".format(l))
        # print("len of batch: {0}".format(len(batch_x_test)))
        epoch_test_loss += l * len(batch_x_test) # this is the correct version
    epoch_test_loss /= len(dataset_test_container)
    print('Epoch {0} test loss: {1}\n'.format(epoch_index, epoch_test_loss))

    # Reconstruct the void volume and compute the loss for this reconstruction
    batch_void_volume = dataset_void_container # one element contained inside
    feed_dict = {input_placeholder: batch_void_volume, reconstruction_loss_pos_weight_placeholder: pos_weight}
    # Compute the loss for the void sample
    void_loss = sess.run(fetches=total_loss, feed_dict=feed_dict)
    print("Epoch {0} Void batch loss: {1}\n".format(epoch_index, void_loss))
    # print("len of batch: {0}".format(len(batch_void_volume)))

    # Log the training statistics:
    # 1. Epoch
    # 2. Training dataset loss
    # 3. Test dataset loss
    # 4. Void reconstruction loss
    training_log_file = open(training_log_filename, "a")
    # To append data to an existing file use the command open("Filename", "a")
    # with open("Output.txt", "w") as text_file:
    #     text_file.write("Purchase Amount: {0}".format(TotalAmount))
    timenow = time.strftime("%Y/%m/%d %H:%M:%S", time.gmtime(time.time()))
    training_log_file.write("{0} \t {1} \t {2} \t {3} \t {4}\n".format( epoch_index, epoch_train_loss, epoch_test_loss, void_loss, timenow))
    training_log_file.close()


    # TODO Increase the epoch counter

        # if (epoch_index % inv_frequency_of_hallucinations_generation == 0):
        #     # Generate the required amount of noise for the reconstruction quality check
        #     # use the learned gaussian distributions here, not a basic one.
        #     computed_z_mean_batch_avg, computed_z_log_var_batch_avg = \
        #         sess.run([z_mean_avg, z_log_var_avg], feed_dict=feed_dict)
        #
        #     # Append the means and log_vars of the batch to the global list
        #     z_means_list.append(computed_z_mean_batch_avg)
        #     # (dataset_size / batch_size) x latent_dim
        #     z_log_vars_list.append(computed_z_log_var_batch_avg)

    # epoch_duration = time.time() - epoch_start_time
    # print("Epoch processing duration:")
    # timespent = time.strftime("%H:%M:%S", time.gmtime(epoch_duration))
    # print(timespent)

    # Evaluate the reconstruction of images
    # reconstruction from real images
    #
    # The number of reconstructed samples is equal to
    # the number of samples in the last batch (which may be incomplete, of size < batch_size):
    # (dataset - floor(dataset / batch_size) * batch_size)
    if (epoch_index % inv_frequency_of_reconstruction_evaluations == 0):
        #
        # Generate the parent folder, that will hold reconstructions
        # for the (1) training dataset, (2) test dataset, (3) void volume
        #
        # Decide the name of the folder
        directory = execution_name + "/reconstructions_" + str(int(time.time())) + "iter_{0}".format(epoch_index)
        # Create the folder
        if not os.path.exists(directory):
            os.makedirs(directory)
            # Decide the names of sub-folders
            directory_train_path = directory + "/train"
            directory_test_path = directory + "/test"
            directory_void_path = directory + "/void"
            # Create the sub-folders
            os.makedirs(directory_train_path)
            os.makedirs(directory_test_path)
            os.makedirs(directory_void_path)

            # 1/3 Save the training set reconstructions
            feed_dict = {input_placeholder: batch_x_train}
            # Generate the reconstructions
            reconstructed_train_samples, original_train_samples = sess.run(
                    [reconstructed_samples_tensor, input_placeholder],
                    feed_dict = feed_dict
                    )
        	# Save the images for checks
            # save_reconstructed_samples(
            tools_reconstructions.save_reconstructed_samples_non_empty(
                        directory_train_path,
                        reconstructed_train_samples,
                        original_train_samples,
                        epoch_index)

            # 2/3 Save the test set reconstructions
            feed_dict = {input_placeholder: batch_x_test}
            #
            reconstructed_test_samples, original_test_samples = sess.run(
                    [reconstructed_samples_tensor, input_placeholder],
                    feed_dict = feed_dict
                    )
        	# Save the images for checks
            # save_reconstructed_samples(
            tools_reconstructions.save_reconstructed_samples_non_empty(
                        directory_test_path,
                        reconstructed_test_samples,
                        original_test_samples,
                        epoch_index)

            # 3/3 Save the void reconstruction
            feed_dict = {input_placeholder: batch_void_volume}
            #
            reconstructed_void_sample, original_void_sample = sess.run(
                    [reconstructed_samples_tensor, input_placeholder],
                    feed_dict = feed_dict
                    )
        	# Save the images for checks
            tools_reconstructions.save_reconstructed_samples_pairs(
                        directory_void_path,
                        reconstructed_void_sample,
                        original_void_sample,
                        epoch_index)



    # # Evaluate the hallucination of images from noise
    # # reconstruction from noise
    # if (epoch_index % inv_frequency_of_hallucinations_generation == 0):
    #     z_means_list = list()
    #     z_log_vars_list = list()
    #
    #     # Compute the average z_means and z_log_vars for the dataset
    #     # size: latent_dim x 1 for each
    #     computed_z_means_dataset_avg, \
    #     computed_z_log_vars_dataset_avg = \
    #         compute_average_Gaussian_distribution_values_over_dataset(
    #             z_means_list,
    #             z_log_vars_list
    #             )
    #
    #     # Sampler: Normal (gaussian) random distribution
    #     # z = z_mean + tf.exp(z_log_var / 2) * eps
    #
    #     noise = np.random.normal(
    #                 loc = computed_z_means_dataset_avg,
    #                 scale = np.exp(computed_z_log_vars_dataset_avg / 2),
    #                 #loc= computed_z_mean, # center
    #                 #scale= np.exp(computed_z_log_var / 2), # use the variance here (DONE)
    #                 size = (total_hallucinations_generated_per_check, latent_dim) # 1
    #                 #size = [total_hallucinations_generated_per_check, latent_dim]
    #                 )
    #     #noise = np.tensordot(noise, computed_z_log_var, AXES)
    #
    #     feed_dict = {hallucination_noise_input_placeholder: noise.astype(np.float32)}
    #     # Evaluate the node (for the given input, return the computed output of this node)
    #     hallucinated_images = reconstructed_hallucinations.eval(
    #                                 session=sess,
    #                                 feed_dict=feed_dict)
    #
    #     save_hallucinated_images(hallucinated_images, epoch_index)

    # Save the model
    if (args.save_params):
        if (epoch_index % inv_frequency_of_parameters_saving == 0):
            # 1. Rename the previous backup, if it exists (overwrite any ancient ones)

            # Save only if loss is lower than the previous model
            # if (epoch_train_loss < min_train_loss):

            # Update the min_train_loss
            min_train_loss = epoch_train_loss

            model_name =    'model_' + \
                            short_dataset_name + \
                            '__t_' + str(int(time.time())) + \
                            '__epoch_' + str(epoch_index) + \
                            '__train_' + str(int(round(epoch_train_loss))) + \
                            '__test_' + str(int(round(epoch_test_loss))) + \
                            '__void_' + str(int(round(void_loss)))
            directory = models_path + model_name
            model_saving_path = directory + '/model_feature_learning_3D.ckpt'

            if not os.path.exists(directory):
                os.makedirs(directory)

            # if (os.path.exists(model_saving_path)):
            #     os.rename(model_saving_path, model_saving_path_backup)

            # 2. Save the variables to disk.
            save_path = saver.save(sess, model_saving_path)
            print("(epoch {0}) Model saved in file: {1}".format(epoch_index, save_path))
            # 3. Once saved, remove the old backup
            # os.remove(model_saving_path_backup)
            # else:

# After running the optimization, save and display the values of the
# learned parameters of the Gaussian, from which we sample the z's for the latent layer.
# These are the parameters define the Gaussians from which the latent variables' values are sampled
