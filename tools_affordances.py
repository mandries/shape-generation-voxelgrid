# TODO Check if this function works
# What type of input does it require? Specify this.
def compute_effect(
        #latent_representation_frame_1,
        frame_start_z_means,
        frame_start_z_log_vars,
        #latent_representation_frame_2
        frame_end_z_means,
        frame_end_z_log_vars
        ):
    # Compute the differences between the two,
    #   and consider this as the observed effect
    effect_z_means = tf.sub(frame_end_z_means, frame_start_z_means)
    effect_z_log_vars = tf.sub(frame_end_z_log_vars, frame_start_z_log_vars)
    return effect_z_means, effect_z_log_vars
