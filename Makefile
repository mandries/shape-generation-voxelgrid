# t is the parameter defining the target machine
username=mihai
target=$($t)

launch_training_on:
	# Connect to the remote machine
	ssh -X $(username)@$(target)
	# Launch the script, avoiding its death on connection loss
	nohup python vae_code_3d.py -r -s &
	# PID: ___ (1 Feb 2018)
	# Peek on the process output
	tail -f nohup.out

train:
	# python vae_code_3d.py -s
	# -r : read and load the existing model
	# -s : save the model every N steps (N is hard-coded in the program)
	python vae_code_3d.py -r -s

quick_test:
	python vae_code_3d.py -q

clean:
	rm *.pyc

register_ssh:
	ssh-copy-id $(username)@$(target)
	#
	#ssh-copy-id username@remote_host
	# cat ~/.ssh/id_rsa.pub | ssh username@remote_host "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"

free_gpu:
	kill -9 $(nvidia-smi | sed -n 's/|\s*[0-9]*\s*\([0-9]*\)\s*.*/\1/p' | sort | uniq | sed '/^$/d')

	# sed -n 's/|\s*[0-9]*\s*\([0-9]*\)\s*.*/\1/p'  will find PID,
	# sort | uniq will exract unique,
	# sed '/^$/d' remove blank lines.

generate_ssh_keys:
	ssh-keygen -t rsa

info_machine_stats_CPU:
	#CPU Type:
	cat /proc/cpuinfo  | grep 'name'| uniq
	# CPU Cores:
	cat /proc/cpuinfo  | grep process| wc -l

	#
	# lspci | grep ' VGA ' | cut -d" " -f 1 | xargs -i lspci -v -s {}
	# lscpu

info_machine_stats_GPU:
	# GPU:
	cat /proc/driver/nvidia/gpus/*/information

help:
	# PID info:
	# ps -u -p PID
	#
