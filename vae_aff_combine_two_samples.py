# This Variational AutoEncoder
# combines latent descriptions of given objects into
# a single latent description of a new object

from __future__ import division, print_function, absolute_import
#~ from array import *
#~ from random import shuffle

import argparse # for parsing arguments
import sys # for counting the number of arguments

import numpy as np
import tensorflow as tf
# import time # for pausing the program
import time # to know how long it takes for the code to run
# Libraries for working with images
import os # to walk through directories, to rename files
import binvox_rw # to manipulate binvox files
import pcl_tools
import tools_dataset_processing
import tools_combination
import model_vae_3d

# The welcome message
print("Start: AutoEncoder for Combining latent vector representations of objects. Requires valid path for loading the VAE model (defined in code). Program is running")

# Introduce the argument parser (useful if you want to load pre-learned parameters, or start learning from zero)
parser = argparse.ArgumentParser()
# parser.add_argument("-r", "--restore_params",
#                 help="Restore the parameters in the graph from the given input file.",
#                 action="store_true")
args = parser.parse_args()

# if (args.restore_params):
#     print ("Restore params enabled")
# else:
#     print ("Restore params disabled: learning from zero")

saver = 0


# sys.exit(0)

################################################################################
################################## Constants ###################################
################################################################################

##### Constants #####
dataset_container = []  # The array containing the (3d) image data
dataset_image_count = 0 # The counter used for counting the number of images in the dataset

# Parameters
learning_rate = 0.001
total_epochs = 10000
batch_size = 1

#inv_frequency_of_hallucinations_generation = 100 # check reconstruction quality every N batches (from noise)
#total_hallucinations_generated_per_check = 20
inv_frequency_of_reconstruction_evaluations = 2 # check reconstruction quality every N batches (from real images)
inv_frequency_of_parameters_saving = 1 # save after every epoch

### Dataset data ###
# Description of the input
voxelized_input_width = 64
voxelized_input_height = 64
voxelized_input_depth = 64
nn_img_input_channels = 1 # 4 # (3 color + 1 depth)

input_file_format_suffix = '.binvox' # point cloud (is not voxelized representation)

# Model saving
model_saving_path = '/home/mihai/Documents/VAE_code/learned_model_vae_3d/model_feature_learning_3D'
model_saving_path_backup = model_saving_path + '_auto_backup.ckpt'
model_saving_path = model_saving_path + '.ckpt'

################################################################################
################################# Functions ####################################
################################################################################

################################################################################
############################# Building the graph ###############################
################################################################################


def build_graph():
    # Building the encoder
    input_placeholder_1 = tf.placeholder(
                        tf.float32,
                        shape=[
                                None, # the size of the batch
                                voxelized_input_width,
                                voxelized_input_height,
                                voxelized_input_depth
                              ]
                              # nn_img_input_channels
                        )

    input_placeholder_2 = tf.placeholder(
                        tf.float32,
                        shape=[
                                None, # the size of the batch
                                voxelized_input_width,
                                voxelized_input_height,
                                voxelized_input_depth
                              ]
                              # nn_img_input_channels
                        )

    # Building the decoder
    target_placeholder = input_placeholder_1 # the thing we are trying to reconstruct

    # Capture the output of the encoder
    with tf.variable_scope("model"):
        z_mean_1, z_log_var_1 = model_vae_3d.encoder_net(input_placeholder_1)
    with tf.variable_scope("model", reuse=True):
        z_mean_2, z_log_var_2 = model_vae_3d.encoder_net(input_placeholder_2)

    with tf.variable_scope("model"):
        combined_mean, combined_log_var = tools_combination.combine_object_descriptions_tf(
            z_mean_1, z_log_var_1, \
            z_mean_2, z_log_var_2)

        # print("shape of z_mean (decoder output): {}".format(z_mean.shape))
        # print("shape of z_log_var (decoder output): {}".format(z_log_var.shape))
        # Sampler: Normal (gaussian) random distribution
        eps = tf.random_normal(
                    # or z_log_var_2 (we only care about the shape, which is the same)
                    tf.shape(z_log_var_1),
                    dtype=tf.float32,
                    mean=0.,
                    stddev=1.0,
                    name='epsilon')
        print("shape of eps (encoder output): {}".format(eps.shape))
        z_combined = combined_mean + tf.exp(combined_log_var / 2) * eps
        print("shape of z (decoder input): {}".format(z_combined.shape))

        z_1 = z_mean_1 + tf.exp(z_log_var_1 / 2) * eps
        z_2 = z_mean_2 + tf.exp(z_log_var_2 / 2) * eps

        # the actual reconstruction
        #with tf.variable_scope("model", reuse=True):
        output_voxelized_volume_combined = model_vae_3d.decoder_net(z_combined)

    # combined_samples_tensor = tf.nn.sigmoid(output_voxelized_volume)

    with tf.variable_scope("model", reuse=True):
        output_voxelized_volume_recon1 = model_vae_3d.decoder_net(z_1)
    with tf.variable_scope("model", reuse=True):
        output_voxelized_volume_recon2 = model_vae_3d.decoder_net(z_2)

    # Initialize the variables (i.e. assign their default value)
    # init = tf.global_variables_initializer()
    # The model (graph) saver (needs to be instanciated after the graph is built)
    # if ((args.save_params) or (args.restore_params)):
    saver = tf.train.Saver()

    return  \
        input_placeholder_1, \
        input_placeholder_2, \
        output_voxelized_volume_recon1, \
        output_voxelized_volume_recon2, \
        output_voxelized_volume_combined, \
        saver

################################################################################
############################## Running the graph ###############################
################################################################################


dataset_paths = ['/media/Data/datasets/ModelNet10_binvox/bathtub/train/', \
'/media/Data/datasets/ModelNet10_binvox/bathtub/test/', \
'/media/Data/datasets/ModelNet10_binvox/bed/train/', \
'/media/Data/datasets/ModelNet10_binvox/bed/test/']

print("Processing the dataset files...")
# Go through the dataset
for index in range(len(dataset_paths)):
    print("Processing the dataset: " + dataset_paths[index])
    # get_dataset_files_from_path(
    dataset_image_count = tools_dataset_processing.load_dataset_binvox_64(
        dataset_paths[index], # vislab_dataset_path,
    	dataset_container,
        input_file_format_suffix,
    	dataset_image_count)


# Do this before you iterate (because we need to feed in an array to tensorflow)
dataset_container = np.asarray(dataset_container)
# Checks
print("Samples in dataset (de facto): {0}".format(len(dataset_container)))
print("Samples in dataset (processed): {0}".format(dataset_image_count))

# Declare the iterator
iterator = tools_dataset_processing.data_iterator(dataset_container, batch_size)

#~ # Start a new TF session
sess = tf.Session()

# if __name__ = main():
input_placeholder_1, \
input_placeholder_2, \
output_voxelized_volume_recon1, \
output_voxelized_volume_recon2, \
output_voxelized_volume_combined, \
saver = build_graph()

# Run the initializer
print("Restoring model from file... ")
saver.restore(sess, model_saving_path)
print("done.")

batch_x_1 = iterator.next()
batch_x_2 = iterator.next()
print("Extracted batches 1 and 2")
feed_dict = {   input_placeholder_1: batch_x_1,
                input_placeholder_2: batch_x_2}
print("Prepared the feed_dict")


# voxelized outputs
input_sample_1, \
input_sample_2, \
output_recon1_sample_1, \
output_recon1_sample_2, \
output_recon_combined = sess.run(
        [
            input_placeholder_1,
            input_placeholder_2,
            output_voxelized_volume_recon1,
            output_voxelized_volume_recon2,
            output_voxelized_volume_combined,
        ],
        feed_dict = feed_dict
        )
print("Generated the combination of 2 samples.")

# print("Shapes of i1, i2, o1, o2, comb\n")
# (1x 64x64x64)
# print(input_sample_1.shape)
# print(input_sample_2.shape)
# print(output_recon1_sample_1.shape)
# print(output_recon1_sample_2.shape)
# print(output_recon_combined.shape)

# Save the models for checks
tools_combination.save_combined_samples(
            input_sample_1[0],
            input_sample_2[0], # or .squeeze
            output_recon1_sample_1[0],
            output_recon1_sample_2[0],
            output_recon_combined[0])
print("Saved the combination files.")
