import numpy as np
import math
import binvox_rw
import time

# Function to parse a point cloud file
# returns a pointcloud
def parse_pcd_file(filename):
    print("\nParsing file {0}".format(filename))
    # Open the input file
    inputfile = open(filename, "r")

    inputfile.next() # Skip first comment line
    # The second line is optional: the version number
    lines_to_skip = 7
    line = inputfile.next()
    tokens = line.split(" ")
    if (tokens[0] == "VERSION"):
        print("Option used: Version is specified")
    else:
        # print("No version specified")
        lines_to_skip -= 1
    # Skip the first N lines (the header)

    # for i in range(11): inputfile.next() # skip first lines
    for i in range(lines_to_skip): inputfile.next() # skip first lines

    # Line 10 contains the number of points in the point cloud
    line = inputfile.next()
    tokens = line.split(" ")
    total_points = int(tokens[1])
    print("Total points given by header: {0}".format(total_points))
    # Line containing: Data = ascii
    line = inputfile.next()

    # Containter for the point cloud (a list of points)
    pointcloud = np.zeros((total_points, 4)) # type of input: x y z rgba

    total_read_points = 0
    # For each line (with point data) in the file do:
    for line in inputfile:
        tokens = line.split(" ") # Tokenize
        # Parse:
        for index in range(0,3): # from 0 to 2 (x y z)
            pointcloud[total_read_points, index] = float(tokens[index])
            #print(pointcloud[total_read_points, index])
        # TODO parse properly the rgba field (32 bytes, 8*3 for RGB, 8 for Alpha)
        pointcloud[total_read_points, 3] = 1 # occupied voxel

        # print(len(tokens))
        total_read_points += 1

    print ("Total points: {0}".format(total_read_points))
    inputfile.close()

    return pointcloud

# Function to parse a CAD file of format OFF
# (http://segeval.cs.princeton.edu/public/off_format.html)
# returns a pointcloud
def parse_OFF_file_using_only_vertices(filename):
    print("\nParsing file {0}".format(filename))
    # Open the input file
    inputfile = open(filename, "r")
    # Skip the first N lines (the header)
    # for i in range(11): inputfile.next() # skip first four lines
    # for i in range(9): inputfile.next() # skip first four lines
    inputfile.next() # skip line with "OFF" header

    # Line 10 contains the number of points in the point cloud
    line = inputfile.next()
    tokens = line.split(" ")
    total_points = int(tokens[0])
    print("Total points given by header: {0}".format(total_points))

    # Containter for the point cloud (a list of points)
    pointcloud = np.zeros((total_points, 3)) # type of input: x y z # rgba

    total_read_points = 0
    # For each line (with point data) in the file do:
    for pointIndex in range(total_points):
    # for line in inputfile:
        line = inputfile.next()
        tokens = line.split(" ") # Tokenize
        # Parse:
        for coordIndex in range(0,3): # from 0 to 2 (x y z)
            pointcloud[pointIndex, coordIndex] = float(tokens[coordIndex])
            #print(pointcloud[total_read_points, index])
        # TODO parse properly the rgba field (32 bytes, 8*3 for RGB, 8 for Alpha)
        # pointcloud[pointIndex, 3] = 1 # occupied voxel

    # print ("Total points: {0}".format(total_read_points))
    inputfile.close()

    return pointcloud

# Parses a file of type BINVOX
# Returns a voxel grid, generated using the binvox_rw.py package
def parse_BINVOX_file_into_voxel_grid(filename):
    filereader = open(filename, 'rb')
    binvox_model = binvox_rw.read_as_3d_array(filereader)
    voxelgrid = binvox_model.data
    return voxelgrid

# Returns the anti-voxelgrid (with all voxel values flipped)
def getVoxelgridComplement(voxelgrid):

    voxelgrid_complement = (-1 * voxelgrid) + 1
    # x_range = len(voxelgrid)
    # y_range = len(voxelgrid[0])
    # z_range = len(voxelgrid[0][0])
    # # print(x_range, y_range, z_range)
    # # Generate the new voxel grid that will store the result
    # voxelgrid_complement = np.zeros((
    #             x_range,
    #             y_range,
    #             z_range))
    #
    # for x in range(x_range):
    #     for y in range(y_range):
    #         for z in range(z_range):
    #             # print(x,y,z)
    #             # Set the voxelgrid_complement to be the opposite of the voxelgrid
    #             # i.e. (0 -> 1) and (1 -> 0)
    #             voxelgrid_complement[x][y][z] = (voxelgrid[x][y][z] + 1) % 2

    # Return the result
    return voxelgrid_complement


# Assumption: pointcloud not empty (at least one point contained)
def get_pointcloud_boundingbox(pointcloud):

    # print("Computing the bounding box...")

    # if length(pointcloud) = 0
    # then return 0,0 0,0 0,0

    # Assign values equal to the first point
    x_min = pointcloud[0][0]
    x_max = pointcloud[0][0]
    y_min = pointcloud[0][1]
    y_max = pointcloud[0][1]
    z_min = pointcloud[0][2]
    z_max = pointcloud[0][2]

    for point in pointcloud:
        if (point[0] < x_min):
            x_min = point[0]
        if (point[0] > x_max):
            x_max = point[0]
        if (point[1] < y_min):
            y_min = point[1]
        if (point[1] > y_max):
            y_max = point[1]
        if (point[2] < z_min):
            z_min = point[2]
        if (point[2] > z_max):
            z_max = point[2]

    bounding_box = ((x_min, x_max), (y_min, y_max), (z_min, z_max))
    # print(bounding_box)

    return bounding_box

######################################################
##################### CONVERTERS #####################
######################################################

# Voxelize a pointcloud, putting it in the given bounds
# Preserves the proportions of the object
def voxelize_pointcloud(
        pointcloud,
        pointcloud_width,
        pointcloud_height,
        pointcloud_depth
        ):

    voxelized_pointcloud = np.zeros((
                pointcloud_width,
                pointcloud_height,
                pointcloud_depth))

    bbox = get_pointcloud_boundingbox(pointcloud)
    #print(bbox[0][0])
    x_range = bbox[0][1] - bbox[0][0] # max - min
    y_range = bbox[1][1] - bbox[1][0]
    z_range = bbox[2][1] - bbox[2][0]

    # print("X range: {0}".format(x_range))
    # print("Y range: {0}".format(y_range))
    # print("Z range: {0}".format(z_range))

    # only the max value will populate the edge cell
    x_discretization_step = x_range / (pointcloud_width -1)
    y_discretization_step = y_range / (pointcloud_height -1)
    z_discretization_step = z_range / (pointcloud_depth -1)

    discretization_step = max(  x_discretization_step,
                                y_discretization_step,
                                z_discretization_step)
    # print("Discretization step: {0}".format(discretization_step))

    for point in pointcloud:
        # Squashed (afine transformation)
        # x = int(math.floor((point[0] - bbox[0][0]) / x_discretization_step))
        # y = int(math.floor((point[1] - bbox[1][0]) / y_discretization_step))
        # z = int(math.floor((point[2] - bbox[2][0]) / z_discretization_step))
        #
        # Keep proportions:
        # [0..max] / (max / width) = max * width / max = width
        x = int(math.floor((point[0] - bbox[0][0]) / discretization_step))
        y = int(math.floor((point[1] - bbox[1][0]) / discretization_step))
        z = int(math.floor((point[2] - bbox[2][0]) / discretization_step))
        # print(x,y,z)
        # print("x,y,z: {0},{1},{2}".format(x,y,z))
        voxelized_pointcloud[x][y][z] = 1

    return voxelized_pointcloud

def voxelized_to_pointcloud(voxel_representation):

    pointcloud = list()

    x_range = len(voxel_representation)
    y_range = len(voxel_representation[0])
    z_range = len(voxel_representation[0][0])
    for x in range(x_range):
        for y in range(y_range):
            for z in range(z_range):
                # print(x,y,z)
                if (voxel_representation[x][y][z] == 1):
                    # point = str(x) + "  " + str(y) + "    " + str(z) + "   " + "9927216"
                    point = np.array([x,y,z,9927216])
                    # print(point)
                    pointcloud.append(point)

    # print("Points in pointcloud: {0}".format(len(pointcloud)))
    return pointcloud

def convert_binvox_to_pointcloud(filename):
    # "/home/mihai/datasets/3D Shapenets Princeton/ModelNet10/ModelNet10/sofa/test/" + filename + ".binvox"
    voxelgrid = parse_BINVOX_file_into_voxel_grid(filename)
    pointcloud = voxelized_to_pointcloud(voxelgrid)
    write_pointcloud_to_file(pointcloud, "./" + filename + ".pcd")
    return


########################################################
##################### FILE WRITERS #####################
########################################################


def write_pointcloud_to_file(pointcloud, filename):

    output_string = "# .PCD v0.7 - Point Cloud Data file format" + "\n" + \
             "VERSION 0.7" + "\n" + \
            "FIELDS x y z" + "\n" + \
            "SIZE 4 4 4" + "\n" + \
            "TYPE F F F" + "\n" + \
            "COUNT 1 1 1" + "\n" + \
            "WIDTH " + str(len(pointcloud)) + "\n" + \
            "HEIGHT 1" + "\n" + \
            "VIEWPOINT 0 0 0 1 0 0 0" + "\n" + \
            "POINTS " + str(len(pointcloud)) + "\n" + \
            "DATA ascii" + "\n"

    for point in pointcloud:
        # print (point[0], point[1], point[2])
        output_string += str(point[0]) + " "  + str(point[1]) + " " + str(point[2]) + "\n"

    # output_file = open("resized_pointcloud.pcd", "w")
    output_file = open(filename, "w")
    output_file.write(output_string)
    output_file.close()
    # print(output_string)

    return 0

# Function that writes a voxel grid to a binvox file
def write_voxelgrid_to_BINVOX_file(voxelgrid):

    data = voxelgrid
    dims = (64,64,64)
    translate = (0,0,0)
    scale = 1
    axis_order = 'xyz'
    voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
    # Specify the file path
    filename = "generated_model_" + str(time.time()) + ".binvox"
    filewriter = open(filename, 'w')
    # Write the binvox model to file (so that it can be seen through viewvow)
    binvox_rw.write(voxel_model, filewriter)
    return



# Test run for the "combined object descriptions"
# a = [0.5, 0.64, 0.3, 0.1, 0]
# avar = [0.15, 0.364, 0.63, 0.31, 0.11]
# b = [0.25, 0.36, 0.13, 0.41, 0.2]
# bvar = [0.525, 0.136, 0.313, 0.141, 0.52]
# print combine_object_descriptions(a, avar, b, bvar)

# filename = "bathtub_0156";
# voxelgrid = parse_BINVOX_file_into_voxel_grid("/home/mihai/datasets/3D_Shapenets_Princeton/ModelNet10/ModelNet10_binvox/bed/train/bed_0444.binvox")
# pointcloud = parse_pcd_file("/home/mihai/datasets/Washington_Uni_Dieter_Fox/rgbd-dataset/apple/apple_1/apple_1_2_9.pcd")
# voxelgrid = voxelize_pointcloud(pointcloud, 64, 64, 64)
# write_voxelgrid_to_BINVOX_file(voxelgrid)


# pointcloud = parse_OFF_file("/home/mihai/datasets/3D Shapenets Princeton/ModelNet10/ModelNet10/bathtub/test/bathtub_0107.off")
# write_pointcloud_to_file(pointcloud, "./off_converted_bathtub.pcd")
#
# pointcloud = parse_OFF_file("/home/mihai/datasets/3D Shapenets Princeton/ModelNet10/ModelNet10/bed/test/bed_0516.off")
# write_pointcloud_to_file(pointcloud, "./off_converted_bed.pcd")
