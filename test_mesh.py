import numpy as np
import trimesh # for converting voxel grids to meshes (to import objects into simulators)
import time # to know how long it takes for the code to run
import os # to walk through directories, to rename files

import tools_sdf_generator
# import tools_combination
import binvox_rw # to manipulate binvox files

def save_voxelgrid(voxelgrid, directory, filename):
    # Write as voxelgrid (binvox format)
    print("Voxelgrid shape for saving: ")
    print(voxelgrid.shape)
    voxelgrid = voxelgrid > 0.5 # thresholding
    data = voxelgrid
    dims = (64,64,64) # DEFAULT voxelgrid size here (not true size, which may vary)
    translate = (0,0,0) # only used for generating the BINVOX file, has no effect on the mesh file.
    scale = 1
    axis_order = 'xyz'
    voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
    # Specify the file path
    filename = os.path.join(directory, filename + ".binvox")
    filewriter = open(filename, 'w')
    # Write the binvox model to file (so that it can be seen through viewvow)
    binvox_rw.write(voxel_model, filewriter)

if __name__ == "__main__":

    # Generate a folder to store the images
    print("Generating a folder to save the object_class_essences and combinations")
    combinations_path = '/media/Data/mihai/combinations/'
    directory = combinations_path + "test_mesh" + str(time.time())
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Create empty voxel grid
    voxelgrid = np.full((64,64,64), False, dtype=bool)

    # fill it with something
    for i in range(16,48,1):
        for j in range (28,36,1):
            for k in range(16, 48, 1):
              voxelgrid[k][i][j] = True

    obj_combined_mesh = trimesh.voxel.matrix_to_marching_cubes(
        # matrix=decoded_combined_threshed[0],
        matrix=voxelgrid,
        pitch=1.0,
        origin=(0,0,0))
        # (32,32,32)) # center of the cube
        #(0,0,0))
        # np.zeros(3)

    print("Merging vertices closer than a pre-set constant...")
    obj_combined_mesh.merge_vertices()
    print("Removing duplicate faces...")
    obj_combined_mesh.remove_duplicate_faces()

    # percentage = str(top_percentages[index]).replace(".","")
    obj_combined_mesh.apply_scale(scaling=1.0)
    # obj_combined_mesh.export(directory + "/combined_mesh_short.stl")

    print("Making the mesh watertight...")
    trimesh.repair.fill_holes(obj_combined_mesh)
    
    # is_watertight = False
    # while (is_watertight == False):
    #     print("repairing mesh...")
    #     is_watertight = trimesh.repair.fill_holes(obj_combined_mesh)
    
    print("Fixing inversion and winding...")
    trimesh.repair.fix_inversion(obj_combined_mesh)
    trimesh.repair.fix_winding(obj_combined_mesh)

    print("Computing the center of mass: ")
    center_of_mass = obj_combined_mesh.center_mass
    print(center_of_mass)

    print("Computing moments of inertia: ")
    moments_of_inertia = obj_combined_mesh.moment_inertia
    print(moments_of_inertia)  # inertia tensor in meshlab

    # connected_components = obj_combined_mesh.split()
    # TODO find how to identify the largest connected component
    # print("Connected components:")
    # print(connected_components)
    # print(len(connected_components))

    # Find connected components
    #edges = trimesh.graph.
    #trimesh.graph.connected_components(
    #	edges=, )

    # trimesh.io.export.export_mesh(
    #     mesh=obj_combined_mesh,
    #     file_obj=directory + "/combined_mesh.ply",
    #     file_type="ply"
    # )

    
    print("Generating the STL mesh file")
    trimesh.exchange.export.export_mesh(
        mesh=obj_combined_mesh,
        file_obj=directory + "/combined_mesh.stl",
        file_type="stl"
    )

    print("Generating the SDF file...")
    object_model_name = "comb"
    #mass=1.00
    
    mass = 0.00
    for i in range(16,48,1):
        for j in range (28,36,1):
            for k in range(16, 48, 1):
              if (voxelgrid[k][i][j] == True):
                  mass += 1.00
    print("Calculated mass (equal to number of filled voxels: {0}".format(mass))
    
    tools_sdf_generator.generate_model_sdf(
        directory=directory,
        object_name=object_model_name,
        center_of_mass=center_of_mass,
        inertia_tensor=moments_of_inertia,
        mass=mass,
        model_stl_path=directory + "/combined_mesh.stl")

    print("Generating BINVOX file...")
    save_voxelgrid(voxelgrid, directory, object_model_name)

    spawning_script_for_gazebo = '#!/bin/bash \n'
    spawning_script_for_gazebo += 'rosrun gazebo_ros spawn_model -sdf -file ' + directory + '/' + object_model_name + '.sdf -model ' + object_model_name + ' \n'

    # Create the spawning script for gazebo
    f = open(directory + "/spawning.sh", "w")
    # Write the content to file
    f.write(spawning_script_for_gazebo)
    # Close the file
    f.close()