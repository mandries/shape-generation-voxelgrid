from __future__ import absolute_import, unicode_literals, print_function, division
import tensorflow as tf
import numpy as np
import tools_dataset_processing as tdp

conv3d_3cube_kernel_dim_step = 2 ** 3
total_activations = 0
batch_size = 32
compute_number_of_activation_values = False

# This file contains the functions:
# - encoder
# - decoder
# - discriminator
# - get_iterators

# TODO :
# 1. Why "encoder" and "decoder" are not inside the class "VAE" ?
#    Because they are reused
# 2. Why "discriminator" is not inside the class "GAN" ?
#    Because it is reused

def add_tensor_to_total_activations(input_tensor):
  print("-- Activations tensor shape received:")
  print(input_tensor.shape)
  tensor_shape = input_tensor.shape
  tensor_activations = tensor_shape[1] # skip the ? for batch size
  for i in range(2,len(tensor_shape)):
    tensor_activations *= tensor_shape[i]
  global total_activations
  total_activations += tensor_activations
  print(" = {} activations".format(tensor_activations))
  #'{:,}'.format(1234567890.001).replace(',', ' ')

def print_total_activations():
  print("\n\n-------\nTotal activations:")
  print(total_activations * batch_size)


def build_encoder(input_placeholder,
                  latent_dim_NOT_USED,
                  graph,
                  reuse,
                  verbose,
                  is_training_tf,
                  # Used for Batch_normalisation and Dropout, (and maybe other things)
                  # which change their behaviour depending on whether
                  # the tensorflow is training or not
                  # i.e. Do we use the model for Test or Train now?
                  is_training_py):

  verboseprint = print if verbose else lambda *a, **k: None
  hist_summary = tf.summary.histogram if is_training_py else lambda *a, **k: None
  summaries_list = list()

  def batch_norm_and_relu(
          conv, # the current layer values
          layer_num): # layer number
    # Debugging info
    verboseprint('\nEncoder shape after conv{}:'.format(layer_num), conv.shape)
    # Tensorboard info
    summaries_list.append(hist_summary("enc_preact{}".format(layer_num), conv))
    # Batch normalisation
    conv = tf.layers.batch_normalization(conv,
        #training: Either a Python boolean, or a TensorFlow boolean scalar tensor (e.g. a placeholder).
        # Whether to return the output
        # in training mode (normalized with statistics of the current batch) or
        # in inference mode (normalized with moving statistics).
        # NOTE: make sure to set this parameter correctly, or else your training/inference will not work properly.
                                         training=is_training_tf,
                                         name='B_N{}'.format(layer_num))
    # Apply ReLu
    conv = tf.nn.relu(conv)
    return conv

  verboseprint('\nEncoder input shape:', input_placeholder.shape)

  with graph.as_default():
    with tf.variable_scope(name_or_scope='encoder', reuse=reuse):
      x = tf.expand_dims(input_placeholder, -1)  # Now it becomes [Batch_size x (1)]
      verboseprint('\nEncoder input shape after dim expansion:', x.shape)

      # Convolution Layer 1
      # input:    (64 x 64 x 64 x 1) = (2^6)^3 x 2^0 = 2^18
      # output:   (32 x 32 x 32 x 8) = (2^5)^3 x 2^3 = 2^18
      # Convolution Layer 2
      # input:    (32 x 32 x 32 x 8) = (2^5)^3 x 2^3 = 2^18
      # output:   (16 x 16 x 16 x 64) = (2^4)^3 x 2^6 = 2^18

      if (compute_number_of_activation_values):
        add_tensor_to_total_activations(x)

      # TODO Add here code for counting filled voxels around each voxel
      # x =
      # TODO Then do the inverse in the last layer of the decoder.


      # CONV 1:
      # (64,64,64,1) -> (32,32,32,8)
      # CONV 2:
      # (32,32,32,8) -> (16,16,16,64)
      # CONV 3:
      # (16,16,16,64) -> (8,8,8,8)
      feature_layers = [8, 32, 32, 32]
      strides_layers = [2, 2, 2, 1]
      for layer_idx in range(len(feature_layers) - 1): #(1, 3, 1):
        # TODO try and remove this tf.variable_scope, to save time instantiating variables
        # As I understand, it is here only for debugging purposes.
        # with tf.variable_scope(name_or_scope='enc_conv{}'.format(layer_idx)):
            x = tf.layers.conv3d(inputs=x,
                                    filters=feature_layers[layer_idx],
                                    kernel_size=(4, 4, 4),
                                    strides=strides_layers[layer_idx],
                                    use_bias=False,
                                    padding='same',
                                    name='conv{}'.format(layer_idx))
            if (compute_number_of_activation_values):
              add_tensor_to_total_activations(x)
            # Batch norm and relu for all convs except the last one
            if (layer_idx < len(feature_layers) -1):
              x = batch_norm_and_relu(x, layer_idx)

      # WE WILL KEEP THE LATENT LAYER AT 8x8x8 = 512

      # filters_in_previous_layer = 8 # TODO keep this updated
      # with tf.name_scope('latent_var_manipulation'):
      means_slice = tf.slice(x,
                             begin=[0, 0, 0, 0, 0],
                             size=[-1, 8, 8, 8, feature_layers[-1]//2], #filters_in_previous_layer//2],
                             name="means_slice")
      log_vars_slice = tf.slice(x,
                                begin=[0, 0, 0, 0, feature_layers[-1]//2],
                                size=[-1, 8, 8, 8, feature_layers[-1]//2],
                                name="log_vars_slice")
      if (compute_number_of_activation_values):
        add_tensor_to_total_activations(means_slice)
        add_tensor_to_total_activations(log_vars_slice)

      # Computing the max over each of the slices
      z_means = tf.reduce_mean(means_slice,
                              axis=[4], #the feature layers dimension #[1, 2, 3],
                              keepdims=False)
      z_log_vars = tf.reduce_mean(log_vars_slice,
                                 axis=[4], #the feature layers dimension #[1, 2, 3],
                                 keepdims=False) # collapse the dimension over which we reduce
      z_vars = tf.exp(z_log_vars)

      # Make a 1D vector out of it.
      z_means = tf.reshape( tensor=z_means,
                            shape=[-1, 512]) # 8^3 = latent_dim
      z_vars = tf.reshape(tensor=z_vars,
                           shape=[-1, 512])  # 8^3 = latent_dim
      z_log_vars = tf.reshape(tensor=z_log_vars,
                          shape=[-1, 512])  # 8^3 = latent_dim


      summaries_list.append(hist_summary("means", z_means))
      summaries_list.append(hist_summary("variances", z_vars))

      for variable in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES):
        if variable.name.endswith('kernel:0') and variable.name.startswith('encoder'):
          summaries_list.append(hist_summary(variable.name.split('/')[-2] + '_W', variable))

    verboseprint('\nEncoder shape after splitting conv4 into means and log_vars:\n',
                 z_means.shape, '\n',
                 z_log_vars.shape)
    return z_means, z_log_vars, z_vars, summaries_list


# This functions builds the decoder network, with some skip-connections between layers
def build_decoder(
        z,
        latent_dim_NOT_USED,
        graph,
        reuse,
        verbose,
        is_training_tf,
        is_training_py):
  verboseprint = print if verbose else lambda *a, **k: None
  hist_summary = tf.summary.histogram if is_training_py else lambda *a, **k: None
  summaries_list = list()

  def batch_norm_and_relu(trans_conv, layer_num):
    # Debugging info
    verboseprint('\nDecoder shape after trans_conv{}:'.format(layer_num),
                 trans_conv.shape)
    # Tensorboard info
    summaries_list.append(
      hist_summary("dec_preact{}".format(layer_num), trans_conv))

    # Batch normalisation
    trans_conv = tf.layers.batch_normalization(trans_conv,
                                               training=is_training_tf,
                                               name='B_N{}'.format(layer_num))
    # ReLU
    trans_conv = tf.nn.relu(trans_conv)

    # Debugging info
    verboseprint('\nDecoder shape after concat{}:'.format(layer_num), trans_conv.shape)
    return trans_conv

  # Build here the Decoder network
  with graph.as_default():
    with tf.variable_scope(name_or_scope='decoder', reuse=reuse):
      x = tf.reshape(z, [-1, 8, 8, 8, 1]) # 8 = cubic root of latent_dim

      if (compute_number_of_activation_values):
        add_tensor_to_total_activations(x)

      verboseprint('\nDecoder layer shape at start, after expansion: ', x.shape)

      # 64 32 8 1
      # from 8 -> 56 = 48 steps = 4 * (13-1)
      feature_layers = [32, 16, 8, 8]
      kernels_layers = [13, 13, 13, 13]
      # Deconvolutions of the decoder
      for layer_number in range(len(kernels_layers)): #range(1,8,1):
        with tf.variable_scope(name_or_scope='dec_tconv{}'.format(layer_number)):
          x = tf.layers.conv3d_transpose(inputs=x,
                                          filters=feature_layers[layer_number],
                                          kernel_size=kernels_layers[layer_number], #1 value = same value for all 3 dims # (8, 8, 8),
                                          strides=1,
                                          use_bias=False, # to avoid the conversion error, https://github.com/tensorflow/tensorflow/issues/10520
                                          padding='valid',
                                          name='tconv_{}'.format(layer_number))
          if (compute_number_of_activation_values):
            add_tensor_to_total_activations(x)
          # Call the Batch normalisation and ReLU
          x = batch_norm_and_relu(x, layer_number)

      with tf.variable_scope(name_or_scope='dec_output'):
        x = tf.layers.conv3d_transpose(inputs=x,
                                                filters=1,
                                                # kernel_size=(5, 5, 5),
                                                kernel_size=(9, 9, 9),
                                                strides=1,
                                                use_bias=False,
                                                padding='valid',
                                                name='tconv_last')
        # Debugging info
        verboseprint('\nDecoder shape after tconv_last:', x.shape)
        print("Decoder: squeezing the feature dimension, to remove it:")
        x = tf.squeeze(x, axis=[-1])
        verboseprint('\nDecoder shape after squeeze:', x.shape)

        summaries_list.append(hist_summary("dec_preact5", x))
        for variable in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES):
          if variable.name.endswith('kernel:0') and variable.name.startswith('decoder'):
            summaries_list.append(hist_summary(variable.name.split('/')[-2] + '_W', variable))

        return x, summaries_list


# def reconstruction_loss(self):
# 	with self.graph.as_default():
# 		with tf.name_scope('reco_loss'):
# 			self.reconstruction_loss = mvd.reconstruction_loss(self.logits,
# 			                                                   self.input_placeholder)
# 			self.scalar_summary('reco_loss', self.reconstruction_loss)
# 	return


# The KL divergence loss to be used when not using softbits
# def kl_loss(self):
# 	with self.graph.as_default():
# 		with tf.name_scope('prior_loss'):
# 			self.prior_loss = mvd.kl_loss(self.z_means, self.z_log_vars)
# 			self.scalar_summary("prior_loss", self.prior_loss)
# 	return


# Build the discriminator
## Returns:
## 1. the sigmoid of the (judgment),
## 2. conv = the 3d-convolution result
## 3. summaries_list
def build_discriminator(x,
                        graph,
                        reuse,
                        verbose,
                        is_training_tf,  # TODO what is this parameter?
                        is_training_py): # TODO what is this parameter?
  # Print debug statements only if "verbose" is True
  verboseprint = print if verbose else lambda *a, **k: None
  hist_summary = tf.summary.histogram if is_training_py else lambda *a, **k: None
  summaries_list = list()

  verboseprint('\nDiscriminator input shape:', x.shape)
  features_list = [8, 16, 16, 8]
  strides = [2, 2, 2, 2]

  with graph.as_default():
    with tf.variable_scope(name_or_scope='discriminator', reuse=reuse):
      # Add one dimension for the features
      x = tf.expand_dims(x, -1)

      if (compute_number_of_activation_values):
        add_tensor_to_total_activations(x)

      verboseprint('\nDiscriminator input shape after dim expansion:', x.shape)
      for layer_idx in range(len(features_list)):
        with tf.variable_scope(name_or_scope='dis_conv{}'.format(layer_idx)):
          x = tf.layers.conv3d(inputs=x,
                                  filters=features_list[layer_idx],
                                  kernel_size=(4,4,4),
                                  strides=strides[layer_idx],
                                  use_bias=False,
                                  padding='same',
                                  name='dis_conv{}'.format(layer_idx))
          verboseprint('\nDiscriminator shape after conv{}:'.format(layer_idx),
                       x.shape)
          summaries_list.append(hist_summary("dis_preact{}".format(layer_idx), x))
          x = tf.layers.batch_normalization(x,
                                             training=is_training_tf,
                                             name='B_N{}'.format(layer_idx))
          x = tf.nn.relu(x)
          print("Discriminator: x.shape after dis_conv{}".format(layer_idx));
          print(x.shape)
          if (compute_number_of_activation_values):
            add_tensor_to_total_activations(x)

      # print("Discriminator: x.shape = ");
      # print(x.shape); #  (?, 4, 4, 4, 512)

      # with tf.variable_scope(name_or_scope='dis_conv4'):
      judgment = tf.layers.conv3d(inputs=x,
                                filters=1,
                                kernel_size=(4,4,4),
                                strides=1,
                                use_bias=False,
                                padding='valid',
                                name='dis_last_conv')
      verboseprint('\nDiscriminator shape after last conv:', judgment.shape)
      if (compute_number_of_activation_values):
        add_tensor_to_total_activations(x)
        print_total_activations()

      return tf.nn.sigmoid(judgment), x, summaries_list


def get_iterators(batch_size, quick_test=False, verbose=False):
  verboseprint = print if verbose else lambda *a, **k: None
  # Dataset root location
  dataset_root_location = '/media/Data/datasets/'
  dataset_name = 'ModelNet40_manually_aligned/lmb.informatik.uni-freiburg.de/'
      # 'ModelNet40_binvox_exact_center_4views/'
  short_dataset_name = dataset_name[:10]
  # dataset_root_location_on_plinio = '/home/mihai/Data/datasets';
  # dataset_root_location = dataset_root_location_on_plinio;

  # Input file format
  #input_file_format_suffix = '.binvox'  # (voxelized representation)
  input_file_format_suffix = '_1.binvox'  # (voxelized representation)

  dataset_train_container = []  # training data
  dataset_test_container = []  # testing data
  dataset_void_container = []  # void volume

  object_classes = [
                    # 'airplane',
                    'bathtub', 'bed', 'bench',
                    'bookshelf', 'bottle', 'bowl',
                    # 'car', # its form does not define its function (wheels are fixed)
                    'chair', 'cone', 'cup', 'curtain',
                    'desk', 'door', 'dresser', 'flower_pot',
                    'glass_box',
                    # 'guitar', # its form does not define its function
                    #'keyboard',
                    'lamp',
                    'laptop', 'mantel', 'monitor', 'night_stand',
                    # 'person', # its form does not define its function
                    # 'piano',
                    'plant',
                    # 'radio',
                    'range_hood', # should we keep it?
                    'sink', 'sofa', 'stairs',
                    'stool', 'table', 'tent', 'toilet',
                    'tv_stand', 'vase', 'wardrobe']#,
                    # 'xbox'] # its form does not define its function

  # Initialise the train/test sets for evaluating the reconstruction precision
  # The paths to object classes are added lower.
  dataset_train_paths = [dataset_root_location + dataset_name + 'void/']
  dataset_test_paths = []
  dataset_void_path = [dataset_root_location + dataset_name + 'void/']

  # Use small datasets when debugging the code
  if quick_test:
    dataset_train_paths.append(dataset_root_location + dataset_name + 'sofa/test/')
    dataset_test_paths.append(dataset_root_location + dataset_name + 'bathtub/test/')
  # Use the entire dataset when running the code nominally
  else:
    for index in range(len(object_classes)):
      # Training dataset
      dataset_train_paths.append(dataset_root_location + dataset_name + object_classes[index] + '/train/')
      # Testing dataset
      dataset_test_paths.append(dataset_root_location + dataset_name + object_classes[index] + '/test/')

  # Go through the dataset
  verboseprint("Processing the dataset (train) files...")
  # Training dataset
  for index in range(len(dataset_train_paths)):
    verboseprint("Processing the dataset (train): " + dataset_train_paths[index])
    tdp.load_dataset_binvox_64(dataset_train_paths[index],
                               dataset_train_container,
                               input_file_format_suffix)
  assert "The train dataset is empty.", (len(dataset_train_container) > 0)

  # Test
  for index in range(len(dataset_test_paths)):
    tdp.load_dataset_binvox_64(
      dataset_test_paths[index],
      dataset_test_container,
      input_file_format_suffix)
  assert "The test dataset is empty.", (len(dataset_test_container) > 0)

  # Load the void volume
  verboseprint("\n\nLoading the void volume...")
  tdp.load_dataset_binvox_64(
    dataset_void_path[0],
    dataset_void_container,
    input_file_format_suffix)
  assert "Failed to load the void volume.", (len(dataset_void_container) == 1)

  # Do this before you iterate (because we need to feed in an array to tensorflow)
  dataset_train_container = np.asarray(dataset_train_container)
  dataset_test_container = np.asarray(dataset_test_container)

  # Checks
  verboseprint("Samples in training dataset (de facto): {0}".format(len(dataset_train_container)))
  verboseprint("Samples in testing dataset (de facto): {0}".format(len(dataset_test_container)))
  # print("Samples in dataset (processed): {0}".format(dataset_samples_count))

  # Declare the iterator_train
  iterator_train = tdp.data_iterator(dataset_train_container, batch_size)

  # Declare the iterator_test
  iterator_test = tdp.data_iterator(dataset_test_container, batch_size)

  return iterator_train, len(dataset_train_container), \
         iterator_test, len(dataset_test_container), \
         dataset_void_container
