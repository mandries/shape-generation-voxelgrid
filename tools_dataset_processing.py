import os  # to walk through directories, to rename files
import binvox_rw  # to manipulate binvox files
import pcl_tools
import numpy as np


# Parse a dataset directory and pick the input dataset files of type PCD
# def get_dataset_files_from_path(
def load_dataset_pcd(
    dataset_path,
    dataset,
    input_file_format_suffix,
    total_input_files_processed,
    sample_input_width,
    sample_input_height,
    sample_input_depth,
    img_input_channels):
  print("\nDataset path: " + dataset_path)
  # Walk through the directory, and pick the files
  for root, dirs, files in os.walk(dataset_path, topdown=True):  # directory
    for directory in dirs:
      print('\nFound: ' + directory + '/')
    for filename in files:
      print('\nFound: ' + filename)
      if filename.endswith(input_file_format_suffix):
        # Process the image and give it as input to the VAE
        # input_file
        global_filename = os.path.join(root, filename)
        # print("Processing:" + global_filename)
        pointcloud = pcl_tools.parse_pcd_file(global_filename)
        voxelized_pointcloud = pcl_tools.voxelize_pointcloud(
          pointcloud,
          sample_input_width,
          sample_input_height,
          sample_input_depth)
        dataset.append(voxelized_pointcloud)
        # pointcloud = voxelized_to_pointcloud(voxelized)
        # write_pointcloud_to_file(pointcloud)
        #
        # add_file_to_dataset(voxelized_pointcloud, dataset)
        # TODO select a default orientation for the voxelized pointcloud?
        # (so as to avoid learning different variations of it)
        # Flip horizontally
        # img_h_flip = cv2.flip(img_resized, 0)
        # add_image_to_processed_dataset(img_h_flip, dataset)
        total_input_files_processed += 1
    return total_input_files_processed


# Load dataset containing BINVOX files of resolution 64x64x64,
# with binary values for the content of each voxel
# by putting the loaded binary voxelgrids into the "dataset" container
def load_dataset_binvox_64(
    dataset_path,
    dataset,
    input_file_format_suffix,
    max_objects_to_load=50):

  # sample_input_width = voxelized_input_width,
  # sample_input_height = voxelized_input_height,
  # sample_input_depth = voxelized_input_depth,
  # img_input_channels = 1):

  print("\nDataset path: " + dataset_path)
  files_name_list = list()

  total_loaded_files = 0

  # Walk through the directory, and pick the files
  for root, dirs, files in os.walk(dataset_path, topdown=True):  # directory
    for directory in dirs:
      print('\nFound: ' + directory + '/')
    for filename in files:

      # if (no limit) or (within limits)
      if ((max_objects_to_load is None) or (total_loaded_files < max_objects_to_load)):

        # print('\nFound: ' + filename)
        if filename.endswith(input_file_format_suffix):
          global_filename = os.path.join(root, filename)
          files_name_list.append(global_filename)
          # print("Processing:" + global_filename)
          voxelgrid = pcl_tools.parse_BINVOX_file_into_voxel_grid(global_filename)
          dataset.append(voxelgrid)

          # increase the counter of loaded files
          total_loaded_files += 1

          # Add the voxelgrid complement
          # voxelgrid_complement = pcl_tools.getVoxelgridComplement(voxelgrid)
          # dataset.append(voxelgrid_complement)

  return files_name_list


# def add_file_to_dataset(
#         dataset_file,
# 		processed_dataset):
#     # img = np.multiply(img, 1. / 255.)
#     # Bring the value of each point in the file between [0,1]
#     processed_dataset.append(dataset_file)
#     return

# Automatic function to get randomized data batches
def data_iterator(input_dataset, batch_size, randomize=True):
  batch_index = 0
  indexes = np.arange(0, len(input_dataset))
  while True:
    if randomize:
      np.random.shuffle(indexes)
    shuffled_data = input_dataset[indexes]
    for batch_index in range(0, len(input_dataset), batch_size):
      images_batch = shuffled_data[batch_index:batch_index + batch_size]
      if images_batch.shape[0] % 2 != 0:
        if images_batch.shape[0] > 1:
          yield images_batch[:-1]
        else:
          yield
      else:
        yield images_batch

    # Computes the ratio of filled voxels vs empty voxels in the whole dataset


def compute_ratio_of_filled_vs_empty_voxels(
    dataset,
    voxelized_input_width,
    voxelized_input_height,
    voxelized_input_depth):
  dataset_length = len(dataset)
  filled_voxels = 0.0
  total_voxels = 0.0
  total_voxels = dataset_length * \
                 voxelized_input_width * \
                 voxelized_input_height * \
                 voxelized_input_depth
  for index_sample in range(0, dataset_length):
    filled_voxels += dataset[index_sample].sum()
  ratio = filled_voxels / (total_voxels - filled_voxels)
  return ratio
