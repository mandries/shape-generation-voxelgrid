# This Variational AutoEncoder
# combines latent descriptions of given objects into
# a single latent description of a new object

from __future__ import division, print_function, absolute_import
#~ from array import *
#~ from random import shuffle

import argparse # for parsing arguments
import sys # for counting the number of arguments

import numpy as np
import tensorflow as tf
# import time # for pausing the program
import time # to know how long it takes for the code to run
# Libraries for working with images
import os # to walk through directories, to rename files
import binvox_rw # to manipulate binvox files
import pcl_tools
import tools_dataset_processing
import tools_combination
import model_vae_3d

# The welcome message
# print("Start: AutoEncoder for Combining latent vector representations of objects. Requires valid path for loading the VAE model (defined in code). Program is running")

# Introduce the argument parser (useful if you want to load pre-learned parameters, or start learning from zero)
#parser = argparse.ArgumentParser()
# parser.add_argument("-r", "--restore_params",
#                 help="Restore the parameters in the graph from the given input file.",
#                 action="store_true")
#args = parser.parse_args()

# if (args.restore_params):
#     print ("Restore params enabled")
# else:
#     print ("Restore params disabled: learning from zero")

saver = 0


# sys.exit(0)

################################################################################
################################## Constants ###################################
################################################################################

##### Constants #####
dataset_container = []  # The array containing the (3d) image data
dataset_image_count = 0 # The counter used for counting the number of images in the dataset

# Parameters
learning_rate = 0.001
total_epochs = 10000
batch_size = 2

#inv_frequency_of_hallucinations_generation = 100 # check reconstruction quality every N batches (from noise)
#total_hallucinations_generated_per_check = 20
inv_frequency_of_reconstruction_evaluations = 2 # check reconstruction quality every N batches (from real images)
inv_frequency_of_parameters_saving = 1 # save after every epoch

### Dataset data ###
# Description of the input
voxelized_input_width = 64
voxelized_input_height = 64
voxelized_input_depth = 64
nn_img_input_channels = 1 # 4 # (3 color + 1 depth)

input_file_format_suffix = '.binvox' # point cloud (is not voxelized representation)

# Path from where the most recent model is loaded
models_path = '/media/Data/mihai/models/'
model_loading_path = models_path + 'model__t_1526250677__epoch_145__train_loss_55280.03303172957__test_loss_123886.02629405286__void_loss_129.80362' + '/model_feature_learning_3D.ckpt'

combinations_path = '/media/Data/mihai/combinations/'


################################################################################
################################# Functions ####################################
################################################################################

def print_graph():
    # Print the list of variables
    graph = tf.get_default_graph()
    # variable_list = [v.name for v in graph.variable_list]
    print("Graph operations:")
    operations_list = [op.name for op in graph.get_operations()]
    print(operations_list)
    #
    print("Graph variables: ")
    variables_list=[v.name for v in graph.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)]
    print(variables_list)
    # print(len(variables_list))


# This function is OK for 3d
def save_reconstructed_samples(
    reconstructed_samples,
    original_samples,
    directory):

    # Generate a folder to store the images
    # directory = "reconstructions_" + str(time.time()) + "iter_{0}".format(iteration_number)
    # if not os.path.exists(directory):
        # os.makedirs(directory)

    print('Saving originals and reconstructions... ')
    for sample_index in range(0, reconstructed_samples.shape[0]):
        # 1. Write original model to file

        # Write as point cloud
        # filename = os.path.join(   directory,
        #                            "sample_{0}_orig.pcd".format(sample_index)
        #                            )
        # pointcloud = pcl_tools.voxelized_to_pointcloud(original_samples[sample_index])
        # pcl_tools.write_pointcloud_to_file(pointcloud, filename)

        # Write as voxelgrid (binvox format)
        data = original_samples[sample_index] #voxelgrid
        dims = (voxelized_input_width,voxelized_input_height,voxelized_input_depth) # DEFAULT voxelgrid size here (not true size, which may vary)
        translate = (0,0,0)
        scale = 1
        axis_order = 'xyz'
        voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
        # Specify the file path
        filename = os.path.join(   directory,
                                   "sample_{0}_orig.binvox".format(sample_index)
                                   )
        filewriter = open(filename, 'w')
        # Write the binvox model to file (so that it can be seen through viewvow)
        binvox_rw.write(voxel_model, filewriter)


        # 2. Write reconstructed model to file
        # Threshold at 0.5 (to have binary states for voxels)
        voxelgrid = reconstructed_samples[sample_index] > 0.5
        # Write as voxelgrid (binvox format)
        data = voxelgrid
        dims = (voxelized_input_width,voxelized_input_height,voxelized_input_depth) # DEFAULT voxelgrid size here (not true size, which may vary)
        translate = (0,0,0)
        scale = 1
        axis_order = 'xyz'
        voxel_model = binvox_rw.Voxels(data, dims, translate, scale, axis_order)
        # Specify the file path
        filename = os.path.join(   directory,
                                   "sample_{0}_recon.binvox".format(sample_index)
                                   )
        filewriter = open(filename, 'w')
        # Write the binvox model to file (so that it can be seen through viewvow)
        binvox_rw.write(voxel_model, filewriter)
    print('done')
    return
################################################################################
############################# Building the graph ###############################
################################################################################

input_placeholder = tf.placeholder(
                    tf.float32,
                    shape=[
                            None, # the size of the batch
                            voxelized_input_width,
                            voxelized_input_height,
                            voxelized_input_depth
                          ],
                    name="input_placeholder_Wonderful"
                          # nn_img_input_channels
                    )

def build_graph():
    # Capture the output of the encoder
    # with tf.name_scope(name="unique_graph"):
    with tf.variable_scope("model", reuse=None): #tf.AUTO_REUSE): #True):
        objects_z_means, objects_z_log_vars = model_vae_3d.encoder_net(input_placeholder)
    # with tf.variable_scope("model", reuse=True):
    #     z_mean_2, z_log_var_2 = model_vae_3d.encoder_net(input_placeholder_2)


    # with tf.variable_scope("model", reuse=True):
        combined_means, combined_log_vars = \
            tools_combination.combine_multiple_object_descriptions_through_averaging_tf(
                objects_z_means,
                objects_z_log_vars
            )
        #tools_combination.combine_multiple_object_descriptions_through_gaussian_intersection_tf(

        # Sampler: Normal (gaussian) random distribution
        eps_for_combined_sample = tf.random_normal(
                    tf.shape(combined_means), # we only care about the shape
                    dtype=tf.float32,
                    mean=0.,
                    stddev=1.0,
                    name='eps_for_combined_sample')
        print("shape of eps (encoder output): {}".format(eps_for_combined_sample.shape))
        # Transform the sample from a Gaussian one to a "Combined probability sample"
        z_combined = tf.add(combined_means, tf.exp(combined_log_vars / 2) * eps_for_combined_sample)
        print("shape of z (decoder input): {}".format(z_combined.shape))

        # The combined object shape:
        #with tf.variable_scope("model", reuse=True):
        output_voxelized_volume_combined = model_vae_3d.decoder_net(z_combined)
        # Squeezing the combined vector fron (1,64,64,64) to (64,64,64)
        output_voxelized_volume_combined = tf.squeeze(output_voxelized_volume_combined, axis=0)



        # Generate reconstructions for all the samples in the batch
        # z_1 = z_mean_1 + tf.exp(z_log_var_1 / 2) * eps
        eps_for_samples_in_batch = tf.random_normal(
                    tf.shape(objects_z_means), # (we only care about the shape)
                    dtype=tf.float32,
                    mean=0.,
                    stddev=1.0,
                    name='eps_for_samples_in_batch')
        z_reconstructions = tf.add(objects_z_means, tf.exp(objects_z_log_vars/ 2) * eps_for_samples_in_batch)


        # Computing the KL loss per dimension (of the latent vector),
        # to identify which variables are most important in the reconstruction of the object
        kl_loss_per_dim = model_vae_3d.kl_loss_per_dim(
                        z_mean = objects_z_means,
                        z_log_var = objects_z_log_vars
                        )

        # Generate an ordering for the variables:
        # ordering = tf.lin_space(start=0, stop=511, num=1, name="KL_div_ordering")
        # kl_loss_per_dim_with_indexes = tf.stack([kl_loss_per_dim, ordering], axis=1) # to have the shape (512,2)
        # Sort the top KL values, and get their indices
        top_kl_values, top_kl_indices = tf.nn.top_k(
                        input = kl_loss_per_dim,
                        k = model_vae_3d.latent_dim, # how many top entries to find ?
                        sorted = True, # sort in descending order
                        name = "kl_loss_per_dim_sorting"
                    )
        # print("Top KL values:")
        # print(top_kl_values)
        # print("{0}; {1}; {2}; {3}".format(top_kl_values[0], top_kl_values[1], top_kl_values[2], top_kl_values[3]))
        # print("Top KL indices:")
        # print(top_kl_indices)
        # print("{0}; {1}; {2}; {3}".format(top_kl_indices[0], top_kl_indices[1], top_kl_indices[2], top_kl_indices[3]))


    # Generate reconstructions for all the samples in the batch
    with tf.variable_scope("model", reuse=True):
        # The reconstructions of the input models
        output_voxelized_volume_reconstructions = model_vae_3d.decoder_net(z_reconstructions)
        # print("z_reconstructions shape:")
        # print(z_reconstructions.shape)

    # combined_samples_tensor = tf.nn.sigmoid(output_voxelized_volume)


    # Initialize the variables (i.e. assign their default value)
    # init = tf.global_variables_initializer()
    # The model (graph) saver (needs to be instanciated after the graph is built)
    # if ((args.save_params) or (args.restore_params)):
    saver = tf.train.Saver()

    ############################################################################
    ############################################################################
    # outside of the model scope, since this part was not in the trained network
    # with tf.variable_scope("model", reuse=tf.AUTO_REUSE): #True):
    # Introduce variations to the combined model
    # by varying the sampled value of a given latent_variable
    # latent_variable_id = 0 # TODO introduce here the ID of the latent variable to vary
    print("top_kl_indices.shape:")
    print(top_kl_indices.shape)
    print("top_kl_indices[0].shape:")
    print(top_kl_indices[0].shape)
    # scalar_index = tf.reshape(top_kl_indices[0],[])
    z_combined_variations, total_sample_variations = tools_combination.explore_latent_variable_effect(
                                z_combined, # the already combined object description,
                                # in which the role of one variable needs to be inspects,
                                # by changing its value around the mean, inside the stddev range
                                combined_means,
                                combined_log_vars,
                                top_kl_indices #scalar_index #top_kl_indices[0]
                                # 0, #latent_variable_id
                                )
    ############################################################################
    ############################################################################

    # # Generate reconstructions for all the samples in the batch
    # with tf.variable_scope("model", reuse=True):
    #     # The reconstructions of the input models
    #     output_voxelized_volume_reconstructions = model_vae_3d.decoder_net(z_reconstructions)

    with tf.variable_scope("model", reuse=True):
        # The variations on the combined model
        # Now reconstruct volumes from these N object descriptions
        print("z_combined_variations shape: (before decoding)")
        print(z_combined_variations.shape)
        output_voxelized_volume_variations = model_vae_3d.decoder_net(z_combined_variations)
        # output_voxelized_volume_variations = output_voxelized_volume_reconstructions # Just for a test. Delete this incorrect lignt afterwards



    return  \
        output_voxelized_volume_reconstructions, \
        output_voxelized_volume_combined, \
        output_voxelized_volume_variations, \
        top_kl_values, \
        top_kl_indices, \
        combined_means, \
        combined_log_vars, \
        z_combined_variations, \
        saver

        # input_placeholder_2, \
        # output_voxelized_volume_recon1, \
        # output_voxelized_volume_recon2, \
        # output_voxelized_volume_combined, \
        # saver

################################################################################
############################## Running the graph ###############################
################################################################################

# dataset_train_paths = (
#     '/media/Data/datasets/ModelNet10_binvox/bathtub/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/bed/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/chair/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/dresser/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/desk/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/monitor/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/night_stand/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/sofa/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/table/train/', \
#     '/media/Data/datasets/ModelNet10_binvox/toilet/train/')
#
# dataset_test_paths = (
#     '/media/Data/datasets/ModelNet10_binvox/bathtub/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/bed/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/chair/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/dresser/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/desk/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/monitor/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/night_stand/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/sofa/test/',\
#     '/media/Data/datasets/ModelNet10_binvox/table/test/', \
#     '/media/Data/datasets/ModelNet10_binvox/toilet/test/')

#dataset_paths = ['/media/Data/datasets/combination_datasets_test/bathtubs/']
#dataset_paths = ['/media/Data/datasets/combination_datasets_test/chairs/']
#dataset_paths = ['/media/Data/datasets/combination_datasets_test/monitors/']
#dataset_paths = ['/media/Data/datasets/combination_datasets_test/tables/']
#dataset_paths = ['/media/Data/datasets/combination_datasets_test/dressers/']
#dataset_paths = ['/media/Data/datasets/combination_datasets_test/sofas/']
#dataset_paths = ['/media/Data/mihai/datasets/fork_spoon/']
dataset_paths = ['/media/Data/mihai/datasets/spoon_fork_ycb/']

if __name__=='__main__':

	print("Processing the dataset files...")
	# Go through the dataset
	for index in range(len(dataset_paths)):
	    print("Processing the dataset: " + dataset_paths[index])
	    # get_dataset_files_from_path(
	    tools_dataset_processing.load_dataset_binvox_64(
		dataset_paths[index], # vislab_dataset_path,
		dataset_container,
		input_file_format_suffix)

	# Do this before you iterate (because we need to feed in an array to tensorflow)
	dataset_container = np.asarray(dataset_container)
	# Checks
	print("Samples in dataset (de facto): {0}".format(len(dataset_container)))

	# Declare the iterator
	iterator = tools_dataset_processing.data_iterator(dataset_container, batch_size)

	#~ # Start a new TF session
	session = tf.Session()

	# if __name__ = main():
	# input_placeholder_1, \
	# input_placeholder_2, \
	# output_voxelized_volume_recon1, \
	# output_voxelized_volume_recon2, \

	output_voxelized_volume_reconstructions, \
	output_voxelized_volume_combined, \
	output_voxelized_volume_variations, \
	output_top_kl_values, \
	output_top_kl_indices, \
	output_combined_means, \
	output_combined_log_vars, \
	output_z_combined_variations, \
	saver = build_graph() #session


	# print_graph()


	# Run the initializer
	# Initialize the variables (i.e. assign their default value)
	# init = tf.global_variables_initializer()
	# session.run(init)
	# Initialize all variables (just to initialize those which were not saved in the model)
	# all_variables = tf.get_collection_ref(tf.GraphKeys.GLOBAL_VARIABLES)
	# session.run(tf.variables_initializer(all_variables))
	# Restore the model from file (should overwrite the model variables that were already initialized)
	# TODO: check if the restore overwrites the variables that were already initialized
	print("Restoring model from file... ")
	saver.restore(session, model_loading_path)
	print("done.")


	print("Prepring the first batch")
	# TODO maybe error here? The initializer is run twice (for the tools_combination part?)
	batch_x_1 = iterator.next()
	# batch_x_2 = iterator.next()
	print("Extracted batch 1")
	feed_dict = {   input_placeholder: batch_x_1}
	print("Prepared the feed_dict")
	# session.run(   [tf.global_variables_initializer()], # init,
	#             feed_dict = feed_dict
	#         )


	# voxelized outputs
	#input_samples, \

	output_voxelized_reconstructions, \
	output_reconstruction_combined_category, \
	output_reconstruction_combined_variations, \
	valued_output_top_kl_values, \
	valued_output_top_kl_indices, \
	valued_output_combined_means, \
	valued_output_combined_log_vars, \
	valued_output_z_combined_variations = session.run(
		[
		    #input_placeholder,
		    output_voxelized_volume_reconstructions,
		    output_voxelized_volume_combined,
		    output_voxelized_volume_variations,
		    output_top_kl_values,
		    output_top_kl_indices,
		    output_combined_means,
		    output_combined_log_vars,
		    output_z_combined_variations, \
		],
		feed_dict = feed_dict
		)

	print("Top KL values:")
	print(valued_output_top_kl_values)
	print("{0}; {1}; {2}; {3}".format(valued_output_top_kl_values[0], valued_output_top_kl_values[1], valued_output_top_kl_values[2], valued_output_top_kl_values[3]))
	print("Top KL indices:")
	print(valued_output_top_kl_indices)
	print("{0}; {1}; {2}; {3}".format(valued_output_top_kl_indices[0], valued_output_top_kl_indices[1], valued_output_top_kl_indices[2], valued_output_top_kl_indices[3]))


	print("Generated the combination of {0} samples.".format(batch_size))
	# print("Generated {0} variations of the combined sample".format(output_reconstruction_combined_variations.size))
	# print("Shapes of i1, i2, o1, o2, comb\n")
	# (1x 64x64x64)
	# print(input_sample_1.shape)
	# print(input_sample_2.shape)
	# print(output_recon1_sample_1.shape)
	# print(output_recon1_sample_2.shape)
	# print(output_recon_combined.shape)

	# Save the models for checks
	directory = combinations_path + "/category_combination_" + str(time.time()) # + "iter_{0}".format(iteration_number)
	if not os.path.exists(directory):
	    os.makedirs(directory)
	filename = "category_combination"

	 # Save the reconstructions
	save_reconstructed_samples(
	    output_voxelized_reconstructions,
	    batch_x_1, #input_samples,
	    directory)
	print("output_reconstruction_combined_category.shape:")
	print(output_reconstruction_combined_category.shape)

	# Save the combination
	tools_combination.save_voxelgrid(
	    output_reconstruction_combined_category, #[0],
	    directory,
	    filename)
	print("Saved the combination files.")

	# Save the variations of the combined model
	# DEBUG INFO
	print("Total variations: {0}".format(output_reconstruction_combined_variations.shape[0]))
	print("Variations shape:")
	print(output_reconstruction_combined_variations.shape)
	#
	for index in range (output_reconstruction_combined_variations.shape[0]):
	    filename = "category_combination_variation_" + str(index)
	    tools_combination.save_voxelgrid(
		output_reconstruction_combined_variations[index],
		directory,
		filename)
	print("Saved the 3D variations of the combined model.")

	print("These 3 values should be different: {0} != {1} != {2}".format(valued_output_z_combined_variations[0][305], valued_output_z_combined_variations[1][305], valued_output_z_combined_variations[2][305]))

	print("Combined means:")
	print(valued_output_combined_means)
	print("Combined log vars:")
	print(valued_output_combined_log_vars)
