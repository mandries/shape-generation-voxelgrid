# Convert all dataset from OFF files into BINVOX files
# find /home/mihai/datasets/3D_Shapenets_Princeton/ModelNet10/ModelNet10_binvox_exact/ -type f -name "*.off" -exec /home/mihai/datasets/Binvox_converter/binvox -d 64 -e {} \;
# find /home/mihai/datasets/3D_Shapenets_Princeton/test/ -type f -name "*.off" -exec /home/mihai/datasets/Binvox_converter/binvox -d 64 -e {} \; -exec rm {} \;


# No centering in the voxel cube
find /home/mihai/datasets/3D_datasets/3D_Shapenets_Princeton/ModelNet40/ -type f -name "*.off" \
    -exec /home/mihai/datasets/Binvox_converter/binvox -d 64 -cb -e {} \; \
    -exec /home/mihai/datasets/Binvox_converter/binvox -d 64 -cb -e {} \; \
    -exec /home/mihai/datasets/Binvox_converter/binvox -d 64 -cb -e -rotz {} \; \
    -exec /home/mihai/datasets/Binvox_converter/binvox -d 64 -cb -e -rotz -rotz {} \; \
    -exec /home/mihai/datasets/Binvox_converter/binvox -d 64 -cb -e -rotz -rotz -rotz {} \; \
    -exec rm {} \;

# Centering in the voxel cube
# find /home/mihai/datasets/3D_Shapenets_Princeton/ModelNet10/ModelNet10_binvox_exact_off_center_4views/ -type f -name "*.off" \
#     -exec /home/mihai/datasets/Binvox_converter/binvox -d 64 -e -cb {} \; \
#     -exec /home/mihai/datasets/Binvox_converter/binvox -d 64 -e -cb -rotz {} \; \
#     -exec /home/mihai/datasets/Binvox_converter/binvox -d 64 -e -cb -rotz -rotz {} \; \
#     -exec /home/mihai/datasets/Binvox_converter/binvox -d 64 -e -cb -rotz -rotz -rotz {} \; \
#     -exec rm {} \;

# Command structure:
# find <dataset_path>  -type f -name <filetype> -exec <binvox_converter_path> <resolution> {} \;
# find . -exec grep chrome {} \;
