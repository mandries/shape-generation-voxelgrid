#!/bin/bash

windowWidth=200
windowHeight=200
xpos=75
ypos_orig=0
# ypos_recon=265
ypos_recon=225
xstep=200
ystep=265
maxImagesPerRow=8
totalSamplesToDisplay=6

number=0 ; while [[ $number -le $totalSamplesToDisplay ]] ; do \
	# echo 'sample_'$number'_orig.binvox' ; \
	#
	# Open original
	viewvox 'sample_'$number'_orig.binvox' & \
	sleep 0.1 ; \
	wmctrl -v -r viewvox -N 'sample_'$number'_orig' & \
	#
	# Open reconstruction
	viewvox 'sample_'$number'_recon.binvox' & \
	sleep 0.2 ; \
	wmctrl -v -r viewvox -N 'sample_'$number'_recon' & \
	#
	# Align
	# wmctrl -r 'sample_'$number'_orig' -e 0,75,0,200,200
	# wmctrl -r 'sample_'$number'_recon' -e 0,75,0,200,200
	wmctrl -r 'sample_'$number'_orig' -e 0,$xpos,$ypos_orig,$windowWidth,$windowHeight
	wmctrl -r 'sample_'$number'_recon' -e 0,$xpos,$ypos_recon,$windowWidth,$windowHeight
	# #
	# Increase counter
	((number = number + 1)) ; \
	xpos=$((xpos+xstep)) ; \
	# echo $xpos ; \
	if [ $(($number % $maxImagesPerRow)) -eq 0 ]
	then
		xpos=75
		ypos_orig=$((ypos_orig+ystep+ystep)) ; \
		ypos_recon=$((ypos_orig+windowHeight)) ; \
	fi
done

viewvox 'category_combination.binvox' & \
sleep 0.1 ; \
wmctrl -v -r viewvox -N 'category_combination' & \
sleep 0.1 ; \
wmctrl -r 'category_combination' -e 0,$xpos,$ypos_orig,$(($windowWidth*2)),$(($windowHeight*2))
